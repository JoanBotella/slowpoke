<?php
declare(strict_types=1);

namespace demo\book\front\library\serviceContainer;

use demo\book\front\page\about\library\serviceContainer\DemoFrontAboutServiceContainerItf;
use demo\book\front\page\home\library\serviceContainer\DemoFrontHomeServiceContainerItf;
use demo\book\front\page\login\library\serviceContainer\DemoFrontLoginServiceContainerItf;
use demo\book\front\page\status400\library\serviceContainer\DemoFrontStatus400ServiceContainerItf;
use demo\book\front\page\status404\library\serviceContainer\DemoFrontStatus404ServiceContainerItf;
use demo\book\front\page\status500\library\serviceContainer\DemoFrontStatus500ServiceContainerItf;
use demo\book\front\service\translationContainer\DemoFrontTranslationContainerItf;
use demo\book\front\service\widget\aside\DemoFrontAsideWidgetItf;
use demo\book\front\service\widget\body\DemoFrontBodyWidgetItf;
use demo\book\front\service\widget\footer\DemoFrontFooterWidgetItf;
use demo\book\front\service\widget\header\DemoFrontHeaderWidgetItf;
use demo\book\front\service\widget\nav\DemoFrontNavWidgetItf;

interface
	DemoFrontServiceContainerItf
extends
	DemoFrontAboutServiceContainerItf,
	DemoFrontHomeServiceContainerItf,
	DemoFrontLoginServiceContainerItf,
	DemoFrontStatus400ServiceContainerItf,
	DemoFrontStatus404ServiceContainerItf,
	DemoFrontStatus500ServiceContainerItf
{

	public function getDemoFrontTranslationContainer():DemoFrontTranslationContainerItf;

	public function getDemoFrontAsideWidget():DemoFrontAsideWidgetItf;

	public function getDemoFrontBodyWidget():DemoFrontBodyWidgetItf;

	public function getDemoFrontFooterWidget():DemoFrontFooterWidgetItf;

	public function getDemoFrontHeaderWidget():DemoFrontHeaderWidgetItf;

	public function getDemoFrontNavWidget():DemoFrontNavWidgetItf;

}