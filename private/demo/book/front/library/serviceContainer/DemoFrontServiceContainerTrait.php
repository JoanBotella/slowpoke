<?php
declare(strict_types=1);

namespace demo\book\front\library\serviceContainer;

use demo\book\front\page\about\library\serviceContainer\DemoFrontAboutServiceContainerTrait;
use demo\book\front\page\home\library\serviceContainer\DemoFrontHomeServiceContainerTrait;
use demo\book\front\page\login\library\serviceContainer\DemoFrontLoginServiceContainerTrait;
use demo\book\front\page\status400\library\serviceContainer\DemoFrontStatus400ServiceContainerTrait;
use demo\book\front\page\status404\library\serviceContainer\DemoFrontStatus404ServiceContainerTrait;
use demo\book\front\page\status500\library\serviceContainer\DemoFrontStatus500ServiceContainerTrait;
use demo\book\front\service\translationContainer\DemoFrontTranslationContainer;
use demo\book\front\service\translationContainer\DemoFrontTranslationContainerItf;
use demo\book\front\service\widget\aside\DemoFrontAsideWidget;
use demo\book\front\service\widget\aside\DemoFrontAsideWidgetItf;
use demo\book\front\service\widget\body\DemoFrontBodyWidget;
use demo\book\front\service\widget\body\DemoFrontBodyWidgetItf;
use demo\book\front\service\widget\footer\DemoFrontFooterWidget;
use demo\book\front\service\widget\footer\DemoFrontFooterWidgetItf;
use demo\book\front\service\widget\header\DemoFrontHeaderWidget;
use demo\book\front\service\widget\header\DemoFrontHeaderWidgetItf;
use demo\book\front\service\widget\nav\DemoFrontNavWidget;
use demo\book\front\service\widget\nav\DemoFrontNavWidgetItf;

trait
	DemoFrontServiceContainerTrait
{
	use
		DemoFrontAboutServiceContainerTrait,
		DemoFrontHomeServiceContainerTrait,
		DemoFrontLoginServiceContainerTrait,
		DemoFrontStatus400ServiceContainerTrait,
		DemoFrontStatus404ServiceContainerTrait,
		DemoFrontStatus500ServiceContainerTrait
	;

	private DemoFrontTranslationContainerItf $frontTranslationContainer;

	public function getDemoFrontTranslationContainer():DemoFrontTranslationContainerItf
	{
		if (!isset($this->frontTranslationContainer))
		{
			$this->frontTranslationContainer = $this->buildDemoFrontTranslationContainer();
		}
		return $this->frontTranslationContainer;
	}

		protected function buildDemoFrontTranslationContainer():DemoFrontTranslationContainerItf
		{
			return new DemoFrontTranslationContainer(
			);
		}

	private DemoFrontAsideWidgetItf $frontAsideWidget;

	public function getDemoFrontAsideWidget():DemoFrontAsideWidgetItf
	{
		if (!isset($this->frontAsideWidget))
		{
			$this->frontAsideWidget = $this->buildDemoFrontAsideWidget();
		}
		return $this->frontAsideWidget;
	}

		protected function buildDemoFrontAsideWidget():DemoFrontAsideWidgetItf
		{
			return new DemoFrontAsideWidget(
			);
		}

	private DemoFrontBodyWidgetItf $frontBodyWidget;

	public function getDemoFrontBodyWidget():DemoFrontBodyWidgetItf
	{
		if (!isset($this->frontBodyWidget))
		{
			$this->frontBodyWidget = $this->buildDemoFrontBodyWidget();
		}
		return $this->frontBodyWidget;
	}

		protected function buildDemoFrontBodyWidget():DemoFrontBodyWidgetItf
		{
			return new DemoFrontBodyWidget(
			);
		}

	private DemoFrontHeaderWidgetItf $frontHeaderWidget;

	public function getDemoFrontHeaderWidget():DemoFrontHeaderWidgetItf
	{
		if (!isset($this->frontHeaderWidget))
		{
			$this->frontHeaderWidget = $this->buildDemoFrontHeaderWidget();
		}
		return $this->frontHeaderWidget;
	}

		protected function buildDemoFrontHeaderWidget():DemoFrontHeaderWidgetItf
		{
			return new DemoFrontHeaderWidget(
			);
		}

	private DemoFrontFooterWidgetItf $frontFooterWidget;

	public function getDemoFrontFooterWidget():DemoFrontFooterWidgetItf
	{
		if (!isset($this->frontFooterWidget))
		{
			$this->frontFooterWidget = $this->buildDemoFrontFooterWidget();
		}
		return $this->frontFooterWidget;
	}

		protected function buildDemoFrontFooterWidget():DemoFrontFooterWidgetItf
		{
			return new DemoFrontFooterWidget(
			);
		}

	private DemoFrontNavWidgetItf $frontNavWidget;

	public function getDemoFrontNavWidget():DemoFrontNavWidgetItf
	{
		if (!isset($this->frontNavWidget))
		{
			$this->frontNavWidget = $this->buildDemoFrontNavWidget();
		}
		return $this->frontNavWidget;
	}

		protected function buildDemoFrontNavWidget():DemoFrontNavWidgetItf
		{
			return new DemoFrontNavWidget(
			);
		}

}