<?php
declare(strict_types=1);

namespace demo\book\front\library\translation;

use demo\book\front\page\about\library\translation\DemoFrontAboutTranslationItf;
use demo\book\front\page\home\library\translation\DemoFrontHomeTranslationItf;
use demo\book\front\page\login\library\translation\DemoFrontLoginTranslationItf;
use demo\book\front\page\status400\library\translation\DemoFrontStatus400TranslationItf;
use demo\book\front\page\status404\library\translation\DemoFrontStatus404TranslationItf;
use demo\book\front\page\status500\library\translation\DemoFrontStatus500TranslationItf;
use demo\book\front\service\widget\footer\library\translation\DemoFrontFooterWidgetTranslationItf;
use demo\book\front\service\widget\nav\library\translation\DemoFrontNavWidgetTranslationItf;

interface
	DemoFrontTranslationItf
extends
	DemoFrontAboutTranslationItf,
	DemoFrontHomeTranslationItf,
	DemoFrontLoginTranslationItf,
	DemoFrontStatus400TranslationItf,
	DemoFrontStatus404TranslationItf,
	DemoFrontStatus500TranslationItf,
	DemoFrontNavWidgetTranslationItf,
	DemoFrontFooterWidgetTranslationItf
{

}