<?php
declare(strict_types=1);

namespace demo\book\front\library\translation\es;

use demo\book\front\library\translation\DemoFrontTranslationItf;
use demo\book\front\page\about\library\translation\es\DemoFrontAboutEsTranslationTrait;
use demo\book\front\page\home\library\translation\es\DemoFrontHomeEsTranslationTrait;
use demo\book\front\page\login\library\translation\es\DemoFrontLoginEsTranslationTrait;
use demo\book\front\page\status400\library\translation\es\DemoFrontStatus400EsTranslationTrait;
use demo\book\front\page\status404\library\translation\es\DemoFrontStatus404EsTranslationTrait;
use demo\book\front\page\status500\library\translation\es\DemoFrontStatus500EsTranslationTrait;
use demo\book\front\service\widget\footer\library\translation\es\DemoFrontFooterWidgetEsTranslationTrait;
use demo\book\front\service\widget\nav\library\translation\es\DemoFrontNavWidgetEsTranslationTrait;

final class
	DemoFrontEsTranslation
implements
	DemoFrontTranslationItf
{
	use
		DemoFrontAboutEsTranslationTrait,
		DemoFrontHomeEsTranslationTrait,
		DemoFrontLoginEsTranslationTrait,
		DemoFrontStatus400EsTranslationTrait,
		DemoFrontStatus404EsTranslationTrait,
		DemoFrontStatus500EsTranslationTrait,
		DemoFrontNavWidgetEsTranslationTrait,
		DemoFrontFooterWidgetEsTranslationTrait
	;

}