<?php
declare(strict_types=1);

namespace demo\book\front\library\translation\en;

use demo\book\front\library\translation\DemoFrontTranslationItf;
use demo\book\front\page\about\library\translation\en\DemoFrontAboutEnTranslationTrait;
use demo\book\front\page\home\library\translation\en\DemoFrontHomeEnTranslationTrait;
use demo\book\front\page\login\library\translation\en\DemoFrontLoginEnTranslationTrait;
use demo\book\front\page\status400\library\translation\en\DemoFrontStatus400EnTranslationTrait;
use demo\book\front\page\status404\library\translation\en\DemoFrontStatus404EnTranslationTrait;
use demo\book\front\page\status500\library\translation\en\DemoFrontStatus500EnTranslationTrait;
use demo\book\front\service\widget\footer\library\translation\en\DemoFrontFooterWidgetEnTranslationTrait;
use demo\book\front\service\widget\nav\library\translation\en\DemoFrontNavWidgetEnTranslationTrait;

final class
	DemoFrontEnTranslation
implements
	DemoFrontTranslationItf
{
	use
		DemoFrontAboutEnTranslationTrait,
		DemoFrontHomeEnTranslationTrait,
		DemoFrontLoginEnTranslationTrait,
		DemoFrontStatus400EnTranslationTrait,
		DemoFrontStatus404EnTranslationTrait,
		DemoFrontStatus500EnTranslationTrait,
		DemoFrontNavWidgetEnTranslationTrait,
		DemoFrontFooterWidgetEnTranslationTrait
	;

}