<?php
declare(strict_types=1);

namespace demo\book\front\library\controller;

use slowpoke\core\library\controller\CoreControllerAbs;
use slowpoke\session\service\sessionManager\SessionSessionManagerItf;
use slowpoke\session\library\controller\SessionControllerTrait;
use slowpoke\session\service\translationContainer\SessionTranslationContainerItf;

abstract class
	DemoFrontControllerAbs
extends
	CoreControllerAbs
{
	use
		SessionControllerTrait
	;

	private SessionSessionManagerItf $sessionSessionManager;

	private SessionTranslationContainerItf $sessionTranslationContainer;

	public function __construct(
		SessionSessionManagerItf $sessionSessionManager,
		SessionTranslationContainerItf $sessionTranslationContainer
	)
	{
		$this->sessionSessionManager = $sessionSessionManager;
		$this->sessionTranslationContainer = $sessionTranslationContainer;
	}

		protected function getSessionSessionManager():SessionSessionManagerItf
		{
			return $this->sessionSessionManager;
		}

		protected function getSessionTranslationContainer():SessionTranslationContainerItf
		{
			return $this->sessionTranslationContainer;
		}

}
