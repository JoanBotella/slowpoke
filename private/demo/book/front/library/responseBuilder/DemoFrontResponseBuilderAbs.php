<?php
declare(strict_types=1);

namespace demo\book\front\library\responseBuilder;

use slowpoke\core\library\CoreNotifications;
use demo\book\front\page\home\service\urlBuilder\DemoFrontHomeUrlBuilderItf;
use demo\book\front\service\widget\body\DemoFrontBodyWidgetContext;
use demo\book\front\service\widget\body\DemoFrontBodyWidgetItf;
use demo\book\front\service\widget\footer\DemoFrontFooterWidgetContext;
use demo\book\front\service\widget\footer\DemoFrontFooterWidgetItf;
use demo\book\front\service\widget\header\DemoFrontHeaderWidgetContext;
use demo\book\front\service\widget\header\DemoFrontHeaderWidgetItf;
use demo\book\front\service\widget\nav\DemoFrontNavWidgetContext;
use demo\book\front\service\widget\nav\DemoFrontNavWidgetItf;
use slowpoke\core\library\responseBuilder\CoreResponseBuilderAbs;
use slowpoke\core\service\widget\html\CoreHtmlWidgetItf;
use slowpoke\core\service\widget\notifications\CoreNotificationsWidgetContext;
use slowpoke\core\service\widget\notifications\CoreNotificationsWidgetItf;
use demo\book\front\library\translation\DemoFrontTranslationItf;
use demo\book\front\page\about\service\urlBuilder\DemoFrontAboutUrlBuilderItf;
use demo\book\front\service\translationContainer\DemoFrontTranslationContainerItf;
use demo\book\front\service\widget\aside\DemoFrontAsideWidgetContext;
use demo\book\front\service\widget\aside\DemoFrontAsideWidgetItf;

abstract class
	DemoFrontResponseBuilderAbs
extends
	CoreResponseBuilderAbs
{

	private string $applicationName;

	private DemoFrontTranslationContainerItf $frontTranslationContainer;

	private DemoFrontHeaderWidgetItf $frontHeaderWidget;

	private DemoFrontNavWidgetItf $frontNavWidget;

	private CoreNotificationsWidgetItf $coreNotificationsWidget;

	private DemoFrontAsideWidgetItf $frontAsideWidget;

	private DemoFrontFooterWidgetItf $frontFooterWidget;

	private DemoFrontBodyWidgetItf $frontBodyWidget;

	private DemoFrontHomeUrlBuilderItf $demoFrontHomeUrlBuilder;

	private DemoFrontAboutUrlBuilderItf $frontAboutUrlBuilder;

	public function __construct(
		string $protocol,
		string $domain,
		string $basePath,
		bool $useMinifiedScripts,
		bool $useMinifiedStyles,
		CoreHtmlWidgetItf $coreHtmlWidget,

		string $applicationName,
		DemoFrontTranslationContainerItf $frontTranslationContainer,
		DemoFrontHeaderWidgetItf $frontHeaderWidget,
		DemoFrontNavWidgetItf $frontNavWidget,
		CoreNotificationsWidgetItf $coreNotificationsWidget,
		DemoFrontAsideWidgetItf $frontAsideWidget,
		DemoFrontFooterWidgetItf $frontFooterWidget,
		DemoFrontBodyWidgetItf $frontBodyWidget,
		DemoFrontHomeUrlBuilderItf $demoFrontHomeUrlBuilder,
		DemoFrontAboutUrlBuilderItf $frontAboutUrlBuilder
	)
	{
		parent::__construct(
			$protocol,
			$domain,
			$basePath,
			$useMinifiedScripts,
			$useMinifiedStyles,
			$coreHtmlWidget
		);
		$this->applicationName = $applicationName;
		$this->frontTranslationContainer = $frontTranslationContainer;
		$this->frontHeaderWidget = $frontHeaderWidget;
		$this->frontNavWidget = $frontNavWidget;
		$this->coreNotificationsWidget = $coreNotificationsWidget;
		$this->frontAsideWidget = $frontAsideWidget;
		$this->frontFooterWidget = $frontFooterWidget;
		$this->frontBodyWidget = $frontBodyWidget;
		$this->demoFrontHomeUrlBuilder = $demoFrontHomeUrlBuilder;
		$this->frontAboutUrlBuilder = $frontAboutUrlBuilder;
	}

	protected function buildBodyElement():string
	{
		$context = new DemoFrontBodyWidgetContext(
			$this->buildHeaderElement(),
			$this->buildNavElement(),
			$this->buildCoreNotificationsElement(),
			$this->buildAsideElement(),
			$this->buildMainElement(),
			$this->buildFooterElement()
		);
		return $this->frontBodyWidget->render($context);
	}

		private function buildHeaderElement():string
		{
			$context = new DemoFrontHeaderWidgetContext(
				$this->applicationName
			);
			return $this->frontHeaderWidget->render($context);
		}

		private function buildNavElement():string
		{
			$context = new DemoFrontNavWidgetContext(
				$this->getDemoFrontTranslation(),
				$this->buildDemoFrontHomeUrl(),
				$this->buildDemoFrontAboutUrl()
			);
			return $this->frontNavWidget->render($context);
		}

			protected function getDemoFrontTranslation():DemoFrontTranslationItf
			{
				return $this->frontTranslationContainer->getTranslation(
					$this->getLanguageCode()
				);
			}

			private function buildDemoFrontHomeUrl():string
			{
				return $this->demoFrontHomeUrlBuilder->build(
					$this->getLanguageCode()
				);
			}

			private function buildDemoFrontAboutUrl():string
			{
				return $this->frontAboutUrlBuilder->build(
					$this->getLanguageCode()
				);
			}

		private function buildCoreNotificationsElement():string
		{
			$context = new CoreNotificationsWidgetContext(
				$this->getCoreNotifications()
			);
			return $this->coreNotificationsWidget->render($context);
		}

			abstract protected function getCoreNotifications():CoreNotifications;

		private function buildAsideElement():string
		{
			$context = new DemoFrontAsideWidgetContext(
			);
			return $this->frontAsideWidget->render($context);
		}

		abstract protected function buildMainElement():string;

		private function buildFooterElement():string
		{
			$context = new DemoFrontFooterWidgetContext(
				$this->getDemoFrontTranslation(),
				$this->getAuthorAnchorHref()
			);
			return $this->frontFooterWidget->render($context);
		}

			private function getAuthorAnchorHref():string
			{
				return 'https://joanbotella.com';
			}

	protected function getTitleContent():string
	{
		return
			$this->getPageName()
			.' - '
			.$this->applicationName
		;
	}

		abstract protected function getPageName():string;

}
