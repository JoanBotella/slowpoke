<?php
declare(strict_types=1);

namespace demo\book\front\library\requestValidator;

use slowpoke\core\library\requestValidator\CoreRequestValidatorAbs;

abstract class
	DemoFrontRequestValidatorAbs
extends
	CoreRequestValidatorAbs
{

}
