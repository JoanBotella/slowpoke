<?php
declare(strict_types=1);

namespace demo\book\front\library\route;

use slowpoke\core\library\route\CoreRouteItf;
use demo\book\front\library\serviceContainer\DemoFrontServiceContainerItf;

abstract class
	DemoFrontRouteAbs
implements
	CoreRouteItf
{

	private DemoFrontServiceContainerItf $serviceContainer;

	public function __construct(
		DemoFrontServiceContainerItf $serviceContainer
	)
	{
		$this->serviceContainer = $serviceContainer;
	}

	protected function getServiceContainer():DemoFrontServiceContainerItf
	{
		return $this->serviceContainer;
	}

}