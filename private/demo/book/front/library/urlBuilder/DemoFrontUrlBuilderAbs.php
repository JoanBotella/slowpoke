<?php
declare(strict_types=1);

namespace demo\book\front\library\urlBuilder;

use demo\book\front\library\translation\DemoFrontTranslationItf;
use demo\book\front\service\translationContainer\DemoFrontTranslationContainerItf;

abstract class
	DemoFrontUrlBuilderAbs
{

	private DemoFrontTranslationContainerItf $frontTranslationContainer;

	public function __construct(
		DemoFrontTranslationContainerItf $frontTranslationContainer
	)
	{
		$this->frontTranslationContainer = $frontTranslationContainer;
	}

	protected function getTranslation(string $languageCode):DemoFrontTranslationItf
	{
		return $this->frontTranslationContainer->getTranslation($languageCode);
	}

}