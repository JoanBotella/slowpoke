<?php
declare(strict_types=1);

namespace demo\book\front\library\requestMatcher;

use slowpoke\core\library\requestMatcher\CoreRequestMatcherAbs;
use demo\book\front\library\translation\DemoFrontTranslationItf;
use demo\book\front\service\translationContainer\DemoFrontTranslationContainerItf;

abstract class
	DemoFrontRequestMatcherAbs
extends
	CoreRequestMatcherAbs
{

	private DemoFrontTranslationContainerItf $frontTranslationContainer;

	public function __construct(
		DemoFrontTranslationContainerItf $frontTranslationContainer
	)
	{
		$this->frontTranslationContainer = $frontTranslationContainer;
	}

	protected function getTranslation(string $languageCode):DemoFrontTranslationItf
	{
		return $this->frontTranslationContainer->getTranslation($languageCode);
	}

}
