<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\service\widget\main\library\translation;

interface
	DemoFrontStatus404MainWidgetTranslationItf
{

	public function demoFrontStatus404MainWidget_h1Content():string;

	public function demoFrontStatus404MainWidget_pContent():string;

}