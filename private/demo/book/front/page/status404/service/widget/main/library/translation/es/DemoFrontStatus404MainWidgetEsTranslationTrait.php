<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\service\widget\main\library\translation\es;

trait
	DemoFrontStatus404MainWidgetEsTranslationTrait
{

	public function demoFrontStatus404MainWidget_h1Content():string { return 'No encontrado'; }

	public function demoFrontStatus404MainWidget_pContent():string { return 'Ésta es la página No Encontrado'; }

}