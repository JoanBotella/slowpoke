<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\service\widget\main\library\translation\en;

trait
	DemoFrontStatus404MainWidgetEnTranslationTrait
{

	public function demoFrontStatus404MainWidget_h1Content():string { return 'Not Found'; }

	public function demoFrontStatus404MainWidget_pContent():string { return 'This is the Not Found page'; }

}