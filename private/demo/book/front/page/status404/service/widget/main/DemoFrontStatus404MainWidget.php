<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\service\widget\main;

use demo\book\front\page\status404\service\widget\main\DemoFrontStatus404MainWidgetContext;
use demo\book\front\page\status404\service\widget\main\DemoFrontStatus404MainWidgetItf;

final class
	DemoFrontStatus404MainWidget
implements
	DemoFrontStatus404MainWidgetItf
{

	public function render(DemoFrontStatus404MainWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/demoFrontStatus404MainWidgetTemplate.php';
		return ob_get_clean();
	}

}