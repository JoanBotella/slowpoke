<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\service\widget\main;

use demo\book\front\page\status404\library\translation\DemoFrontStatus404TranslationItf;

final class
	DemoFrontStatus404MainWidgetContext
{

	private DemoFrontStatus404TranslationItf $translation;

	public function __construct(
		DemoFrontStatus404TranslationItf $translation
	)
	{
		$this->translation = $translation;
	}

	public function getTranslation():DemoFrontStatus404TranslationItf
	{
		return $this->translation;
	}

}