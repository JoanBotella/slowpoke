<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\service\widget\main;

use demo\book\front\page\status404\service\widget\main\DemoFrontStatus404MainWidgetContext;

interface
	DemoFrontStatus404MainWidgetItf
{

	public function render(DemoFrontStatus404MainWidgetContext $context):string;

}