<?php
declare(strict_types=1);

use demo\book\front\page\status404\service\widget\main\DemoFrontStatus404MainWidgetContext;

/**
 * @var DemoFrontStatus404MainWidgetContext $context
 */

$translation = $context->getTranslation();

?><main data-widget="demo-front-status_404-main">

	<h1><?= $translation->demoFrontStatus404MainWidget_h1Content() ?></h1>

	<p><?= $translation->demoFrontStatus404MainWidget_pContent() ?></p>

</main>