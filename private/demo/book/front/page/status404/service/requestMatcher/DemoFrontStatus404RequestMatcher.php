<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\service\requestMatcher;

use demo\book\front\page\status404\service\requestMatcher\DemoFrontStatus404RequestMatcherItf;
use demo\book\front\library\requestMatcher\DemoFrontRequestMatcherAbs;
use slowpoke\core\library\request\CoreRequest;

final class
	DemoFrontStatus404RequestMatcher
extends
	DemoFrontRequestMatcherAbs
implements
	DemoFrontStatus404RequestMatcherItf
{

	public function isMatch(CoreRequest $coreRequest):bool
	{
		$languageCode = $coreRequest->getLanguageCode();
		$translation = $this->getTranslation($languageCode);

		return
			$coreRequest->getPath() ===
				$languageCode
				.'/'
				.$translation->demoFrontStatus404Page_slug()
		;
	}

}
