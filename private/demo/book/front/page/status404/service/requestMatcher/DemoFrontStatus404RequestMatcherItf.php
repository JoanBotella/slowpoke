<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\service\requestMatcher;

use slowpoke\core\library\requestMatcher\CoreRequestMatcherItf;

interface
	DemoFrontStatus404RequestMatcherItf
extends
	CoreRequestMatcherItf
{

}