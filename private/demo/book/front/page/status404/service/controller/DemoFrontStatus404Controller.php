<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\service\controller;

use demo\book\front\page\status404\service\controller\DemoFrontStatus404ControllerItf;
use demo\book\front\library\controller\DemoFrontControllerAbs;
use demo\book\front\page\status404\service\responseBuilder\DemoFrontStatus404ResponseBuilderItf;
use slowpoke\core\library\response\CoreResponse;
use slowpoke\session\service\sessionManager\SessionSessionManagerItf;
use slowpoke\session\service\translationContainer\SessionTranslationContainerItf;

final class
	DemoFrontStatus404Controller
extends
	DemoFrontControllerAbs
implements
	DemoFrontStatus404ControllerItf
{

	private DemoFrontStatus404ResponseBuilderItf $demoFrontStatus404ResponseBuilder;

	public function __construct(
		SessionSessionManagerItf $sessionSessionManager,
		SessionTranslationContainerItf $sessionTranslationContainer,

		DemoFrontStatus404ResponseBuilderItf $demoFrontStatus404ResponseBuilder
	)
	{
		parent::__construct(
			$sessionSessionManager,
			$sessionTranslationContainer
		);
		$this->demoFrontStatus404ResponseBuilder = $demoFrontStatus404ResponseBuilder;
	}

	protected function buildResponse():CoreResponse
	{
		$coreRequest = $this->getCoreRequest();
		return $this->demoFrontStatus404ResponseBuilder->build(
			$coreRequest->getLanguageCode(),
			$this->getCoreNotifications()
		);
	}

}
