<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\service\controller;

use slowpoke\core\library\controller\CoreControllerItf;

interface
	DemoFrontStatus404ControllerItf
extends
	CoreControllerItf
{

}