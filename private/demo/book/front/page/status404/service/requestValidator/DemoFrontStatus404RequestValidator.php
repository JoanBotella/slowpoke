<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\service\requestValidator;

use demo\book\front\page\status404\service\requestValidator\DemoFrontStatus404RequestValidatorItf;
use demo\book\front\library\requestValidator\DemoFrontRequestValidatorAbs;
use slowpoke\core\library\request\CoreRequest;

final class
	DemoFrontStatus404RequestValidator
extends
	DemoFrontRequestValidatorAbs
implements
	DemoFrontStatus404RequestValidatorItf
{

	public function isValid(CoreRequest $coreRequest):bool
	{
		return true;	// !!!
	}

}
