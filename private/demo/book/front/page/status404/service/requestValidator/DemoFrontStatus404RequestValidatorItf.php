<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\service\requestValidator;

use slowpoke\core\library\requestValidator\CoreRequestValidatorItf;

interface
	DemoFrontStatus404RequestValidatorItf
extends
	CoreRequestValidatorItf
{

}