<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\service\responseBuilder;

use slowpoke\core\library\CoreNotifications;
use demo\book\front\page\status404\service\responseBuilder\DemoFrontStatus404ResponseBuilderItf;
use demo\book\front\library\responseBuilder\DemoFrontResponseBuilderAbs;
use demo\book\front\page\status404\service\widget\main\DemoFrontStatus404MainWidgetContext;
use demo\book\front\page\status404\service\widget\main\DemoFrontStatus404MainWidgetItf;
use demo\book\front\page\home\service\urlBuilder\DemoFrontHomeUrlBuilderItf;
use demo\book\front\service\widget\body\DemoFrontBodyWidgetItf;
use demo\book\front\service\widget\footer\DemoFrontFooterWidgetItf;
use demo\book\front\service\widget\header\DemoFrontHeaderWidgetItf;
use demo\book\front\service\widget\nav\DemoFrontNavWidgetItf;
use slowpoke\core\library\response\CoreResponse;
use slowpoke\core\service\widget\html\CoreHtmlWidgetItf;
use slowpoke\core\service\widget\notifications\CoreNotificationsWidgetItf;
use demo\book\front\page\about\service\urlBuilder\DemoFrontAboutUrlBuilderItf;
use demo\book\front\service\translationContainer\DemoFrontTranslationContainerItf;
use demo\book\front\service\widget\aside\DemoFrontAsideWidgetItf;
use slowpoke\core\library\http\CoreHttpStatusConstant;

final class
	DemoFrontStatus404ResponseBuilder
extends
	DemoFrontResponseBuilderAbs
implements
	DemoFrontStatus404ResponseBuilderItf
{

	private DemoFrontStatus404MainWidgetItf $demoFrontStatus404MainWidget;

	private string $languageCode;

	private CoreNotifications $coreNotifications;

	public function __construct(
		string $protocol,
		string $domain,
		string $basePath,
		bool $useMinifiedScripts,
		bool $useMinifiedStyles,
		CoreHtmlWidgetItf $htmlWidget,

		string $applicationName,
		DemoFrontTranslationContainerItf $demoFrontTranslationContainer,
		DemoFrontHeaderWidgetItf $demoFrontHeaderWidget,
		DemoFrontNavWidgetItf $demoFrontNavWidgetItf,
		CoreNotificationsWidgetItf $coreNotificationsWidget,
		DemoFrontAsideWidgetItf $demoFrontAsideWidget,
		DemoFrontFooterWidgetItf $demoFrontFooterWidget,
		DemoFrontBodyWidgetItf $demoFrontBodyWidget,
		DemoFrontHomeUrlBuilderItf $demoFrontHomeUrlBuilder,
		DemoFrontAboutUrlBuilderItf $demoFrontAboutUrlBuilder,

		DemoFrontStatus404MainWidgetItf $demoFrontStatus404MainWidget
	)
	{
		parent::__construct(
			$protocol,
			$domain,
			$basePath,
			$useMinifiedScripts,
			$useMinifiedStyles,
			$htmlWidget,

			$applicationName,
			$demoFrontTranslationContainer,
			$demoFrontHeaderWidget,
			$demoFrontNavWidgetItf,
			$coreNotificationsWidget,
			$demoFrontAsideWidget,
			$demoFrontFooterWidget,
			$demoFrontBodyWidget,
			$demoFrontHomeUrlBuilder,
			$demoFrontAboutUrlBuilder
		);
		$this->demoFrontStatus404MainWidget = $demoFrontStatus404MainWidget;
	}

	public function build(
		string $languageCode,
		CoreNotifications $coreNotifications
	):CoreResponse
	{
		$this->languageCode = $languageCode;
		$this->coreNotifications = $coreNotifications;

		$coreResponse = $this->buildCoreResponse();

		$this->reset();

		return $coreResponse;
	}

		private function reset():void
		{
			unset($this->languageCode);
		}

	protected function getLanguageCode():string
	{
		return $this->languageCode;
	}

	protected function getPageCode():string
	{
		return 'demo-front-status_404';
	}

	protected function getHttpStatusCode():int
	{
		return CoreHttpStatusConstant::NOT_FOUND;
	}

	protected function buildMainElement():string
	{
		$context = new DemoFrontStatus404MainWidgetContext(
			$this->getDemoFrontTranslation()
		);	
		return $this->demoFrontStatus404MainWidget->render($context);
	}

	protected function getPageName():string
	{
		return $this->getDemoFrontTranslation()->demoFrontStatus404Page_pageName();
	}

	protected function getCoreNotifications():CoreNotifications
	{
		return $this->coreNotifications;
	}

}
