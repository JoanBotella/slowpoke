<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\service\urlBuilder;

use demo\book\front\library\urlBuilder\DemoFrontUrlBuilderAbs;
use demo\book\front\page\status404\service\urlBuilder\DemoFrontStatus404UrlBuilderItf;

final class
	DemoFrontStatus404UrlBuilder
extends
	DemoFrontUrlBuilderAbs
implements
	DemoFrontStatus404UrlBuilderItf
{

	public function build(string $languageCode):string
	{
		$translation = $this->getTranslation($languageCode);

		return
			$languageCode
			.'/'
			.$translation->demoFrontStatus404Page_slug()
		;
	}

}