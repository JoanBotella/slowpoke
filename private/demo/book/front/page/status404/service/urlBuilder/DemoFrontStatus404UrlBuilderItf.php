<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\service\urlBuilder;

interface
	DemoFrontStatus404UrlBuilderItf
{

	public function build(string $languageCode):string;

}