<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\library\serviceContainer;

use demo\book\front\page\status404\service\controller\DemoFrontStatus404ControllerItf;
use demo\book\front\page\status404\service\requestMatcher\DemoFrontStatus404RequestMatcherItf;
use demo\book\front\page\status404\service\requestValidator\DemoFrontStatus404RequestValidatorItf;
use demo\book\front\page\status404\service\responseBuilder\DemoFrontStatus404ResponseBuilderItf;
use demo\book\front\page\status404\service\urlBuilder\DemoFrontStatus404UrlBuilderItf;
use demo\book\front\page\status404\service\widget\main\DemoFrontStatus404MainWidgetItf;

interface
	DemoFrontStatus404ServiceContainerItf
{

	public function getDemoFrontStatus404Controller():DemoFrontStatus404ControllerItf;

	public function getDemoFrontStatus404MainWidget():DemoFrontStatus404MainWidgetItf;

	public function getDemoFrontStatus404RequestMatcher():DemoFrontStatus404RequestMatcherItf;

	public function getDemoFrontStatus404RequestValidator():DemoFrontStatus404RequestValidatorItf;

	public function getDemoFrontStatus404ResponseBuilder():DemoFrontStatus404ResponseBuilderItf;

	public function getDemoFrontStatus404UrlBuilder():DemoFrontStatus404UrlBuilderItf;

}