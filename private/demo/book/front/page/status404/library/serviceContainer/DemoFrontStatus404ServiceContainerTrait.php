<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\library\serviceContainer;

use demo\book\front\page\status404\service\controller\DemoFrontStatus404Controller;
use demo\book\front\page\status404\service\controller\DemoFrontStatus404ControllerItf;
use demo\book\front\page\status404\service\requestMatcher\DemoFrontStatus404RequestMatcher;
use demo\book\front\page\status404\service\requestMatcher\DemoFrontStatus404RequestMatcherItf;
use demo\book\front\page\status404\service\requestValidator\DemoFrontStatus404RequestValidator;
use demo\book\front\page\status404\service\requestValidator\DemoFrontStatus404RequestValidatorItf;
use demo\book\front\page\status404\service\responseBuilder\DemoFrontStatus404ResponseBuilder;
use demo\book\front\page\status404\service\responseBuilder\DemoFrontStatus404ResponseBuilderItf;
use demo\book\front\page\status404\service\urlBuilder\DemoFrontStatus404UrlBuilder;
use demo\book\front\page\status404\service\urlBuilder\DemoFrontStatus404UrlBuilderItf;
use demo\book\front\page\status404\service\widget\main\DemoFrontStatus404MainWidget;
use demo\book\front\page\status404\service\widget\main\DemoFrontStatus404MainWidgetItf;

trait
	DemoFrontStatus404ServiceContainerTrait
{

	private DemoFrontStatus404ControllerItf $demoFrontStatus404Controller;

	public function getDemoFrontStatus404Controller():DemoFrontStatus404ControllerItf
	{
		if (!isset($this->demoFrontStatus404Controller))
		{
			$this->demoFrontStatus404Controller = $this->buildDemoFrontStatus404Controller();
		}
		return $this->demoFrontStatus404Controller;
	}

		protected function buildDemoFrontStatus404Controller():DemoFrontStatus404ControllerItf
		{
			return new DemoFrontStatus404Controller(
				$this->getSessionSessionManager(),
				$this->getSessionTranslationContainer(),

				$this->getDemoFrontStatus404ResponseBuilder()
			);
		}

	private DemoFrontStatus404MainWidgetItf $demoFrontStatus404MainWidget;

	public function getDemoFrontStatus404MainWidget():DemoFrontStatus404MainWidgetItf
	{
		if (!isset($this->demoFrontStatus404MainWidget))
		{
			$this->demoFrontStatus404MainWidget = $this->buildDemoFrontStatus404MainWidget();
		}
		return $this->demoFrontStatus404MainWidget;
	}

		protected function buildDemoFrontStatus404MainWidget():DemoFrontStatus404MainWidgetItf
		{
			return new DemoFrontStatus404MainWidget(
			);
		}

	private DemoFrontStatus404RequestMatcherItf $demoFrontStatus404RequestMatcher;

	public function getDemoFrontStatus404RequestMatcher():DemoFrontStatus404RequestMatcherItf
	{
		if (!isset($this->demoFrontStatus404RequestMatcher))
		{
			$this->demoFrontStatus404RequestMatcher = $this->buildDemoFrontStatus404RequestMatcher();
		}
		return $this->demoFrontStatus404RequestMatcher;
	}

		protected function buildDemoFrontStatus404RequestMatcher():DemoFrontStatus404RequestMatcherItf
		{
			return new DemoFrontStatus404RequestMatcher(
				$this->getDemoFrontTranslationContainer()
			);
		}

	private DemoFrontStatus404RequestValidatorItf $demoFrontStatus404RequestValidator;

	public function getDemoFrontStatus404RequestValidator():DemoFrontStatus404RequestValidatorItf
	{
		if (!isset($this->demoFrontStatus404RequestValidator))
		{
			$this->demoFrontStatus404RequestValidator = $this->buildDemoFrontStatus404RequestValidator();
		}
		return $this->demoFrontStatus404RequestValidator;
	}

		protected function buildDemoFrontStatus404RequestValidator():DemoFrontStatus404RequestValidatorItf
		{
			return new DemoFrontStatus404RequestValidator(
			);
		}

	private DemoFrontStatus404ResponseBuilderItf $demoFrontStatus404ResponseBuilder;

	public function getDemoFrontStatus404ResponseBuilder():DemoFrontStatus404ResponseBuilderItf
	{
		if (!isset($this->demoFrontStatus404ResponseBuilder))
		{
			$this->demoFrontStatus404ResponseBuilder = $this->buildDemoFrontStatus404ResponseBuilder();
		}
		return $this->demoFrontStatus404ResponseBuilder;
	}

		protected function buildDemoFrontStatus404ResponseBuilder():DemoFrontStatus404ResponseBuilderItf
		{
			return new DemoFrontStatus404ResponseBuilder(
				$this->configuration_core_protocol(),
				$this->configuration_core_domain(),
				$this->configuration_core_basePath(),
				$this->configuration_core_useMinifiedScripts(),
				$this->configuration_core_useMinifiedStyles(),
				$this->getCoreHtmlWidget(),
	
				$this->configuration_core_applicationName(),
				$this->getDemoFrontTranslationContainer(),
				$this->getDemoFrontHeaderWidget(),
				$this->getDemoFrontNavWidget(),
				$this->getCoreNotificationsWidget(),
				$this->getDemoFrontAsideWidget(),
				$this->getDemoFrontFooterWidget(),
				$this->getDemoFrontBodyWidget(),
				$this->getDemoFrontHomeUrlBuilder(),
				$this->getDemoFrontAboutUrlBuilder(),
		
				$this->getDemoFrontStatus404MainWidget()
			);
		}

	private DemoFrontStatus404UrlBuilderItf $demoFrontStatus404UrlBuilder;

	public function getDemoFrontStatus404UrlBuilder():DemoFrontStatus404UrlBuilderItf
	{
		if (!isset($this->demoFrontStatus404UrlBuilder))
		{
			$this->demoFrontStatus404UrlBuilder = $this->buildDemoFrontStatus404UrlBuilder();
		}
		return $this->demoFrontStatus404UrlBuilder;
	}

		protected function buildDemoFrontStatus404UrlBuilder():DemoFrontStatus404UrlBuilderItf
		{
			return new DemoFrontStatus404UrlBuilder(
				$this->getDemoFrontTranslationContainer()
			);
		}

}