<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\library\translation;

use demo\book\front\page\status404\service\widget\main\library\translation\DemoFrontStatus404MainWidgetTranslationItf;

interface
	DemoFrontStatus404TranslationItf
extends
	DemoFrontStatus404MainWidgetTranslationItf
{

	public function demoFrontStatus404Page_pageName():string;

	public function demoFrontStatus404Page_slug():string;

}