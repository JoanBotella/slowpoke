<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\library\translation\es;

use demo\book\front\page\status404\service\widget\main\library\translation\es\DemoFrontStatus404MainWidgetEsTranslationTrait;

trait
	DemoFrontStatus404EsTranslationTrait
{
	use
		DemoFrontStatus404MainWidgetEsTranslationTrait
	;

	public function demoFrontStatus404Page_pageName():string { return 'No Encontrado'; }

	public function demoFrontStatus404Page_slug():string { return 'no-encontrado'; }

}
