<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\library\translation\en;

use demo\book\front\page\status404\service\widget\main\library\translation\en\DemoFrontStatus404MainWidgetEnTranslationTrait;

trait
	DemoFrontStatus404EnTranslationTrait
{
	use
		DemoFrontStatus404MainWidgetEnTranslationTrait
	;

	public function demoFrontStatus404Page_pageName():string { return 'Not Found'; }

	public function demoFrontStatus404Page_slug():string { return 'not-found'; }

}
