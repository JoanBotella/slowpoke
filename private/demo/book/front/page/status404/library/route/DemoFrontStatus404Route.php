<?php
declare(strict_types=1);

namespace demo\book\front\page\status404\library\route;

use slowpoke\core\library\controller\CoreControllerItf;
use slowpoke\core\library\requestMatcher\CoreRequestMatcherItf;
use slowpoke\core\library\requestValidator\CoreRequestValidatorItf;
use demo\book\front\library\route\DemoFrontRouteAbs;

final class
	DemoFrontStatus404Route
extends
	DemoFrontRouteAbs
{

	public function getController():CoreControllerItf
	{
		return $this->getServiceContainer()->getDemoFrontStatus404Controller();
	}

	public function getRequestMatcher():CoreRequestMatcherItf
	{
		return $this->getServiceContainer()->getDemoFrontStatus404RequestMatcher();
	}

	public function getRequestValidator():CoreRequestValidatorItf
	{
		return $this->getServiceContainer()->getDemoFrontStatus404RequestValidator();
	}

}
