<?php
declare(strict_types=1);

namespace demo\book\front\page\login\library\serviceContainer;

use demo\book\front\page\login\service\controller\DemoFrontLoginControllerItf;
use demo\book\front\page\login\service\requestMatcher\DemoFrontLoginRequestMatcherItf;
use demo\book\front\page\login\service\requestValidator\DemoFrontLoginRequestValidatorItf;
use demo\book\front\page\login\service\responseBuilder\DemoFrontLoginResponseBuilderItf;
use demo\book\front\page\login\service\urlBuilder\DemoFrontLoginUrlBuilderItf;
use demo\book\front\page\login\service\widget\main\DemoFrontLoginMainWidgetItf;

interface
	DemoFrontLoginServiceContainerItf
{

	public function getDemoFrontLoginController():DemoFrontLoginControllerItf;

	public function getDemoFrontLoginMainWidget():DemoFrontLoginMainWidgetItf;

	public function getDemoFrontLoginRequestMatcher():DemoFrontLoginRequestMatcherItf;

	public function getDemoFrontLoginRequestValidator():DemoFrontLoginRequestValidatorItf;

	public function getDemoFrontLoginResponseBuilder():DemoFrontLoginResponseBuilderItf;

	public function getDemoFrontLoginUrlBuilder():DemoFrontLoginUrlBuilderItf;

}