<?php
declare(strict_types=1);

namespace demo\book\front\page\login\library\serviceContainer;

use demo\book\front\page\login\service\controller\DemoFrontLoginController;
use demo\book\front\page\login\service\controller\DemoFrontLoginControllerItf;
use demo\book\front\page\login\service\requestMatcher\DemoFrontLoginRequestMatcher;
use demo\book\front\page\login\service\requestMatcher\DemoFrontLoginRequestMatcherItf;
use demo\book\front\page\login\service\requestValidator\DemoFrontLoginRequestValidator;
use demo\book\front\page\login\service\requestValidator\DemoFrontLoginRequestValidatorItf;
use demo\book\front\page\login\service\responseBuilder\DemoFrontLoginResponseBuilder;
use demo\book\front\page\login\service\responseBuilder\DemoFrontLoginResponseBuilderItf;
use demo\book\front\page\login\service\urlBuilder\DemoFrontLoginUrlBuilder;
use demo\book\front\page\login\service\urlBuilder\DemoFrontLoginUrlBuilderItf;
use demo\book\front\page\login\service\widget\main\DemoFrontLoginMainWidget;
use demo\book\front\page\login\service\widget\main\DemoFrontLoginMainWidgetItf;

trait
	DemoFrontLoginServiceContainerTrait
{

	private DemoFrontLoginControllerItf $demoFrontLoginController;

	public function getDemoFrontLoginController():DemoFrontLoginControllerItf
	{
		if (!isset($this->demoFrontLoginController))
		{
			$this->demoFrontLoginController = $this->buildDemoFrontLoginController();
		}
		return $this->demoFrontLoginController;
	}

		protected function buildDemoFrontLoginController():DemoFrontLoginControllerItf
		{
			return new DemoFrontLoginController(
				$this->getSessionSessionManager(),
				$this->getSessionTranslationContainer(),

				$this->getDemoFrontLoginResponseBuilder()
			);
		}

	private DemoFrontLoginMainWidgetItf $demoFrontLoginMainWidget;

	public function getDemoFrontLoginMainWidget():DemoFrontLoginMainWidgetItf
	{
		if (!isset($this->demoFrontLoginMainWidget))
		{
			$this->demoFrontLoginMainWidget = $this->buildDemoFrontLoginMainWidget();
		}
		return $this->demoFrontLoginMainWidget;
	}

		protected function buildDemoFrontLoginMainWidget():DemoFrontLoginMainWidgetItf
		{
			return new DemoFrontLoginMainWidget(
			);
		}

	private DemoFrontLoginRequestMatcherItf $demoFrontLoginRequestMatcher;

	public function getDemoFrontLoginRequestMatcher():DemoFrontLoginRequestMatcherItf
	{
		if (!isset($this->demoFrontLoginRequestMatcher))
		{
			$this->demoFrontLoginRequestMatcher = $this->buildDemoFrontLoginRequestMatcher();
		}
		return $this->demoFrontLoginRequestMatcher;
	}

		protected function buildDemoFrontLoginRequestMatcher():DemoFrontLoginRequestMatcherItf
		{
			return new DemoFrontLoginRequestMatcher(
				$this->getDemoFrontTranslationContainer()
			);
		}

	private DemoFrontLoginRequestValidatorItf $demoFrontLoginRequestValidator;

	public function getDemoFrontLoginRequestValidator():DemoFrontLoginRequestValidatorItf
	{
		if (!isset($this->demoFrontLoginRequestValidator))
		{
			$this->demoFrontLoginRequestValidator = $this->buildDemoFrontLoginRequestValidator();
		}
		return $this->demoFrontLoginRequestValidator;
	}

		protected function buildDemoFrontLoginRequestValidator():DemoFrontLoginRequestValidatorItf
		{
			return new DemoFrontLoginRequestValidator(
			);
		}

	private DemoFrontLoginResponseBuilderItf $demoFrontLoginResponseBuilder;

	public function getDemoFrontLoginResponseBuilder():DemoFrontLoginResponseBuilderItf
	{
		if (!isset($this->demoFrontLoginResponseBuilder))
		{
			$this->demoFrontLoginResponseBuilder = $this->buildDemoFrontLoginResponseBuilder();
		}
		return $this->demoFrontLoginResponseBuilder;
	}

		protected function buildDemoFrontLoginResponseBuilder():DemoFrontLoginResponseBuilderItf
		{
			return new DemoFrontLoginResponseBuilder(
				$this->configuration_core_protocol(),
				$this->configuration_core_domain(),
				$this->configuration_core_basePath(),
				$this->configuration_core_useMinifiedScripts(),
				$this->configuration_core_useMinifiedStyles(),
				$this->getCoreHtmlWidget(),
	
				$this->configuration_core_applicationName(),
				$this->getDemoFrontTranslationContainer(),
				$this->getDemoFrontHeaderWidget(),
				$this->getDemoFrontNavWidget(),
				$this->getCoreNotificationsWidget(),
				$this->getDemoFrontAsideWidget(),
				$this->getDemoFrontFooterWidget(),
				$this->getDemoFrontBodyWidget(),
				$this->getDemoFrontHomeUrlBuilder(),
				$this->getDemoFrontAboutUrlBuilder(),
		
				$this->getDemoFrontLoginMainWidget()
			);
		}

	private DemoFrontLoginUrlBuilderItf $demoFrontLoginUrlBuilder;

	public function getDemoFrontLoginUrlBuilder():DemoFrontLoginUrlBuilderItf
	{
		if (!isset($this->demoFrontLoginUrlBuilder))
		{
			$this->demoFrontLoginUrlBuilder = $this->buildDemoFrontLoginUrlBuilder();
		}
		return $this->demoFrontLoginUrlBuilder;
	}

		protected function buildDemoFrontLoginUrlBuilder():DemoFrontLoginUrlBuilderItf
		{
			return new DemoFrontLoginUrlBuilder(
				$this->getDemoFrontTranslationContainer()
			);
		}

}