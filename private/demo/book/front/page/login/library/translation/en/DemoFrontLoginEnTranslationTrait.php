<?php
declare(strict_types=1);

namespace demo\book\front\page\login\library\translation\en;

use demo\book\front\page\login\service\widget\main\library\translation\en\DemoFrontLoginMainWidgetEnTranslationTrait;

trait
	DemoFrontLoginEnTranslationTrait
{
	use
		DemoFrontLoginMainWidgetEnTranslationTrait
	;

	public function demoFrontLoginPage_pageName():string { return 'Login'; }

	public function demoFrontLoginPage_slug():string { return 'login'; }

}
