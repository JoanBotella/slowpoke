<?php
declare(strict_types=1);

namespace demo\book\front\page\login\library\translation\es;

use demo\book\front\page\login\service\widget\main\library\translation\es\DemoFrontLoginMainWidgetEsTranslationTrait;

trait
	DemoFrontLoginEsTranslationTrait
{
	use
		DemoFrontLoginMainWidgetEsTranslationTrait
	;

	public function demoFrontLoginPage_pageName():string { return 'Entrar'; }

	public function demoFrontLoginPage_slug():string { return 'entrar'; }

}
