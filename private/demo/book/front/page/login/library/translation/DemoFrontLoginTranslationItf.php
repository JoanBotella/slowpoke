<?php
declare(strict_types=1);

namespace demo\book\front\page\login\library\translation;

use demo\book\front\page\login\service\widget\main\library\translation\DemoFrontLoginMainWidgetTranslationItf;

interface
	DemoFrontLoginTranslationItf
extends
	DemoFrontLoginMainWidgetTranslationItf
{

	public function demoFrontLoginPage_pageName():string;

	public function demoFrontLoginPage_slug():string;

}