<?php
declare(strict_types=1);

namespace demo\book\front\page\login\library\route;

use slowpoke\core\library\controller\CoreControllerItf;
use slowpoke\core\library\requestMatcher\CoreRequestMatcherItf;
use slowpoke\core\library\requestValidator\CoreRequestValidatorItf;
use demo\book\front\library\route\DemoFrontRouteAbs;

final class
	DemoFrontLoginRoute
extends
	DemoFrontRouteAbs
{

	public function getController():CoreControllerItf
	{
		return $this->getServiceContainer()->getDemoFrontLoginController();
	}

	public function getRequestMatcher():CoreRequestMatcherItf
	{
		return $this->getServiceContainer()->getDemoFrontLoginRequestMatcher();
	}

	public function getRequestValidator():CoreRequestValidatorItf
	{
		return $this->getServiceContainer()->getDemoFrontLoginRequestValidator();
	}

}
