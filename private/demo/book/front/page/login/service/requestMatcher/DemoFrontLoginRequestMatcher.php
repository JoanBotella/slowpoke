<?php
declare(strict_types=1);

namespace demo\book\front\page\login\service\requestMatcher;

use demo\book\front\page\login\service\requestMatcher\DemoFrontLoginRequestMatcherItf;
use demo\book\front\library\requestMatcher\DemoFrontRequestMatcherAbs;
use slowpoke\core\library\request\CoreRequest;

final class
	DemoFrontLoginRequestMatcher
extends
	DemoFrontRequestMatcherAbs
implements
	DemoFrontLoginRequestMatcherItf
{

	public function isMatch(CoreRequest $coreRequest):bool
	{
		$languageCode = $coreRequest->getLanguageCode();
		$translation = $this->getTranslation($languageCode);

		return
			$coreRequest->getPath() ===
				$languageCode
				.'/'
				.$translation->demoFrontLoginPage_slug()
		;
	}

}
