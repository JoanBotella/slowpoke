<?php
declare(strict_types=1);

namespace demo\book\front\page\login\service\requestMatcher;

use slowpoke\core\library\requestMatcher\CoreRequestMatcherItf;

interface
	DemoFrontLoginRequestMatcherItf
extends
	CoreRequestMatcherItf
{

}