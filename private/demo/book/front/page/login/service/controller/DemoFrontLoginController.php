<?php
declare(strict_types=1);

namespace demo\book\front\page\login\service\controller;

use demo\book\front\page\login\service\controller\DemoFrontLoginControllerItf;
use demo\book\front\library\controller\DemoFrontControllerAbs;
use demo\book\front\page\login\service\responseBuilder\DemoFrontLoginResponseBuilderItf;
use slowpoke\core\library\response\CoreResponse;
use slowpoke\session\service\sessionManager\SessionSessionManagerItf;
use slowpoke\session\service\translationContainer\SessionTranslationContainerItf;

final class
	DemoFrontLoginController
extends
	DemoFrontControllerAbs
implements
	DemoFrontLoginControllerItf
{

	private DemoFrontLoginResponseBuilderItf $demoFrontLoginResponseBuilder;

	public function __construct(
		SessionSessionManagerItf $sessionSessionManager,
		SessionTranslationContainerItf $sessionTranslationContainer,

		DemoFrontLoginResponseBuilderItf $demoFrontLoginResponseBuilder
	)
	{
		parent::__construct(
			$sessionSessionManager,
			$sessionTranslationContainer
		);
		$this->demoFrontLoginResponseBuilder = $demoFrontLoginResponseBuilder;
	}

	protected function buildResponse():CoreResponse
	{
		$coreRequest = $this->getCoreRequest();
		return $this->demoFrontLoginResponseBuilder->build(
			$coreRequest->getLanguageCode(),
			$this->getCoreNotifications()
		);
	}

}
