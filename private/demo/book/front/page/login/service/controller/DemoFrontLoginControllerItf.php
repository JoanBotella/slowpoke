<?php
declare(strict_types=1);

namespace demo\book\front\page\login\service\controller;

use slowpoke\core\library\controller\CoreControllerItf;

interface
	DemoFrontLoginControllerItf
extends
	CoreControllerItf
{

}