<?php
declare(strict_types=1);

namespace demo\book\front\page\login\service\requestValidator;

use slowpoke\core\library\requestValidator\CoreRequestValidatorItf;

interface
	DemoFrontLoginRequestValidatorItf
extends
	CoreRequestValidatorItf
{

}