<?php
declare(strict_types=1);

namespace demo\book\front\page\login\service\requestValidator;

use demo\book\front\page\login\service\requestValidator\DemoFrontLoginRequestValidatorItf;
use demo\book\front\library\requestValidator\DemoFrontRequestValidatorAbs;
use slowpoke\core\library\request\CoreRequest;

final class
	DemoFrontLoginRequestValidator
extends
	DemoFrontRequestValidatorAbs
implements
	DemoFrontLoginRequestValidatorItf
{

	public function isValid(CoreRequest $coreRequest):bool
	{
		return true;	// !!!
	}

}
