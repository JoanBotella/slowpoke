<?php
declare(strict_types=1);

namespace demo\book\front\page\login\service\urlBuilder;

interface
	DemoFrontLoginUrlBuilderItf
{

	public function build(string $languageCode):string;

}