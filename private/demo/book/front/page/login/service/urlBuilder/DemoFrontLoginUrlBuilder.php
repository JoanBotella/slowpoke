<?php
declare(strict_types=1);

namespace demo\book\front\page\login\service\urlBuilder;

use demo\book\front\library\urlBuilder\DemoFrontUrlBuilderAbs;
use demo\book\front\page\login\service\urlBuilder\DemoFrontLoginUrlBuilderItf;

final class
	DemoFrontLoginUrlBuilder
extends
	DemoFrontUrlBuilderAbs
implements
	DemoFrontLoginUrlBuilderItf
{

	public function build(string $languageCode):string
	{
		$translation = $this->getTranslation($languageCode);

		return
			$languageCode
			.'/'
			.$translation->demoFrontLoginPage_slug()
		;
	}

}