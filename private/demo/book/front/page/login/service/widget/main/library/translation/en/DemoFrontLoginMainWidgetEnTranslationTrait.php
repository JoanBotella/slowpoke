<?php
declare(strict_types=1);

namespace demo\book\front\page\login\service\widget\main\library\translation\en;

trait
	DemoFrontLoginMainWidgetEnTranslationTrait
{

	public function demoFrontLoginMainWidget_h1Content():string { return 'Login'; }

	public function demoFrontLoginMainWidget_pContent():string { return 'This is the Login page'; }

}