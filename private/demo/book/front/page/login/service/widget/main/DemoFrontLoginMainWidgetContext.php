<?php
declare(strict_types=1);

namespace demo\book\front\page\login\service\widget\main;

use demo\book\front\page\login\library\translation\DemoFrontLoginTranslationItf;

final class
	DemoFrontLoginMainWidgetContext
{

	private DemoFrontLoginTranslationItf $translation;

	public function __construct(
		DemoFrontLoginTranslationItf $translation
	)
	{
		$this->translation = $translation;
	}

	public function getTranslation():DemoFrontLoginTranslationItf
	{
		return $this->translation;
	}

}