<?php
declare(strict_types=1);

namespace demo\book\front\page\login\service\widget\main;

use demo\book\front\page\login\service\widget\main\DemoFrontLoginMainWidgetContext;

interface
	DemoFrontLoginMainWidgetItf
{

	public function render(DemoFrontLoginMainWidgetContext $context):string;

}