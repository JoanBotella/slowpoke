<?php
declare(strict_types=1);

namespace demo\book\front\page\login\service\widget\main\library\translation\es;

trait
	DemoFrontLoginMainWidgetEsTranslationTrait
{

	public function demoFrontLoginMainWidget_h1Content():string { return 'Entrar'; }

	public function demoFrontLoginMainWidget_pContent():string { return 'Ésta es la página Entrar'; }

}