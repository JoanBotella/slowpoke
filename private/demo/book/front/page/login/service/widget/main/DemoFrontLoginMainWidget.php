<?php
declare(strict_types=1);

namespace demo\book\front\page\login\service\widget\main;

use demo\book\front\page\login\service\widget\main\DemoFrontLoginMainWidgetContext;
use demo\book\front\page\login\service\widget\main\DemoFrontLoginMainWidgetItf;

final class
	DemoFrontLoginMainWidget
implements
	DemoFrontLoginMainWidgetItf
{

	public function render(DemoFrontLoginMainWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/demoFrontLoginMainWidgetTemplate.php';
		return ob_get_clean();
	}

}