<?php
declare(strict_types=1);

use demo\book\front\page\login\service\widget\main\DemoFrontLoginMainWidgetContext;

/**
 * @var DemoFrontLoginMainWidgetContext $context
 */

$translation = $context->getTranslation();

?><main data-widget="demo-front-login-main">

	<h1><?= $translation->demoFrontLoginMainWidget_h1Content() ?></h1>

	<p><?= $translation->demoFrontLoginMainWidget_pContent() ?></p>

</main>