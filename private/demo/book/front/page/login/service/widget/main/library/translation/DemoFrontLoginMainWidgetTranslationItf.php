<?php
declare(strict_types=1);

namespace demo\book\front\page\login\service\widget\main\library\translation;

interface
	DemoFrontLoginMainWidgetTranslationItf
{

	public function demoFrontLoginMainWidget_h1Content():string;

	public function demoFrontLoginMainWidget_pContent():string;

}