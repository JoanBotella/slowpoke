<?php
declare(strict_types=1);

namespace demo\book\front\page\login\service\responseBuilder;

use slowpoke\core\library\CoreNotifications;
use slowpoke\core\library\response\CoreResponse;

interface
	DemoFrontLoginResponseBuilderItf
{

	public function build(
		string $languageCode,
		CoreNotifications $coreNotifications
	):CoreResponse;

}
