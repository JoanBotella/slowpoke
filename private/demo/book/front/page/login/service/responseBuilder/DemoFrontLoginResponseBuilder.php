<?php
declare(strict_types=1);

namespace demo\book\front\page\login\service\responseBuilder;

use slowpoke\core\library\CoreNotifications;
use demo\book\front\page\login\service\responseBuilder\DemoFrontLoginResponseBuilderItf;
use demo\book\front\library\responseBuilder\DemoFrontResponseBuilderAbs;
use demo\book\front\page\login\service\widget\main\DemoFrontLoginMainWidgetContext;
use demo\book\front\page\login\service\widget\main\DemoFrontLoginMainWidgetItf;
use demo\book\front\page\home\service\urlBuilder\DemoFrontHomeUrlBuilderItf;
use demo\book\front\service\widget\body\DemoFrontBodyWidgetItf;
use demo\book\front\service\widget\footer\DemoFrontFooterWidgetItf;
use demo\book\front\service\widget\header\DemoFrontHeaderWidgetItf;
use demo\book\front\service\widget\nav\DemoFrontNavWidgetItf;
use slowpoke\core\library\response\CoreResponse;
use slowpoke\core\service\widget\html\CoreHtmlWidgetItf;
use slowpoke\core\service\widget\notifications\CoreNotificationsWidgetItf;
use demo\book\front\page\about\service\urlBuilder\DemoFrontAboutUrlBuilderItf;
use demo\book\front\service\translationContainer\DemoFrontTranslationContainerItf;
use demo\book\front\service\widget\aside\DemoFrontAsideWidgetItf;

final class
	DemoFrontLoginResponseBuilder
extends
	DemoFrontResponseBuilderAbs
implements
	DemoFrontLoginResponseBuilderItf
{

	private DemoFrontLoginMainWidgetItf $demoFrontLoginMainWidget;

	private string $languageCode;

	private CoreNotifications $coreNotifications;

	public function __construct(
		string $protocol,
		string $domain,
		string $basePath,
		bool $useMinifiedScripts,
		bool $useMinifiedStyles,
		CoreHtmlWidgetItf $htmlWidget,

		string $applicationName,
		DemoFrontTranslationContainerItf $demoFrontTranslationContainer,
		DemoFrontHeaderWidgetItf $demoFrontHeaderWidget,
		DemoFrontNavWidgetItf $demoFrontNavWidgetItf,
		CoreNotificationsWidgetItf $coreNotificationsWidget,
		DemoFrontAsideWidgetItf $demoFrontAsideWidget,
		DemoFrontFooterWidgetItf $demoFrontFooterWidget,
		DemoFrontBodyWidgetItf $demoFrontBodyWidget,
		DemoFrontHomeUrlBuilderItf $demoFrontHomeUrlBuilder,
		DemoFrontAboutUrlBuilderItf $demoFrontAboutUrlBuilder,

		DemoFrontLoginMainWidgetItf $demoFrontLoginMainWidget
	)
	{
		parent::__construct(
			$protocol,
			$domain,
			$basePath,
			$useMinifiedScripts,
			$useMinifiedStyles,
			$htmlWidget,

			$applicationName,
			$demoFrontTranslationContainer,
			$demoFrontHeaderWidget,
			$demoFrontNavWidgetItf,
			$coreNotificationsWidget,
			$demoFrontAsideWidget,
			$demoFrontFooterWidget,
			$demoFrontBodyWidget,
			$demoFrontHomeUrlBuilder,
			$demoFrontAboutUrlBuilder
		);
		$this->demoFrontLoginMainWidget = $demoFrontLoginMainWidget;
	}

	public function build(
		string $languageCode,
		CoreNotifications $coreNotifications
	):CoreResponse
	{
		$this->languageCode = $languageCode;
		$this->coreNotifications = $coreNotifications;

		$coreResponse = $this->buildCoreResponse();

		$this->reset();

		return $coreResponse;
	}

		private function reset():void
		{
			unset($this->languageCode);
		}

	protected function getLanguageCode():string
	{
		return $this->languageCode;
	}

	protected function getPageCode():string
	{
		return 'demo-front-login';
	}

	protected function buildMainElement():string
	{
		$context = new DemoFrontLoginMainWidgetContext(
			$this->getDemoFrontTranslation()
		);	
		return $this->demoFrontLoginMainWidget->render($context);
	}

	protected function getPageName():string
	{
		return $this->getDemoFrontTranslation()->demoFrontLoginPage_pageName();
	}

	protected function getCoreNotifications():CoreNotifications
	{
		return $this->coreNotifications;
	}

}
