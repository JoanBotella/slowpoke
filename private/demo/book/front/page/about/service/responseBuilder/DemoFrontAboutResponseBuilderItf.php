<?php
declare(strict_types=1);

namespace demo\book\front\page\about\service\responseBuilder;

use slowpoke\core\library\CoreNotifications;
use slowpoke\core\library\response\CoreResponse;

interface
	DemoFrontAboutResponseBuilderItf
{

	public function build(
		string $languageCode,
		CoreNotifications $coreNotifications
	):CoreResponse;

}
