<?php
declare(strict_types=1);

namespace demo\book\front\page\about\service\requestMatcher;

use slowpoke\core\library\requestMatcher\CoreRequestMatcherItf;

interface
	DemoFrontAboutRequestMatcherItf
extends
	CoreRequestMatcherItf
{

}