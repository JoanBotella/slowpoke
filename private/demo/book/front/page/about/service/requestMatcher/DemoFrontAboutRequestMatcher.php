<?php
declare(strict_types=1);

namespace demo\book\front\page\about\service\requestMatcher;

use demo\book\front\page\about\service\requestMatcher\DemoFrontAboutRequestMatcherItf;
use demo\book\front\library\requestMatcher\DemoFrontRequestMatcherAbs;
use slowpoke\core\library\request\CoreRequest;

final class
	DemoFrontAboutRequestMatcher
extends
	DemoFrontRequestMatcherAbs
implements
	DemoFrontAboutRequestMatcherItf
{

	public function isMatch(CoreRequest $coreRequest):bool
	{
		$languageCode = $coreRequest->getLanguageCode();
		$translation = $this->getTranslation($languageCode);

		return
			$coreRequest->getPath() ===
				$languageCode
				.'/'
				.$translation->demoFrontAboutPage_slug()
		;
	}

}
