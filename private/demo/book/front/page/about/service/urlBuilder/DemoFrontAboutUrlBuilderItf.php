<?php
declare(strict_types=1);

namespace demo\book\front\page\about\service\urlBuilder;

interface
	DemoFrontAboutUrlBuilderItf
{

	public function build(string $languageCode):string;

}