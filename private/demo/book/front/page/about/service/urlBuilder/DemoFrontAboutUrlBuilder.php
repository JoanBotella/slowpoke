<?php
declare(strict_types=1);

namespace demo\book\front\page\about\service\urlBuilder;

use demo\book\front\library\urlBuilder\DemoFrontUrlBuilderAbs;
use demo\book\front\page\about\service\urlBuilder\DemoFrontAboutUrlBuilderItf;

final class
	DemoFrontAboutUrlBuilder
extends
	DemoFrontUrlBuilderAbs
implements
	DemoFrontAboutUrlBuilderItf
{

	public function build(string $languageCode):string
	{
		$translation = $this->getTranslation($languageCode);

		return
			$languageCode
			.'/'
			.$translation->demoFrontAboutPage_slug()
		;
	}

}