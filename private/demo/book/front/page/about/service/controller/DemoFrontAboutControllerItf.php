<?php
declare(strict_types=1);

namespace demo\book\front\page\about\service\controller;

use slowpoke\core\library\controller\CoreControllerItf;

interface
	DemoFrontAboutControllerItf
extends
	CoreControllerItf
{

}