<?php
declare(strict_types=1);

namespace demo\book\front\page\about\service\controller;

use demo\book\front\page\about\service\controller\DemoFrontAboutControllerItf;
use demo\book\front\library\controller\DemoFrontControllerAbs;
use demo\book\front\page\about\service\responseBuilder\DemoFrontAboutResponseBuilderItf;
use slowpoke\core\library\response\CoreResponse;
use slowpoke\session\service\sessionManager\SessionSessionManagerItf;
use slowpoke\session\service\translationContainer\SessionTranslationContainerItf;

final class
	DemoFrontAboutController
extends
	DemoFrontControllerAbs
implements
	DemoFrontAboutControllerItf
{

	private DemoFrontAboutResponseBuilderItf $demoFrontAboutResponseBuilder;

	public function __construct(
		SessionSessionManagerItf $sessionSessionManager,
		SessionTranslationContainerItf $sessionTranslationContainer,

		DemoFrontAboutResponseBuilderItf $demoFrontAboutResponseBuilder
	)
	{
		parent::__construct(
			$sessionSessionManager,
			$sessionTranslationContainer
		);
		$this->demoFrontAboutResponseBuilder = $demoFrontAboutResponseBuilder;
	}

	protected function buildResponse():CoreResponse
	{
		$coreRequest = $this->getCoreRequest();
		return $this->demoFrontAboutResponseBuilder->build(
			$coreRequest->getLanguageCode(),
			$this->getCoreNotifications()
		);
	}

}
