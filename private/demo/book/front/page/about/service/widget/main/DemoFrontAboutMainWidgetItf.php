<?php
declare(strict_types=1);

namespace demo\book\front\page\about\service\widget\main;

use demo\book\front\page\about\service\widget\main\DemoFrontAboutMainWidgetContext;

interface
	DemoFrontAboutMainWidgetItf
{

	public function render(DemoFrontAboutMainWidgetContext $context):string;

}