<?php
declare(strict_types=1);

namespace demo\book\front\page\about\service\widget\main;

use demo\book\front\page\about\library\translation\DemoFrontAboutTranslationItf;

final class
	DemoFrontAboutMainWidgetContext
{

	private DemoFrontAboutTranslationItf $translation;

	public function __construct(
		DemoFrontAboutTranslationItf $translation
	)
	{
		$this->translation = $translation;
	}

	public function getTranslation():DemoFrontAboutTranslationItf
	{
		return $this->translation;
	}

}