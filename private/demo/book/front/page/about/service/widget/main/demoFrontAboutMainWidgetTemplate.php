<?php
declare(strict_types=1);

use demo\book\front\page\about\service\widget\main\DemoFrontAboutMainWidgetContext;

/**
 * @var DemoFrontAboutMainWidgetContext $context
 */

$translation = $context->getTranslation();

?><main data-widget="demo-front-about-main">

	<h1><?= $translation->demoFrontAboutMainWidget_h1Content() ?></h1>

	<p><?= $translation->demoFrontAboutMainWidget_pContent() ?></p>

</main>