<?php
declare(strict_types=1);

namespace demo\book\front\page\about\service\widget\main\library\translation\en;

trait
	DemoFrontAboutMainWidgetEnTranslationTrait
{

	public function demoFrontAboutMainWidget_h1Content():string { return 'About Us'; }

	public function demoFrontAboutMainWidget_pContent():string { return 'This is the About Us page'; }

}