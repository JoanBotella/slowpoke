<?php
declare(strict_types=1);

namespace demo\book\front\page\about\service\widget\main\library\translation\es;

trait
	DemoFrontAboutMainWidgetEsTranslationTrait
{

	public function demoFrontAboutMainWidget_h1Content():string { return 'Sobre Nosotros'; }

	public function demoFrontAboutMainWidget_pContent():string { return 'Ésta es la página Sobre Nosotros'; }

}