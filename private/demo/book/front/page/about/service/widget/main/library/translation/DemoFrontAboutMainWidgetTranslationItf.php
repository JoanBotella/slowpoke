<?php
declare(strict_types=1);

namespace demo\book\front\page\about\service\widget\main\library\translation;

interface
	DemoFrontAboutMainWidgetTranslationItf
{

	public function demoFrontAboutMainWidget_h1Content():string;

	public function demoFrontAboutMainWidget_pContent():string;

}