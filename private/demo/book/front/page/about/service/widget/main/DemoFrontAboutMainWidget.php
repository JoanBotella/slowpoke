<?php
declare(strict_types=1);

namespace demo\book\front\page\about\service\widget\main;

use demo\book\front\page\about\service\widget\main\DemoFrontAboutMainWidgetContext;
use demo\book\front\page\about\service\widget\main\DemoFrontAboutMainWidgetItf;

final class
	DemoFrontAboutMainWidget
implements
	DemoFrontAboutMainWidgetItf
{

	public function render(DemoFrontAboutMainWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/demoFrontAboutMainWidgetTemplate.php';
		return ob_get_clean();
	}

}