<?php
declare(strict_types=1);

namespace demo\book\front\page\about\service\requestValidator;

use slowpoke\core\library\requestValidator\CoreRequestValidatorItf;

interface
	DemoFrontAboutRequestValidatorItf
extends
	CoreRequestValidatorItf
{

}