<?php
declare(strict_types=1);

namespace demo\book\front\page\about\service\requestValidator;

use demo\book\front\page\about\service\requestValidator\DemoFrontAboutRequestValidatorItf;
use demo\book\front\library\requestValidator\DemoFrontRequestValidatorAbs;
use slowpoke\core\library\request\CoreRequest;

final class
	DemoFrontAboutRequestValidator
extends
	DemoFrontRequestValidatorAbs
implements
	DemoFrontAboutRequestValidatorItf
{

	public function isValid(CoreRequest $coreRequest):bool
	{
		return true;	// !!!
	}

}
