<?php
declare(strict_types=1);

namespace demo\book\front\page\about\library\serviceContainer;

use demo\book\front\page\about\service\controller\DemoFrontAboutController;
use demo\book\front\page\about\service\controller\DemoFrontAboutControllerItf;
use demo\book\front\page\about\service\requestMatcher\DemoFrontAboutRequestMatcher;
use demo\book\front\page\about\service\requestMatcher\DemoFrontAboutRequestMatcherItf;
use demo\book\front\page\about\service\requestValidator\DemoFrontAboutRequestValidator;
use demo\book\front\page\about\service\requestValidator\DemoFrontAboutRequestValidatorItf;
use demo\book\front\page\about\service\responseBuilder\DemoFrontAboutResponseBuilder;
use demo\book\front\page\about\service\responseBuilder\DemoFrontAboutResponseBuilderItf;
use demo\book\front\page\about\service\urlBuilder\DemoFrontAboutUrlBuilder;
use demo\book\front\page\about\service\urlBuilder\DemoFrontAboutUrlBuilderItf;
use demo\book\front\page\about\service\widget\main\DemoFrontAboutMainWidget;
use demo\book\front\page\about\service\widget\main\DemoFrontAboutMainWidgetItf;

trait
	DemoFrontAboutServiceContainerTrait
{

	private DemoFrontAboutControllerItf $demoFrontAboutController;

	public function getDemoFrontAboutController():DemoFrontAboutControllerItf
	{
		if (!isset($this->demoFrontAboutController))
		{
			$this->demoFrontAboutController = $this->buildDemoFrontAboutController();
		}
		return $this->demoFrontAboutController;
	}

		protected function buildDemoFrontAboutController():DemoFrontAboutControllerItf
		{
			return new DemoFrontAboutController(
				$this->getSessionSessionManager(),
				$this->getSessionTranslationContainer(),

				$this->getDemoFrontAboutResponseBuilder()
			);
		}

	private DemoFrontAboutMainWidgetItf $demoFrontAboutMainWidget;

	public function getDemoFrontAboutMainWidget():DemoFrontAboutMainWidgetItf
	{
		if (!isset($this->demoFrontAboutMainWidget))
		{
			$this->demoFrontAboutMainWidget = $this->buildDemoFrontAboutMainWidget();
		}
		return $this->demoFrontAboutMainWidget;
	}

		protected function buildDemoFrontAboutMainWidget():DemoFrontAboutMainWidgetItf
		{
			return new DemoFrontAboutMainWidget(
			);
		}

	private DemoFrontAboutRequestMatcherItf $demoFrontAboutRequestMatcher;

	public function getDemoFrontAboutRequestMatcher():DemoFrontAboutRequestMatcherItf
	{
		if (!isset($this->demoFrontAboutRequestMatcher))
		{
			$this->demoFrontAboutRequestMatcher = $this->buildDemoFrontAboutRequestMatcher();
		}
		return $this->demoFrontAboutRequestMatcher;
	}

		protected function buildDemoFrontAboutRequestMatcher():DemoFrontAboutRequestMatcherItf
		{
			return new DemoFrontAboutRequestMatcher(
				$this->getDemoFrontTranslationContainer()
			);
		}

	private DemoFrontAboutRequestValidatorItf $demoFrontAboutRequestValidator;

	public function getDemoFrontAboutRequestValidator():DemoFrontAboutRequestValidatorItf
	{
		if (!isset($this->demoFrontAboutRequestValidator))
		{
			$this->demoFrontAboutRequestValidator = $this->buildDemoFrontAboutRequestValidator();
		}
		return $this->demoFrontAboutRequestValidator;
	}

		protected function buildDemoFrontAboutRequestValidator():DemoFrontAboutRequestValidatorItf
		{
			return new DemoFrontAboutRequestValidator(
			);
		}

	private DemoFrontAboutResponseBuilderItf $demoFrontAboutResponseBuilder;

	public function getDemoFrontAboutResponseBuilder():DemoFrontAboutResponseBuilderItf
	{
		if (!isset($this->demoFrontAboutResponseBuilder))
		{
			$this->demoFrontAboutResponseBuilder = $this->buildDemoFrontAboutResponseBuilder();
		}
		return $this->demoFrontAboutResponseBuilder;
	}

		protected function buildDemoFrontAboutResponseBuilder():DemoFrontAboutResponseBuilderItf
		{
			return new DemoFrontAboutResponseBuilder(
				$this->configuration_core_protocol(),
				$this->configuration_core_domain(),
				$this->configuration_core_basePath(),
				$this->configuration_core_useMinifiedScripts(),
				$this->configuration_core_useMinifiedStyles(),
				$this->getCoreHtmlWidget(),
	
				$this->configuration_core_applicationName(),
				$this->getDemoFrontTranslationContainer(),
				$this->getDemoFrontHeaderWidget(),
				$this->getDemoFrontNavWidget(),
				$this->getCoreNotificationsWidget(),
				$this->getDemoFrontAsideWidget(),
				$this->getDemoFrontFooterWidget(),
				$this->getDemoFrontBodyWidget(),
				$this->getDemoFrontHomeUrlBuilder(),
				$this->getDemoFrontAboutUrlBuilder(),
		
				$this->getDemoFrontAboutMainWidget()
			);
		}

	private DemoFrontAboutUrlBuilderItf $demoFrontAboutUrlBuilder;

	public function getDemoFrontAboutUrlBuilder():DemoFrontAboutUrlBuilderItf
	{
		if (!isset($this->demoFrontAboutUrlBuilder))
		{
			$this->demoFrontAboutUrlBuilder = $this->buildDemoFrontAboutUrlBuilder();
		}
		return $this->demoFrontAboutUrlBuilder;
	}

		protected function buildDemoFrontAboutUrlBuilder():DemoFrontAboutUrlBuilderItf
		{
			return new DemoFrontAboutUrlBuilder(
				$this->getDemoFrontTranslationContainer()
			);
		}

}