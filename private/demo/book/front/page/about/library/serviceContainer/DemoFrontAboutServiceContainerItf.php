<?php
declare(strict_types=1);

namespace demo\book\front\page\about\library\serviceContainer;

use demo\book\front\page\about\service\controller\DemoFrontAboutControllerItf;
use demo\book\front\page\about\service\requestMatcher\DemoFrontAboutRequestMatcherItf;
use demo\book\front\page\about\service\requestValidator\DemoFrontAboutRequestValidatorItf;
use demo\book\front\page\about\service\responseBuilder\DemoFrontAboutResponseBuilderItf;
use demo\book\front\page\about\service\urlBuilder\DemoFrontAboutUrlBuilderItf;
use demo\book\front\page\about\service\widget\main\DemoFrontAboutMainWidgetItf;

interface
	DemoFrontAboutServiceContainerItf
{

	public function getDemoFrontAboutController():DemoFrontAboutControllerItf;

	public function getDemoFrontAboutMainWidget():DemoFrontAboutMainWidgetItf;

	public function getDemoFrontAboutRequestMatcher():DemoFrontAboutRequestMatcherItf;

	public function getDemoFrontAboutRequestValidator():DemoFrontAboutRequestValidatorItf;

	public function getDemoFrontAboutResponseBuilder():DemoFrontAboutResponseBuilderItf;

	public function getDemoFrontAboutUrlBuilder():DemoFrontAboutUrlBuilderItf;

}