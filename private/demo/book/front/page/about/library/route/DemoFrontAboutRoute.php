<?php
declare(strict_types=1);

namespace demo\book\front\page\about\library\route;

use slowpoke\core\library\controller\CoreControllerItf;
use slowpoke\core\library\requestMatcher\CoreRequestMatcherItf;
use slowpoke\core\library\requestValidator\CoreRequestValidatorItf;
use demo\book\front\library\route\DemoFrontRouteAbs;

final class
	DemoFrontAboutRoute
extends
	DemoFrontRouteAbs
{

	public function getController():CoreControllerItf
	{
		return $this->getServiceContainer()->getDemoFrontAboutController();
	}

	public function getRequestMatcher():CoreRequestMatcherItf
	{
		return $this->getServiceContainer()->getDemoFrontAboutRequestMatcher();
	}

	public function getRequestValidator():CoreRequestValidatorItf
	{
		return $this->getServiceContainer()->getDemoFrontAboutRequestValidator();
	}

}
