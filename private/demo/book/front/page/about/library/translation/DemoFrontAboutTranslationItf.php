<?php
declare(strict_types=1);

namespace demo\book\front\page\about\library\translation;

use demo\book\front\page\about\service\widget\main\library\translation\DemoFrontAboutMainWidgetTranslationItf;

interface
	DemoFrontAboutTranslationItf
extends
	DemoFrontAboutMainWidgetTranslationItf
{

	public function demoFrontAboutPage_pageName():string;

	public function demoFrontAboutPage_slug():string;

}