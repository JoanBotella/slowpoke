<?php
declare(strict_types=1);

namespace demo\book\front\page\about\library\translation\en;

use demo\book\front\page\about\service\widget\main\library\translation\en\DemoFrontAboutMainWidgetEnTranslationTrait;

trait
	DemoFrontAboutEnTranslationTrait
{
	use
		DemoFrontAboutMainWidgetEnTranslationTrait
	;

	public function demoFrontAboutPage_pageName():string { return 'About Us'; }

	public function demoFrontAboutPage_slug():string { return 'about-us'; }

}
