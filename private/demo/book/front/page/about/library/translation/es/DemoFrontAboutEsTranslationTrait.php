<?php
declare(strict_types=1);

namespace demo\book\front\page\about\library\translation\es;

use demo\book\front\page\about\service\widget\main\library\translation\es\DemoFrontAboutMainWidgetEsTranslationTrait;

trait
	DemoFrontAboutEsTranslationTrait
{
	use
		DemoFrontAboutMainWidgetEsTranslationTrait
	;

	public function demoFrontAboutPage_pageName():string { return 'Sobre Nosotros'; }

	public function demoFrontAboutPage_slug():string { return 'sobre-nosotros'; }

}
