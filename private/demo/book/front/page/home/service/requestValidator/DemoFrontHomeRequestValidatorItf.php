<?php
declare(strict_types=1);

namespace demo\book\front\page\home\service\requestValidator;

use slowpoke\core\library\requestValidator\CoreRequestValidatorItf;

interface
	DemoFrontHomeRequestValidatorItf
extends
	CoreRequestValidatorItf
{

}