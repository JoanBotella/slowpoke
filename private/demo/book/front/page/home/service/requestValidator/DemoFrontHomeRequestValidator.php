<?php
declare(strict_types=1);

namespace demo\book\front\page\home\service\requestValidator;

use demo\book\front\page\home\service\requestValidator\DemoFrontHomeRequestValidatorItf;
use demo\book\front\library\requestValidator\DemoFrontRequestValidatorAbs;
use slowpoke\core\library\request\CoreRequest;

final class
	DemoFrontHomeRequestValidator
extends
	DemoFrontRequestValidatorAbs
implements
	DemoFrontHomeRequestValidatorItf
{

	public function isValid(CoreRequest $coreRequest):bool
	{
		return true;	// !!!
	}

}
