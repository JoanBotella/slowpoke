<?php
declare(strict_types=1);

namespace demo\book\front\page\home\service\controller;

use demo\book\front\page\home\service\controller\DemoFrontHomeControllerItf;
use demo\book\front\library\controller\DemoFrontControllerAbs;
use demo\book\front\page\home\service\responseBuilder\DemoFrontHomeResponseBuilderItf;
use slowpoke\core\library\response\CoreResponse;
use slowpoke\session\service\sessionManager\SessionSessionManagerItf;
use slowpoke\session\service\translationContainer\SessionTranslationContainerItf;

final class
	DemoFrontHomeController
extends
	DemoFrontControllerAbs
implements
	DemoFrontHomeControllerItf
{

	private DemoFrontHomeResponseBuilderItf $demoFrontHomeResponseBuilder;

	public function __construct(
		SessionSessionManagerItf $sessionSessionManager,
		SessionTranslationContainerItf $sessionTranslationContainer,

		DemoFrontHomeResponseBuilderItf $demoFrontHomeResponseBuilder
	)
	{
		parent::__construct(
			$sessionSessionManager,
			$sessionTranslationContainer
		);
		$this->demoFrontHomeResponseBuilder = $demoFrontHomeResponseBuilder;
	}

	protected function buildResponse():CoreResponse
	{
		$coreRequest = $this->getCoreRequest();
		return $this->demoFrontHomeResponseBuilder->build(
			$coreRequest->getLanguageCode(),
			$coreRequest->getPath(),
			$this->getCoreNotifications()
		);
	}

}
