<?php
declare(strict_types=1);

namespace demo\book\front\page\home\service\controller;

use slowpoke\core\library\controller\CoreControllerItf;

interface
	DemoFrontHomeControllerItf
extends
	CoreControllerItf
{

}