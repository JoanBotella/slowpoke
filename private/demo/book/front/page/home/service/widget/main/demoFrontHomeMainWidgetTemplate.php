<?php
declare(strict_types=1);

use demo\book\front\page\home\service\widget\main\DemoFrontHomeMainWidgetContext;

/**
 * @var DemoFrontHomeMainWidgetContext $context
 */

$translation = $context->getTranslation();

?><main data-widget="demo-front-home-main">

	<h1><?= $translation->demoFrontHomeMainWidget_h1Content() ?></h1>

	<p><?= $translation->demoFrontHomeMainWidget_pContent() ?></p>

</main>