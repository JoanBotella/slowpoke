<?php
declare(strict_types=1);

namespace demo\book\front\page\home\service\widget\main\library\translation;

interface
	DemoFrontHomeMainWidgetTranslationItf
{

	public function demoFrontHomeMainWidget_h1Content():string;

	public function demoFrontHomeMainWidget_pContent():string;

}