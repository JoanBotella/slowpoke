<?php
declare(strict_types=1);

namespace demo\book\front\page\home\service\widget\main;

use demo\book\front\page\home\service\widget\main\DemoFrontHomeMainWidgetContext;
use demo\book\front\page\home\service\widget\main\DemoFrontHomeMainWidgetItf;

final class
	DemoFrontHomeMainWidget
implements
	DemoFrontHomeMainWidgetItf
{

	public function render(DemoFrontHomeMainWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/demoFrontHomeMainWidgetTemplate.php';
		return ob_get_clean();
	}

}