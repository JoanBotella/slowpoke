<?php
declare(strict_types=1);

namespace demo\book\front\page\home\service\widget\main;

use demo\book\front\page\home\service\widget\main\DemoFrontHomeMainWidgetContext;

interface
	DemoFrontHomeMainWidgetItf
{

	public function render(DemoFrontHomeMainWidgetContext $context):string;

}