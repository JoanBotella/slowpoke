<?php
declare(strict_types=1);

namespace demo\book\front\page\home\service\widget\main\library\translation\es;

trait
	DemoFrontHomeMainWidgetEsTranslationTrait
{

	public function demoFrontHomeMainWidget_h1Content():string { return 'Inicio'; }

	public function demoFrontHomeMainWidget_pContent():string { return 'Ésta es la página Inicio'; }

}