<?php
declare(strict_types=1);

namespace demo\book\front\page\home\service\widget\main;

use demo\book\front\page\home\library\translation\DemoFrontHomeTranslationItf;

final class
	DemoFrontHomeMainWidgetContext
{

	private DemoFrontHomeTranslationItf $translation;

	public function __construct(
		DemoFrontHomeTranslationItf $translation
	)
	{
		$this->translation = $translation;
	}

	public function getTranslation():DemoFrontHomeTranslationItf
	{
		return $this->translation;
	}

}