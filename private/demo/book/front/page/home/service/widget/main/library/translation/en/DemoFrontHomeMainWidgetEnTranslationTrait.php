<?php
declare(strict_types=1);

namespace demo\book\front\page\home\service\widget\main\library\translation\en;

trait
	DemoFrontHomeMainWidgetEnTranslationTrait
{

	public function demoFrontHomeMainWidget_h1Content():string { return 'Home'; }

	public function demoFrontHomeMainWidget_pContent():string { return 'This is the Home page'; }

}