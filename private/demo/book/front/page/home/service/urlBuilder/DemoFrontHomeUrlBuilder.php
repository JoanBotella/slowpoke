<?php
declare(strict_types=1);

namespace demo\book\front\page\home\service\urlBuilder;

use demo\book\front\library\urlBuilder\DemoFrontUrlBuilderAbs;
use demo\book\front\page\home\service\urlBuilder\DemoFrontHomeUrlBuilderItf;

final class
	DemoFrontHomeUrlBuilder
extends
	DemoFrontUrlBuilderAbs
implements
	DemoFrontHomeUrlBuilderItf
{

	public function build(string $languageCode):string
	{
		$translation = $this->getTranslation($languageCode);

		return
			$languageCode
			.'/'
			.$translation->demoFrontHomePage_slug()
		;
	}

}