<?php
declare(strict_types=1);

namespace demo\book\front\page\home\service\urlBuilder;

interface
	DemoFrontHomeUrlBuilderItf
{

	public function build(string $languageCode):string;

}