<?php
declare(strict_types=1);

namespace demo\book\front\page\home\service\requestMatcher;

use demo\book\front\page\home\service\requestMatcher\DemoFrontHomeRequestMatcherItf;
use demo\book\front\library\requestMatcher\DemoFrontRequestMatcherAbs;
use slowpoke\core\library\request\CoreRequest;

final class
	DemoFrontHomeRequestMatcher
extends
	DemoFrontRequestMatcherAbs
implements
	DemoFrontHomeRequestMatcherItf
{

	public function isMatch(CoreRequest $coreRequest):bool
	{

		$path = $coreRequest->getPath();

		if ($path === '')
		{
			return true;
		}

		$languageCode = $coreRequest->getLanguageCode();
		$translation = $this->getTranslation($languageCode);

		return
			$path ===
				$languageCode
				.'/'
				.$translation->demoFrontHomePage_slug()
		;
	}

}
