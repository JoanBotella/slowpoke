<?php
declare(strict_types=1);

namespace demo\book\front\page\home\service\requestMatcher;

use slowpoke\core\library\requestMatcher\CoreRequestMatcherItf;

interface
	DemoFrontHomeRequestMatcherItf
extends
	CoreRequestMatcherItf
{

}