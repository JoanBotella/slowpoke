<?php
declare(strict_types=1);

namespace demo\book\front\page\home\service\responseBuilder;

use slowpoke\core\library\CoreNotifications;
use slowpoke\core\library\response\CoreResponse;

interface
	DemoFrontHomeResponseBuilderItf
{

	public function build(
		string $languageCode,
		string $requestPath,
		CoreNotifications $coreNotifications
	):CoreResponse;

}
