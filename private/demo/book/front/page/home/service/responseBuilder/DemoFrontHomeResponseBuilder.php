<?php
declare(strict_types=1);

namespace demo\book\front\page\home\service\responseBuilder;

use slowpoke\core\library\CoreNotifications;
use demo\book\front\page\home\service\responseBuilder\DemoFrontHomeResponseBuilderItf;
use demo\book\front\library\responseBuilder\DemoFrontResponseBuilderAbs;
use demo\book\front\page\home\service\urlBuilder\DemoFrontHomeUrlBuilderItf;
use demo\book\front\page\home\service\widget\main\DemoFrontHomeMainWidgetContext;
use demo\book\front\page\home\service\widget\main\DemoFrontHomeMainWidgetItf;
use demo\book\front\service\widget\body\DemoFrontBodyWidgetItf;
use demo\book\front\service\widget\footer\DemoFrontFooterWidgetItf;
use demo\book\front\service\widget\header\DemoFrontHeaderWidgetItf;
use demo\book\front\service\widget\nav\DemoFrontNavWidgetItf;
use slowpoke\core\library\response\CoreResponse;
use slowpoke\core\service\widget\html\CoreHtmlWidgetItf;
use slowpoke\core\service\widget\notifications\CoreNotificationsWidgetItf;
use demo\book\front\page\about\service\urlBuilder\DemoFrontAboutUrlBuilderItf;
use demo\book\front\service\translationContainer\DemoFrontTranslationContainerItf;
use demo\book\front\service\widget\aside\DemoFrontAsideWidgetItf;

final class
	DemoFrontHomeResponseBuilder
extends
	DemoFrontResponseBuilderAbs
implements
	DemoFrontHomeResponseBuilderItf
{

	private string $languageCode;

	private string $requestPath;

	private CoreNotifications $coreNotifications;

	private DemoFrontHomeUrlBuilderItf $demoDemoFrontHomeUrlBuilder;

	public function __construct(
		string $protocol,
		string $domain,
		string $basePath,
		bool $useMinifiedScripts,
		bool $useMinifiedStyles,
		CoreHtmlWidgetItf $coreHtmlWidget,

		string $applicationName,
		DemoFrontTranslationContainerItf $demoFrontTranslationContainer,
		DemoFrontHeaderWidgetItf $demoFrontHeaderWidget,
		DemoFrontNavWidgetItf $demoFrontNavWidgetItf,
		CoreNotificationsWidgetItf $coreNotificationsWidget,
		DemoFrontAsideWidgetItf $demoFrontAsideWidget,
		DemoFrontFooterWidgetItf $demoFrontFooterWidget,
		DemoFrontBodyWidgetItf $demoFrontBodyWidget,
		DemoFrontHomeUrlBuilderItf $demoDemoFrontHomeUrlBuilder,
		DemoFrontAboutUrlBuilderItf $demoFrontAboutUrlBuilder,

		DemoFrontHomeMainWidgetItf $demoFrontHomeMainWidget
	)
	{
		parent::__construct(
			$protocol,
			$domain,
			$basePath,
			$useMinifiedScripts,
			$useMinifiedStyles,
			$coreHtmlWidget,

			$applicationName,
			$demoFrontTranslationContainer,
			$demoFrontHeaderWidget,
			$demoFrontNavWidgetItf,
			$coreNotificationsWidget,
			$demoFrontAsideWidget,
			$demoFrontFooterWidget,
			$demoFrontBodyWidget,
			$demoDemoFrontHomeUrlBuilder,
			$demoFrontAboutUrlBuilder
		);
		$this->demoFrontHomeMainWidget = $demoFrontHomeMainWidget;
		$this->demoDemoFrontHomeUrlBuilder = $demoDemoFrontHomeUrlBuilder;
	}

	public function build(
		string $languageCode,
		string $requestPath,
		CoreNotifications $coreNotifications
	):CoreResponse
	{
		$this->languageCode = $languageCode;
		$this->requestPath = $requestPath;
		$this->coreNotifications = $coreNotifications;

		$coreResponse = $this->buildCoreResponse();

		$this->reset();

		return $coreResponse;
	}

		private function reset():void
		{
			unset($this->languageCode);
			unset($this->requestPath);
		}

	protected function getLanguageCode():string
	{
		return $this->languageCode;
	}

	protected function getPageCode():string
	{
		return 'demo-front-home';
	}

	protected function buildMainElement():string
	{
		$context = new DemoFrontHomeMainWidgetContext(
			$this->getDemoFrontTranslation()
		);	
		return $this->demoFrontHomeMainWidget->render($context);
	}

	protected function hasCanonicalHref():bool
	{
		return $this->requestPath === '';
	}

	protected function getCanonicalHrefAfterHas():string
	{
		return $this->demoDemoFrontHomeUrlBuilder->build(
			$this->languageCode
		);
	}

	protected function getPageName():string
	{
		return $this->getDemoFrontTranslation()->demoFrontHomePage_pageName();
	}

	protected function getCoreNotifications():CoreNotifications
	{
		return $this->coreNotifications;
	}

}
