<?php
declare(strict_types=1);

namespace demo\book\front\page\home\library\translation\es;

use demo\book\front\page\home\service\widget\main\library\translation\es\DemoFrontHomeMainWidgetEsTranslationTrait;

trait
	DemoFrontHomeEsTranslationTrait
{
	use
		DemoFrontHomeMainWidgetEsTranslationTrait
	;

	public function demoFrontHomePage_pageName():string { return 'Inicio'; }

	public function demoFrontHomePage_slug():string { return 'inicio'; }

}
