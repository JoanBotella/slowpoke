<?php
declare(strict_types=1);

namespace demo\book\front\page\home\library\translation\en;

use demo\book\front\page\home\service\widget\main\library\translation\en\DemoFrontHomeMainWidgetEnTranslationTrait;

trait
	DemoFrontHomeEnTranslationTrait
{
	use
		DemoFrontHomeMainWidgetEnTranslationTrait
	;

	public function demoFrontHomePage_pageName():string { return 'Home'; }

	public function demoFrontHomePage_slug():string { return 'home'; }

}
