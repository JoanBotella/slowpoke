<?php
declare(strict_types=1);

namespace demo\book\front\page\home\library\translation;

use demo\book\front\page\home\service\widget\main\library\translation\DemoFrontHomeMainWidgetTranslationItf;

interface
	DemoFrontHomeTranslationItf
extends
	DemoFrontHomeMainWidgetTranslationItf
{

	public function demoFrontHomePage_pageName():string;

	public function demoFrontHomePage_slug():string;

}