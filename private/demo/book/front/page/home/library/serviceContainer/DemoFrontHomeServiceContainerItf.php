<?php
declare(strict_types=1);

namespace demo\book\front\page\home\library\serviceContainer;

use demo\book\front\page\home\service\controller\DemoFrontHomeControllerItf;
use demo\book\front\page\home\service\requestMatcher\DemoFrontHomeRequestMatcherItf;
use demo\book\front\page\home\service\requestValidator\DemoFrontHomeRequestValidatorItf;
use demo\book\front\page\home\service\responseBuilder\DemoFrontHomeResponseBuilderItf;
use demo\book\front\page\home\service\urlBuilder\DemoFrontHomeUrlBuilderItf;
use demo\book\front\page\home\service\widget\main\DemoFrontHomeMainWidgetItf;

interface
	DemoFrontHomeServiceContainerItf
{

	public function getDemoFrontHomeController():DemoFrontHomeControllerItf;

	public function getDemoFrontHomeMainWidget():DemoFrontHomeMainWidgetItf;

	public function getDemoFrontHomeRequestMatcher():DemoFrontHomeRequestMatcherItf;

	public function getDemoFrontHomeRequestValidator():DemoFrontHomeRequestValidatorItf;

	public function getDemoFrontHomeResponseBuilder():DemoFrontHomeResponseBuilderItf;

	public function getDemoFrontHomeUrlBuilder():DemoFrontHomeUrlBuilderItf;

}