<?php
declare(strict_types=1);

namespace demo\book\front\page\home\library\serviceContainer;

use demo\book\front\page\home\service\controller\DemoFrontHomeController;
use demo\book\front\page\home\service\controller\DemoFrontHomeControllerItf;
use demo\book\front\page\home\service\requestMatcher\DemoFrontHomeRequestMatcher;
use demo\book\front\page\home\service\requestMatcher\DemoFrontHomeRequestMatcherItf;
use demo\book\front\page\home\service\requestValidator\DemoFrontHomeRequestValidator;
use demo\book\front\page\home\service\requestValidator\DemoFrontHomeRequestValidatorItf;
use demo\book\front\page\home\service\responseBuilder\DemoFrontHomeResponseBuilder;
use demo\book\front\page\home\service\responseBuilder\DemoFrontHomeResponseBuilderItf;
use demo\book\front\page\home\service\urlBuilder\DemoFrontHomeUrlBuilder;
use demo\book\front\page\home\service\urlBuilder\DemoFrontHomeUrlBuilderItf;
use demo\book\front\page\home\service\widget\main\DemoFrontHomeMainWidget;
use demo\book\front\page\home\service\widget\main\DemoFrontHomeMainWidgetItf;

trait
	DemoFrontHomeServiceContainerTrait
{

	private DemoFrontHomeControllerItf $demoFrontHomeController;

	public function getDemoFrontHomeController():DemoFrontHomeControllerItf
	{
		if (!isset($this->demoFrontHomeController))
		{
			$this->demoFrontHomeController = $this->buildDemoFrontHomeController();
		}
		return $this->demoFrontHomeController;
	}

		protected function buildDemoFrontHomeController():DemoFrontHomeControllerItf
		{
			return new DemoFrontHomeController(
				$this->getSessionSessionManager(),
				$this->getSessionTranslationContainer(),

				$this->getDemoFrontHomeResponseBuilder()
			);
		}

	private DemoFrontHomeMainWidgetItf $demoFrontHomeMainWidget;

	public function getDemoFrontHomeMainWidget():DemoFrontHomeMainWidgetItf
	{
		if (!isset($this->demoFrontHomeMainWidget))
		{
			$this->demoFrontHomeMainWidget = $this->buildDemoFrontHomeMainWidget();
		}
		return $this->demoFrontHomeMainWidget;
	}

		protected function buildDemoFrontHomeMainWidget():DemoFrontHomeMainWidgetItf
		{
			return new DemoFrontHomeMainWidget(
			);
		}

	private DemoFrontHomeRequestMatcherItf $demoFrontHomeRequestMatcher;

	public function getDemoFrontHomeRequestMatcher():DemoFrontHomeRequestMatcherItf
	{
		if (!isset($this->demoFrontHomeRequestMatcher))
		{
			$this->demoFrontHomeRequestMatcher = $this->buildDemoFrontHomeRequestMatcher();
		}
		return $this->demoFrontHomeRequestMatcher;
	}

		protected function buildDemoFrontHomeRequestMatcher():DemoFrontHomeRequestMatcherItf
		{
			return new DemoFrontHomeRequestMatcher(
				$this->getDemoFrontTranslationContainer()
			);
		}

	private DemoFrontHomeRequestValidatorItf $demoFrontHomeRequestValidator;

	public function getDemoFrontHomeRequestValidator():DemoFrontHomeRequestValidatorItf
	{
		if (!isset($this->demoFrontHomeRequestValidator))
		{
			$this->demoFrontHomeRequestValidator = $this->buildDemoFrontHomeRequestValidator();
		}
		return $this->demoFrontHomeRequestValidator;
	}

		protected function buildDemoFrontHomeRequestValidator():DemoFrontHomeRequestValidatorItf
		{
			return new DemoFrontHomeRequestValidator(
			);
		}

	private DemoFrontHomeResponseBuilderItf $demoFrontHomeResponseBuilder;

	public function getDemoFrontHomeResponseBuilder():DemoFrontHomeResponseBuilderItf
	{
		if (!isset($this->demoFrontHomeResponseBuilder))
		{
			$this->demoFrontHomeResponseBuilder = $this->buildDemoFrontHomeResponseBuilder();
		}
		return $this->demoFrontHomeResponseBuilder;
	}

		protected function buildDemoFrontHomeResponseBuilder():DemoFrontHomeResponseBuilderItf
		{
			return new DemoFrontHomeResponseBuilder(
				$this->configuration_core_protocol(),
				$this->configuration_core_domain(),
				$this->configuration_core_basePath(),
				$this->configuration_core_useMinifiedScripts(),
				$this->configuration_core_useMinifiedStyles(),
				$this->getCoreHtmlWidget(),
	
				$this->configuration_core_applicationName(),
				$this->getDemoFrontTranslationContainer(),
				$this->getDemoFrontHeaderWidget(),
				$this->getDemoFrontNavWidget(),
				$this->getCoreNotificationsWidget(),
				$this->getDemoFrontAsideWidget(),
				$this->getDemoFrontFooterWidget(),
				$this->getDemoFrontBodyWidget(),
				$this->getDemoFrontHomeUrlBuilder(),
				$this->getDemoFrontAboutUrlBuilder(),
		
				$this->getDemoFrontHomeMainWidget()
			);
		}

	private DemoFrontHomeUrlBuilderItf $demoDemoFrontHomeUrlBuilder;

	public function getDemoFrontHomeUrlBuilder():DemoFrontHomeUrlBuilderItf
	{
		if (!isset($this->demoDemoFrontHomeUrlBuilder))
		{
			$this->demoDemoFrontHomeUrlBuilder = $this->buildDemoFrontHomeUrlBuilder();
		}
		return $this->demoDemoFrontHomeUrlBuilder;
	}

		protected function buildDemoFrontHomeUrlBuilder():DemoFrontHomeUrlBuilderItf
		{
			return new DemoFrontHomeUrlBuilder(
				$this->getDemoFrontTranslationContainer()
			);
		}

}