<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\library\serviceContainer;

use demo\book\front\page\status500\service\controller\DemoFrontStatus500Controller;
use demo\book\front\page\status500\service\controller\DemoFrontStatus500ControllerItf;
use demo\book\front\page\status500\service\requestMatcher\DemoFrontStatus500RequestMatcher;
use demo\book\front\page\status500\service\requestMatcher\DemoFrontStatus500RequestMatcherItf;
use demo\book\front\page\status500\service\requestValidator\DemoFrontStatus500RequestValidator;
use demo\book\front\page\status500\service\requestValidator\DemoFrontStatus500RequestValidatorItf;
use demo\book\front\page\status500\service\responseBuilder\DemoFrontStatus500ResponseBuilder;
use demo\book\front\page\status500\service\responseBuilder\DemoFrontStatus500ResponseBuilderItf;
use demo\book\front\page\status500\service\urlBuilder\DemoFrontStatus500UrlBuilder;
use demo\book\front\page\status500\service\urlBuilder\DemoFrontStatus500UrlBuilderItf;
use demo\book\front\page\status500\service\widget\main\DemoFrontStatus500MainWidget;
use demo\book\front\page\status500\service\widget\main\DemoFrontStatus500MainWidgetItf;

trait
	DemoFrontStatus500ServiceContainerTrait
{

	private DemoFrontStatus500ControllerItf $demoFrontStatus500Controller;

	public function getDemoFrontStatus500Controller():DemoFrontStatus500ControllerItf
	{
		if (!isset($this->demoFrontStatus500Controller))
		{
			$this->demoFrontStatus500Controller = $this->buildDemoFrontStatus500Controller();
		}
		return $this->demoFrontStatus500Controller;
	}

		protected function buildDemoFrontStatus500Controller():DemoFrontStatus500ControllerItf
		{
			return new DemoFrontStatus500Controller(
				$this->getSessionSessionManager(),
				$this->getSessionTranslationContainer(),

				$this->getDemoFrontStatus500ResponseBuilder()
			);
		}

	private DemoFrontStatus500MainWidgetItf $demoFrontStatus500MainWidget;

	public function getDemoFrontStatus500MainWidget():DemoFrontStatus500MainWidgetItf
	{
		if (!isset($this->demoFrontStatus500MainWidget))
		{
			$this->demoFrontStatus500MainWidget = $this->buildDemoFrontStatus500MainWidget();
		}
		return $this->demoFrontStatus500MainWidget;
	}

		protected function buildDemoFrontStatus500MainWidget():DemoFrontStatus500MainWidgetItf
		{
			return new DemoFrontStatus500MainWidget(
			);
		}

	private DemoFrontStatus500RequestMatcherItf $demoFrontStatus500RequestMatcher;

	public function getDemoFrontStatus500RequestMatcher():DemoFrontStatus500RequestMatcherItf
	{
		if (!isset($this->demoFrontStatus500RequestMatcher))
		{
			$this->demoFrontStatus500RequestMatcher = $this->buildDemoFrontStatus500RequestMatcher();
		}
		return $this->demoFrontStatus500RequestMatcher;
	}

		protected function buildDemoFrontStatus500RequestMatcher():DemoFrontStatus500RequestMatcherItf
		{
			return new DemoFrontStatus500RequestMatcher(
				$this->getDemoFrontTranslationContainer()
			);
		}

	private DemoFrontStatus500RequestValidatorItf $demoFrontStatus500RequestValidator;

	public function getDemoFrontStatus500RequestValidator():DemoFrontStatus500RequestValidatorItf
	{
		if (!isset($this->demoFrontStatus500RequestValidator))
		{
			$this->demoFrontStatus500RequestValidator = $this->buildDemoFrontStatus500RequestValidator();
		}
		return $this->demoFrontStatus500RequestValidator;
	}

		protected function buildDemoFrontStatus500RequestValidator():DemoFrontStatus500RequestValidatorItf
		{
			return new DemoFrontStatus500RequestValidator(
			);
		}

	private DemoFrontStatus500ResponseBuilderItf $demoFrontStatus500ResponseBuilder;

	public function getDemoFrontStatus500ResponseBuilder():DemoFrontStatus500ResponseBuilderItf
	{
		if (!isset($this->demoFrontStatus500ResponseBuilder))
		{
			$this->demoFrontStatus500ResponseBuilder = $this->buildDemoFrontStatus500ResponseBuilder();
		}
		return $this->demoFrontStatus500ResponseBuilder;
	}

		protected function buildDemoFrontStatus500ResponseBuilder():DemoFrontStatus500ResponseBuilderItf
		{
			return new DemoFrontStatus500ResponseBuilder(
				$this->configuration_core_protocol(),
				$this->configuration_core_domain(),
				$this->configuration_core_basePath(),
				$this->configuration_core_useMinifiedScripts(),
				$this->configuration_core_useMinifiedStyles(),
				$this->getCoreHtmlWidget(),
	
				$this->configuration_core_applicationName(),
				$this->getDemoFrontTranslationContainer(),
				$this->getDemoFrontHeaderWidget(),
				$this->getDemoFrontNavWidget(),
				$this->getCoreNotificationsWidget(),
				$this->getDemoFrontAsideWidget(),
				$this->getDemoFrontFooterWidget(),
				$this->getDemoFrontBodyWidget(),
				$this->getDemoFrontHomeUrlBuilder(),
				$this->getDemoFrontAboutUrlBuilder(),
		
				$this->getDemoFrontStatus500MainWidget()
			);
		}

	private DemoFrontStatus500UrlBuilderItf $demoFrontStatus500UrlBuilder;

	public function getDemoFrontStatus500UrlBuilder():DemoFrontStatus500UrlBuilderItf
	{
		if (!isset($this->demoFrontStatus500UrlBuilder))
		{
			$this->demoFrontStatus500UrlBuilder = $this->buildDemoFrontStatus500UrlBuilder();
		}
		return $this->demoFrontStatus500UrlBuilder;
	}

		protected function buildDemoFrontStatus500UrlBuilder():DemoFrontStatus500UrlBuilderItf
		{
			return new DemoFrontStatus500UrlBuilder(
				$this->getDemoFrontTranslationContainer()
			);
		}

}