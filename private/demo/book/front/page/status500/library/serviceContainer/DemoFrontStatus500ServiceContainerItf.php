<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\library\serviceContainer;

use demo\book\front\page\status500\service\controller\DemoFrontStatus500ControllerItf;
use demo\book\front\page\status500\service\requestMatcher\DemoFrontStatus500RequestMatcherItf;
use demo\book\front\page\status500\service\requestValidator\DemoFrontStatus500RequestValidatorItf;
use demo\book\front\page\status500\service\responseBuilder\DemoFrontStatus500ResponseBuilderItf;
use demo\book\front\page\status500\service\urlBuilder\DemoFrontStatus500UrlBuilderItf;
use demo\book\front\page\status500\service\widget\main\DemoFrontStatus500MainWidgetItf;

interface
	DemoFrontStatus500ServiceContainerItf
{

	public function getDemoFrontStatus500Controller():DemoFrontStatus500ControllerItf;

	public function getDemoFrontStatus500MainWidget():DemoFrontStatus500MainWidgetItf;

	public function getDemoFrontStatus500RequestMatcher():DemoFrontStatus500RequestMatcherItf;

	public function getDemoFrontStatus500RequestValidator():DemoFrontStatus500RequestValidatorItf;

	public function getDemoFrontStatus500ResponseBuilder():DemoFrontStatus500ResponseBuilderItf;

	public function getDemoFrontStatus500UrlBuilder():DemoFrontStatus500UrlBuilderItf;

}