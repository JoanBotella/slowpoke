<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\library\route;

use slowpoke\core\library\controller\CoreControllerItf;
use slowpoke\core\library\requestMatcher\CoreRequestMatcherItf;
use slowpoke\core\library\requestValidator\CoreRequestValidatorItf;
use demo\book\front\library\route\DemoFrontRouteAbs;

final class
	DemoFrontStatus500Route
extends
	DemoFrontRouteAbs
{

	public function getController():CoreControllerItf
	{
		return $this->getServiceContainer()->getDemoFrontStatus500Controller();
	}

	public function getRequestMatcher():CoreRequestMatcherItf
	{
		return $this->getServiceContainer()->getDemoFrontStatus500RequestMatcher();
	}

	public function getRequestValidator():CoreRequestValidatorItf
	{
		return $this->getServiceContainer()->getDemoFrontStatus500RequestValidator();
	}

}
