<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\library\translation\en;

use demo\book\front\page\status500\service\widget\main\library\translation\en\DemoFrontStatus500MainWidgetEnTranslationTrait;

trait
	DemoFrontStatus500EnTranslationTrait
{
	use
		DemoFrontStatus500MainWidgetEnTranslationTrait
	;

	public function demoFrontStatus500Page_pageName():string { return 'Internal Server Error'; }

	public function demoFrontStatus500Page_slug():string { return 'internal-server-error'; }

}
