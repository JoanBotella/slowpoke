<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\library\translation;

use demo\book\front\page\status500\service\widget\main\library\translation\DemoFrontStatus500MainWidgetTranslationItf;

interface
	DemoFrontStatus500TranslationItf
extends
	DemoFrontStatus500MainWidgetTranslationItf
{

	public function demoFrontStatus500Page_pageName():string;

	public function demoFrontStatus500Page_slug():string;

}