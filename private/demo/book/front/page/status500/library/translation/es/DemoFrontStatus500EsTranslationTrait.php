<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\library\translation\es;

use demo\book\front\page\status500\service\widget\main\library\translation\es\DemoFrontStatus500MainWidgetEsTranslationTrait;

trait
	DemoFrontStatus500EsTranslationTrait
{
	use
		DemoFrontStatus500MainWidgetEsTranslationTrait
	;

	public function demoFrontStatus500Page_pageName():string { return 'Error Interno del Servidor'; }

	public function demoFrontStatus500Page_slug():string { return 'error-interno-del-servidor'; }

}
