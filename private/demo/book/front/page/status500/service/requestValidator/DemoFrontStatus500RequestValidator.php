<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\service\requestValidator;

use demo\book\front\page\status500\service\requestValidator\DemoFrontStatus500RequestValidatorItf;
use demo\book\front\library\requestValidator\DemoFrontRequestValidatorAbs;
use slowpoke\core\library\request\CoreRequest;

final class
	DemoFrontStatus500RequestValidator
extends
	DemoFrontRequestValidatorAbs
implements
	DemoFrontStatus500RequestValidatorItf
{

	public function isValid(CoreRequest $coreRequest):bool
	{
		return true;	// !!!
	}

}
