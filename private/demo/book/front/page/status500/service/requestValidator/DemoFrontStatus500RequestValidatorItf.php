<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\service\requestValidator;

use slowpoke\core\library\requestValidator\CoreRequestValidatorItf;

interface
	DemoFrontStatus500RequestValidatorItf
extends
	CoreRequestValidatorItf
{

}