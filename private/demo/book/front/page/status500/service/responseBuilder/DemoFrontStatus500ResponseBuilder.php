<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\service\responseBuilder;

use slowpoke\core\library\CoreNotifications;
use demo\book\front\page\status500\service\responseBuilder\DemoFrontStatus500ResponseBuilderItf;
use demo\book\front\library\responseBuilder\DemoFrontResponseBuilderAbs;
use demo\book\front\page\status500\service\widget\main\DemoFrontStatus500MainWidgetContext;
use demo\book\front\page\status500\service\widget\main\DemoFrontStatus500MainWidgetItf;
use demo\book\front\page\home\service\urlBuilder\DemoFrontHomeUrlBuilderItf;
use demo\book\front\service\widget\body\DemoFrontBodyWidgetItf;
use demo\book\front\service\widget\footer\DemoFrontFooterWidgetItf;
use demo\book\front\service\widget\header\DemoFrontHeaderWidgetItf;
use demo\book\front\service\widget\nav\DemoFrontNavWidgetItf;
use slowpoke\core\library\response\CoreResponse;
use slowpoke\core\service\widget\html\CoreHtmlWidgetItf;
use slowpoke\core\service\widget\notifications\CoreNotificationsWidgetItf;
use demo\book\front\page\about\service\urlBuilder\DemoFrontAboutUrlBuilderItf;
use demo\book\front\service\translationContainer\DemoFrontTranslationContainerItf;
use demo\book\front\service\widget\aside\DemoFrontAsideWidgetItf;
use slowpoke\core\library\http\CoreHttpStatusConstant;

final class
	DemoFrontStatus500ResponseBuilder
extends
	DemoFrontResponseBuilderAbs
implements
	DemoFrontStatus500ResponseBuilderItf
{

	private DemoFrontStatus500MainWidgetItf $demoFrontStatus500MainWidget;

	private string $languageCode;

	private CoreNotifications $coreNotifications;

	public function __construct(
		string $protocol,
		string $domain,
		string $basePath,
		bool $useMinifiedScripts,
		bool $useMinifiedStyles,
		CoreHtmlWidgetItf $htmlWidget,

		string $applicationName,
		DemoFrontTranslationContainerItf $demoFrontTranslationContainer,
		DemoFrontHeaderWidgetItf $demoFrontHeaderWidget,
		DemoFrontNavWidgetItf $demoFrontNavWidgetItf,
		CoreNotificationsWidgetItf $coreNotificationsWidget,
		DemoFrontAsideWidgetItf $demoFrontAsideWidget,
		DemoFrontFooterWidgetItf $demoFrontFooterWidget,
		DemoFrontBodyWidgetItf $demoFrontBodyWidget,
		DemoFrontHomeUrlBuilderItf $demoFrontHomeUrlBuilder,
		DemoFrontAboutUrlBuilderItf $demoFrontAboutUrlBuilder,

		DemoFrontStatus500MainWidgetItf $demoFrontStatus500MainWidget
	)
	{
		parent::__construct(
			$protocol,
			$domain,
			$basePath,
			$useMinifiedScripts,
			$useMinifiedStyles,
			$htmlWidget,

			$applicationName,
			$demoFrontTranslationContainer,
			$demoFrontHeaderWidget,
			$demoFrontNavWidgetItf,
			$coreNotificationsWidget,
			$demoFrontAsideWidget,
			$demoFrontFooterWidget,
			$demoFrontBodyWidget,
			$demoFrontHomeUrlBuilder,
			$demoFrontAboutUrlBuilder
		);
		$this->demoFrontStatus500MainWidget = $demoFrontStatus500MainWidget;
	}

	public function build(
		string $languageCode,
		CoreNotifications $coreNotifications
	):CoreResponse
	{
		$this->languageCode = $languageCode;
		$this->coreNotifications = $coreNotifications;

		$coreResponse = $this->buildCoreResponse();

		$this->reset();

		return $coreResponse;
	}

		private function reset():void
		{
			unset($this->languageCode);
		}

	protected function getLanguageCode():string
	{
		return $this->languageCode;
	}

	protected function getPageCode():string
	{
		return 'demo-front-status_500';
	}

	protected function getHttpStatusCode():int
	{
		return CoreHttpStatusConstant::INTERNAL_SERVER_ERROR;
	}

	protected function buildMainElement():string
	{
		$context = new DemoFrontStatus500MainWidgetContext(
			$this->getDemoFrontTranslation()
		);	
		return $this->demoFrontStatus500MainWidget->render($context);
	}

	protected function getPageName():string
	{
		return $this->getDemoFrontTranslation()->demoFrontStatus500Page_pageName();
	}

	protected function getCoreNotifications():CoreNotifications
	{
		return $this->coreNotifications;
	}

}
