<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\service\responseBuilder;

use slowpoke\core\library\CoreNotifications;
use slowpoke\core\library\response\CoreResponse;

interface
	DemoFrontStatus500ResponseBuilderItf
{

	public function build(
		string $languageCode,
		CoreNotifications $coreNotifications
	):CoreResponse;

}
