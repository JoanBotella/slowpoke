<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\service\controller;

use slowpoke\core\library\controller\CoreControllerItf;

interface
	DemoFrontStatus500ControllerItf
extends
	CoreControllerItf
{

}