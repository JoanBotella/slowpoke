<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\service\controller;

use demo\book\front\page\status500\service\controller\DemoFrontStatus500ControllerItf;
use demo\book\front\library\controller\DemoFrontControllerAbs;
use demo\book\front\page\status500\service\responseBuilder\DemoFrontStatus500ResponseBuilderItf;
use slowpoke\core\library\response\CoreResponse;
use slowpoke\session\service\sessionManager\SessionSessionManagerItf;
use slowpoke\session\service\translationContainer\SessionTranslationContainerItf;

final class
	DemoFrontStatus500Controller
extends
	DemoFrontControllerAbs
implements
	DemoFrontStatus500ControllerItf
{

	private DemoFrontStatus500ResponseBuilderItf $demoFrontStatus500ResponseBuilder;

	public function __construct(
		SessionSessionManagerItf $sessionSessionManager,
		SessionTranslationContainerItf $sessionTranslationContainer,

		DemoFrontStatus500ResponseBuilderItf $demoFrontStatus500ResponseBuilder
	)
	{
		parent::__construct(
			$sessionSessionManager,
			$sessionTranslationContainer
		);
		$this->demoFrontStatus500ResponseBuilder = $demoFrontStatus500ResponseBuilder;
	}

	protected function buildResponse():CoreResponse
	{
		$coreRequest = $this->getCoreRequest();
		return $this->demoFrontStatus500ResponseBuilder->build(
			$coreRequest->getLanguageCode(),
			$this->getCoreNotifications()
		);
	}

}
