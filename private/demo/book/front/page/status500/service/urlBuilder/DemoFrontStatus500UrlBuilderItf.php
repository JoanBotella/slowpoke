<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\service\urlBuilder;

interface
	DemoFrontStatus500UrlBuilderItf
{

	public function build(string $languageCode):string;

}