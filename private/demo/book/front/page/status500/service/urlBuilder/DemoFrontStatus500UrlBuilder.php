<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\service\urlBuilder;

use demo\book\front\library\urlBuilder\DemoFrontUrlBuilderAbs;
use demo\book\front\page\status500\service\urlBuilder\DemoFrontStatus500UrlBuilderItf;

final class
	DemoFrontStatus500UrlBuilder
extends
	DemoFrontUrlBuilderAbs
implements
	DemoFrontStatus500UrlBuilderItf
{

	public function build(string $languageCode):string
	{
		$translation = $this->getTranslation($languageCode);

		return
			$languageCode
			.'/'
			.$translation->demoFrontStatus500Page_slug()
		;
	}

}