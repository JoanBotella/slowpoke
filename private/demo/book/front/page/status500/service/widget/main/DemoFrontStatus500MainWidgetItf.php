<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\service\widget\main;

use demo\book\front\page\status500\service\widget\main\DemoFrontStatus500MainWidgetContext;

interface
	DemoFrontStatus500MainWidgetItf
{

	public function render(DemoFrontStatus500MainWidgetContext $context):string;

}