<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\service\widget\main\library\translation;

interface
	DemoFrontStatus500MainWidgetTranslationItf
{

	public function demoFrontStatus500MainWidget_h1Content():string;

	public function demoFrontStatus500MainWidget_pContent():string;

}