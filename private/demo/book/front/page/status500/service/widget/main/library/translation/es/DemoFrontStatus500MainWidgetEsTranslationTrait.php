<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\service\widget\main\library\translation\es;

trait
	DemoFrontStatus500MainWidgetEsTranslationTrait
{

	public function demoFrontStatus500MainWidget_h1Content():string { return 'Error Interno del Servidor'; }

	public function demoFrontStatus500MainWidget_pContent():string { return 'Ésta es la página Error Interno del Servidor'; }

}