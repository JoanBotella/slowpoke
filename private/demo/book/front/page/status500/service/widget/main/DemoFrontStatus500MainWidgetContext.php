<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\service\widget\main;

use demo\book\front\page\status500\library\translation\DemoFrontStatus500TranslationItf;

final class
	DemoFrontStatus500MainWidgetContext
{

	private DemoFrontStatus500TranslationItf $translation;

	public function __construct(
		DemoFrontStatus500TranslationItf $translation
	)
	{
		$this->translation = $translation;
	}

	public function getTranslation():DemoFrontStatus500TranslationItf
	{
		return $this->translation;
	}

}