<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\service\widget\main;

use demo\book\front\page\status500\service\widget\main\DemoFrontStatus500MainWidgetContext;
use demo\book\front\page\status500\service\widget\main\DemoFrontStatus500MainWidgetItf;

final class
	DemoFrontStatus500MainWidget
implements
	DemoFrontStatus500MainWidgetItf
{

	public function render(DemoFrontStatus500MainWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/demoFrontStatus500MainWidgetTemplate.php';
		return ob_get_clean();
	}

}