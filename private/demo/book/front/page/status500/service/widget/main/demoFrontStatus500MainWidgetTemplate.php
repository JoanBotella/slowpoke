<?php
declare(strict_types=1);

use demo\book\front\page\status500\service\widget\main\DemoFrontStatus500MainWidgetContext;

/**
 * @var DemoFrontStatus500MainWidgetContext $context
 */

$translation = $context->getTranslation();

?><main data-widget="demo-front-status_500-main">

	<h1><?= $translation->demoFrontStatus500MainWidget_h1Content() ?></h1>

	<p><?= $translation->demoFrontStatus500MainWidget_pContent() ?></p>

</main>