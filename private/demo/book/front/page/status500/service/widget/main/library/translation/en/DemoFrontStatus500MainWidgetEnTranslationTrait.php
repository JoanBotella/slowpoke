<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\service\widget\main\library\translation\en;

trait
	DemoFrontStatus500MainWidgetEnTranslationTrait
{

	public function demoFrontStatus500MainWidget_h1Content():string { return 'Internal Server Error'; }

	public function demoFrontStatus500MainWidget_pContent():string { return 'This is the Internal Server Error page'; }

}