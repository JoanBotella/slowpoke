<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\service\requestMatcher;

use slowpoke\core\library\requestMatcher\CoreRequestMatcherItf;

interface
	DemoFrontStatus500RequestMatcherItf
extends
	CoreRequestMatcherItf
{

}