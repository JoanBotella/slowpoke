<?php
declare(strict_types=1);

namespace demo\book\front\page\status500\service\requestMatcher;

use demo\book\front\page\status500\service\requestMatcher\DemoFrontStatus500RequestMatcherItf;
use demo\book\front\library\requestMatcher\DemoFrontRequestMatcherAbs;
use slowpoke\core\library\request\CoreRequest;

final class
	DemoFrontStatus500RequestMatcher
extends
	DemoFrontRequestMatcherAbs
implements
	DemoFrontStatus500RequestMatcherItf
{

	public function isMatch(CoreRequest $coreRequest):bool
	{
		$languageCode = $coreRequest->getLanguageCode();
		$translation = $this->getTranslation($languageCode);

		return
			$coreRequest->getPath() ===
				$languageCode
				.'/'
				.$translation->demoFrontStatus500Page_slug()
		;
	}

}
