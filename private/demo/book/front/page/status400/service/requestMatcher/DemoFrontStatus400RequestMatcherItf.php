<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\service\requestMatcher;

use slowpoke\core\library\requestMatcher\CoreRequestMatcherItf;

interface
	DemoFrontStatus400RequestMatcherItf
extends
	CoreRequestMatcherItf
{

}