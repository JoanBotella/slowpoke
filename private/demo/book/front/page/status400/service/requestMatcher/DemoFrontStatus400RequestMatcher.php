<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\service\requestMatcher;

use demo\book\front\page\status400\service\requestMatcher\DemoFrontStatus400RequestMatcherItf;
use demo\book\front\library\requestMatcher\DemoFrontRequestMatcherAbs;
use slowpoke\core\library\request\CoreRequest;

final class
	DemoFrontStatus400RequestMatcher
extends
	DemoFrontRequestMatcherAbs
implements
	DemoFrontStatus400RequestMatcherItf
{

	public function isMatch(CoreRequest $coreRequest):bool
	{
		$languageCode = $coreRequest->getLanguageCode();
		$translation = $this->getTranslation($languageCode);

		return
			$coreRequest->getPath() ===
				$languageCode
				.'/'
				.$translation->demoFrontStatus400Page_slug()
		;
	}

}
