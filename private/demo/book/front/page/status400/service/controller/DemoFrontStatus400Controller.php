<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\service\controller;

use demo\book\front\page\status400\service\controller\DemoFrontStatus400ControllerItf;
use demo\book\front\library\controller\DemoFrontControllerAbs;
use demo\book\front\page\status400\service\responseBuilder\DemoFrontStatus400ResponseBuilderItf;
use slowpoke\core\library\response\CoreResponse;
use slowpoke\session\service\sessionManager\SessionSessionManagerItf;
use slowpoke\session\service\translationContainer\SessionTranslationContainerItf;

final class
	DemoFrontStatus400Controller
extends
	DemoFrontControllerAbs
implements
	DemoFrontStatus400ControllerItf
{

	private DemoFrontStatus400ResponseBuilderItf $demoFrontStatus400ResponseBuilder;

	public function __construct(
		SessionSessionManagerItf $sessionSessionManager,
		SessionTranslationContainerItf $sessionTranslationContainer,

		DemoFrontStatus400ResponseBuilderItf $demoFrontStatus400ResponseBuilder
	)
	{
		parent::__construct(
			$sessionSessionManager,
			$sessionTranslationContainer
		);
		$this->demoFrontStatus400ResponseBuilder = $demoFrontStatus400ResponseBuilder;
	}

	protected function buildResponse():CoreResponse
	{
		$coreRequest = $this->getCoreRequest();
		return $this->demoFrontStatus400ResponseBuilder->build(
			$coreRequest->getLanguageCode(),
			$this->getCoreNotifications()
		);
	}

}
