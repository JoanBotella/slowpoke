<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\service\urlBuilder;

use demo\book\front\library\urlBuilder\DemoFrontUrlBuilderAbs;
use demo\book\front\page\status400\service\urlBuilder\DemoFrontStatus400UrlBuilderItf;

final class
	DemoFrontStatus400UrlBuilder
extends
	DemoFrontUrlBuilderAbs
implements
	DemoFrontStatus400UrlBuilderItf
{

	public function build(string $languageCode):string
	{
		$translation = $this->getTranslation($languageCode);

		return
			$languageCode
			.'/'
			.$translation->demoFrontStatus400Page_slug()
		;
	}

}