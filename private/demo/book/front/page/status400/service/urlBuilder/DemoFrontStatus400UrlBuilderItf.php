<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\service\urlBuilder;

interface
	DemoFrontStatus400UrlBuilderItf
{

	public function build(string $languageCode):string;

}