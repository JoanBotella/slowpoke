<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\service\requestValidator;

use slowpoke\core\library\requestValidator\CoreRequestValidatorItf;

interface
	DemoFrontStatus400RequestValidatorItf
extends
	CoreRequestValidatorItf
{

}