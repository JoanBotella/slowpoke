<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\service\requestValidator;

use demo\book\front\page\status400\service\requestValidator\DemoFrontStatus400RequestValidatorItf;
use demo\book\front\library\requestValidator\DemoFrontRequestValidatorAbs;
use slowpoke\core\library\request\CoreRequest;

final class
	DemoFrontStatus400RequestValidator
extends
	DemoFrontRequestValidatorAbs
implements
	DemoFrontStatus400RequestValidatorItf
{

	public function isValid(CoreRequest $coreRequest):bool
	{
		return true;	// !!!
	}

}
