<?php
declare(strict_types=1);

use demo\book\front\page\status400\service\widget\main\DemoFrontStatus400MainWidgetContext;

/**
 * @var DemoFrontStatus400MainWidgetContext $context
 */

$translation = $context->getTranslation();

?><main data-widget="demo-front-status_400-main">

	<h1><?= $translation->demoFrontStatus400MainWidget_h1Content() ?></h1>

	<p><?= $translation->demoFrontStatus400MainWidget_pContent() ?></p>

</main>