<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\service\widget\main;

use demo\book\front\page\status400\service\widget\main\DemoFrontStatus400MainWidgetContext;

interface
	DemoFrontStatus400MainWidgetItf
{

	public function render(DemoFrontStatus400MainWidgetContext $context):string;

}