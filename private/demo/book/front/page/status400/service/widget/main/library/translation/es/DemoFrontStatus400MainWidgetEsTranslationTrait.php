<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\service\widget\main\library\translation\es;

trait
	DemoFrontStatus400MainWidgetEsTranslationTrait
{

	public function demoFrontStatus400MainWidget_h1Content():string { return 'Petición Incorrecta'; }

	public function demoFrontStatus400MainWidget_pContent():string { return 'Ésta es la página Petición Incorrecta'; }

}