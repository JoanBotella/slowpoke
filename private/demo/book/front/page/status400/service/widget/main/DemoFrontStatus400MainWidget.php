<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\service\widget\main;

use demo\book\front\page\status400\service\widget\main\DemoFrontStatus400MainWidgetContext;
use demo\book\front\page\status400\service\widget\main\DemoFrontStatus400MainWidgetItf;

final class
	DemoFrontStatus400MainWidget
implements
	DemoFrontStatus400MainWidgetItf
{

	public function render(DemoFrontStatus400MainWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/demoFrontStatus400MainWidgetTemplate.php';
		return ob_get_clean();
	}

}