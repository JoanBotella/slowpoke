<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\service\widget\main\library\translation;

interface
	DemoFrontStatus400MainWidgetTranslationItf
{

	public function demoFrontStatus400MainWidget_h1Content():string;

	public function demoFrontStatus400MainWidget_pContent():string;

}