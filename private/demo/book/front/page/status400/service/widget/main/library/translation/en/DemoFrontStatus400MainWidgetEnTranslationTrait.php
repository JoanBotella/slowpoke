<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\service\widget\main\library\translation\en;

trait
	DemoFrontStatus400MainWidgetEnTranslationTrait
{

	public function demoFrontStatus400MainWidget_h1Content():string { return 'Bad Request'; }

	public function demoFrontStatus400MainWidget_pContent():string { return 'This is the Bad Request page'; }

}