<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\service\widget\main;

use demo\book\front\page\status400\library\translation\DemoFrontStatus400TranslationItf;

final class
	DemoFrontStatus400MainWidgetContext
{

	private DemoFrontStatus400TranslationItf $translation;

	public function __construct(
		DemoFrontStatus400TranslationItf $translation
	)
	{
		$this->translation = $translation;
	}

	public function getTranslation():DemoFrontStatus400TranslationItf
	{
		return $this->translation;
	}

}