<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\library\serviceContainer;

use demo\book\front\page\status400\service\controller\DemoFrontStatus400Controller;
use demo\book\front\page\status400\service\controller\DemoFrontStatus400ControllerItf;
use demo\book\front\page\status400\service\requestMatcher\DemoFrontStatus400RequestMatcher;
use demo\book\front\page\status400\service\requestMatcher\DemoFrontStatus400RequestMatcherItf;
use demo\book\front\page\status400\service\requestValidator\DemoFrontStatus400RequestValidator;
use demo\book\front\page\status400\service\requestValidator\DemoFrontStatus400RequestValidatorItf;
use demo\book\front\page\status400\service\responseBuilder\DemoFrontStatus400ResponseBuilder;
use demo\book\front\page\status400\service\responseBuilder\DemoFrontStatus400ResponseBuilderItf;
use demo\book\front\page\status400\service\urlBuilder\DemoFrontStatus400UrlBuilder;
use demo\book\front\page\status400\service\urlBuilder\DemoFrontStatus400UrlBuilderItf;
use demo\book\front\page\status400\service\widget\main\DemoFrontStatus400MainWidget;
use demo\book\front\page\status400\service\widget\main\DemoFrontStatus400MainWidgetItf;

trait
	DemoFrontStatus400ServiceContainerTrait
{

	private DemoFrontStatus400ControllerItf $demoFrontStatus400Controller;

	public function getDemoFrontStatus400Controller():DemoFrontStatus400ControllerItf
	{
		if (!isset($this->demoFrontStatus400Controller))
		{
			$this->demoFrontStatus400Controller = $this->buildDemoFrontStatus400Controller();
		}
		return $this->demoFrontStatus400Controller;
	}

		protected function buildDemoFrontStatus400Controller():DemoFrontStatus400ControllerItf
		{
			return new DemoFrontStatus400Controller(
				$this->getSessionSessionManager(),
				$this->getSessionTranslationContainer(),

				$this->getDemoFrontStatus400ResponseBuilder()
			);
		}

	private DemoFrontStatus400MainWidgetItf $demoFrontStatus400MainWidget;

	public function getDemoFrontStatus400MainWidget():DemoFrontStatus400MainWidgetItf
	{
		if (!isset($this->demoFrontStatus400MainWidget))
		{
			$this->demoFrontStatus400MainWidget = $this->buildDemoFrontStatus400MainWidget();
		}
		return $this->demoFrontStatus400MainWidget;
	}

		protected function buildDemoFrontStatus400MainWidget():DemoFrontStatus400MainWidgetItf
		{
			return new DemoFrontStatus400MainWidget(
			);
		}

	private DemoFrontStatus400RequestMatcherItf $demoFrontStatus400RequestMatcher;

	public function getDemoFrontStatus400RequestMatcher():DemoFrontStatus400RequestMatcherItf
	{
		if (!isset($this->demoFrontStatus400RequestMatcher))
		{
			$this->demoFrontStatus400RequestMatcher = $this->buildDemoFrontStatus400RequestMatcher();
		}
		return $this->demoFrontStatus400RequestMatcher;
	}

		protected function buildDemoFrontStatus400RequestMatcher():DemoFrontStatus400RequestMatcherItf
		{
			return new DemoFrontStatus400RequestMatcher(
				$this->getDemoFrontTranslationContainer()
			);
		}

	private DemoFrontStatus400RequestValidatorItf $demoFrontStatus400RequestValidator;

	public function getDemoFrontStatus400RequestValidator():DemoFrontStatus400RequestValidatorItf
	{
		if (!isset($this->demoFrontStatus400RequestValidator))
		{
			$this->demoFrontStatus400RequestValidator = $this->buildDemoFrontStatus400RequestValidator();
		}
		return $this->demoFrontStatus400RequestValidator;
	}

		protected function buildDemoFrontStatus400RequestValidator():DemoFrontStatus400RequestValidatorItf
		{
			return new DemoFrontStatus400RequestValidator(
			);
		}

	private DemoFrontStatus400ResponseBuilderItf $demoFrontStatus400ResponseBuilder;

	public function getDemoFrontStatus400ResponseBuilder():DemoFrontStatus400ResponseBuilderItf
	{
		if (!isset($this->demoFrontStatus400ResponseBuilder))
		{
			$this->demoFrontStatus400ResponseBuilder = $this->buildDemoFrontStatus400ResponseBuilder();
		}
		return $this->demoFrontStatus400ResponseBuilder;
	}

		protected function buildDemoFrontStatus400ResponseBuilder():DemoFrontStatus400ResponseBuilderItf
		{
			return new DemoFrontStatus400ResponseBuilder(
				$this->configuration_core_protocol(),
				$this->configuration_core_domain(),
				$this->configuration_core_basePath(),
				$this->configuration_core_useMinifiedScripts(),
				$this->configuration_core_useMinifiedStyles(),
				$this->getCoreHtmlWidget(),
	
				$this->configuration_core_applicationName(),
				$this->getDemoFrontTranslationContainer(),
				$this->getDemoFrontHeaderWidget(),
				$this->getDemoFrontNavWidget(),
				$this->getCoreNotificationsWidget(),
				$this->getDemoFrontAsideWidget(),
				$this->getDemoFrontFooterWidget(),
				$this->getDemoFrontBodyWidget(),
				$this->getDemoFrontHomeUrlBuilder(),
				$this->getDemoFrontAboutUrlBuilder(),
		
				$this->getDemoFrontStatus400MainWidget()
			);
		}

	private DemoFrontStatus400UrlBuilderItf $demoFrontStatus400UrlBuilder;

	public function getDemoFrontStatus400UrlBuilder():DemoFrontStatus400UrlBuilderItf
	{
		if (!isset($this->demoFrontStatus400UrlBuilder))
		{
			$this->demoFrontStatus400UrlBuilder = $this->buildDemoFrontStatus400UrlBuilder();
		}
		return $this->demoFrontStatus400UrlBuilder;
	}

		protected function buildDemoFrontStatus400UrlBuilder():DemoFrontStatus400UrlBuilderItf
		{
			return new DemoFrontStatus400UrlBuilder(
				$this->getDemoFrontTranslationContainer()
			);
		}

}