<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\library\serviceContainer;

use demo\book\front\page\status400\service\controller\DemoFrontStatus400ControllerItf;
use demo\book\front\page\status400\service\requestMatcher\DemoFrontStatus400RequestMatcherItf;
use demo\book\front\page\status400\service\requestValidator\DemoFrontStatus400RequestValidatorItf;
use demo\book\front\page\status400\service\responseBuilder\DemoFrontStatus400ResponseBuilderItf;
use demo\book\front\page\status400\service\urlBuilder\DemoFrontStatus400UrlBuilderItf;
use demo\book\front\page\status400\service\widget\main\DemoFrontStatus400MainWidgetItf;

interface
	DemoFrontStatus400ServiceContainerItf
{

	public function getDemoFrontStatus400Controller():DemoFrontStatus400ControllerItf;

	public function getDemoFrontStatus400MainWidget():DemoFrontStatus400MainWidgetItf;

	public function getDemoFrontStatus400RequestMatcher():DemoFrontStatus400RequestMatcherItf;

	public function getDemoFrontStatus400RequestValidator():DemoFrontStatus400RequestValidatorItf;

	public function getDemoFrontStatus400ResponseBuilder():DemoFrontStatus400ResponseBuilderItf;

	public function getDemoFrontStatus400UrlBuilder():DemoFrontStatus400UrlBuilderItf;

}