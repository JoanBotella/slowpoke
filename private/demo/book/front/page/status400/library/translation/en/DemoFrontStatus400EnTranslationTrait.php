<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\library\translation\en;

use demo\book\front\page\status400\service\widget\main\library\translation\en\DemoFrontStatus400MainWidgetEnTranslationTrait;

trait
	DemoFrontStatus400EnTranslationTrait
{
	use
		DemoFrontStatus400MainWidgetEnTranslationTrait
	;

	public function demoFrontStatus400Page_pageName():string { return 'Bad Request'; }

	public function demoFrontStatus400Page_slug():string { return 'bad-request'; }

}
