<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\library\translation;

use demo\book\front\page\status400\service\widget\main\library\translation\DemoFrontStatus400MainWidgetTranslationItf;

interface
	DemoFrontStatus400TranslationItf
extends
	DemoFrontStatus400MainWidgetTranslationItf
{

	public function demoFrontStatus400Page_pageName():string;

	public function demoFrontStatus400Page_slug():string;

}