<?php
declare(strict_types=1);

namespace demo\book\front\page\status400\library\translation\es;

use demo\book\front\page\status400\service\widget\main\library\translation\es\DemoFrontStatus400MainWidgetEsTranslationTrait;

trait
	DemoFrontStatus400EsTranslationTrait
{
	use
		DemoFrontStatus400MainWidgetEsTranslationTrait
	;

	public function demoFrontStatus400Page_pageName():string { return 'Petición Incorrecta'; }

	public function demoFrontStatus400Page_slug():string { return 'peticion-incorrecta'; }

}
