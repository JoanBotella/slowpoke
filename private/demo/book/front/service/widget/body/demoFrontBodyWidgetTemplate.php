<?php
declare(strict_types=1);

use demo\book\front\service\widget\body\DemoFrontBodyWidgetContext;

/**
 * @var DemoFrontBodyWidgetContext $context
 */

?><body data-widget="demo-front-body">

	<?= $context->getHeaderElement() ?>


	<?= $context->getNavElement() ?>

	<?= $context->getNotificationsElement() ?>

	<div class="demo-front-body__middle">
		<?= $context->getMainElement() ?>


		<?= $context->getAsideElement() ?>
	</div>


	<?= $context->getFooterElement() ?>


</body>