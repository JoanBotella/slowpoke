<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\body;

use demo\book\front\service\widget\body\DemoFrontBodyWidgetContext;

interface
	DemoFrontBodyWidgetItf
{

	public function render(DemoFrontBodyWidgetContext $context):string;

}