<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\body;

use demo\book\front\service\widget\body\DemoFrontBodyWidgetContext;
use demo\book\front\service\widget\body\DemoFrontBodyWidgetItf;

final class
	DemoFrontBodyWidget
implements
	DemoFrontBodyWidgetItf
{

	public function render(DemoFrontBodyWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/demoFrontBodyWidgetTemplate.php';
		return ob_get_clean();
	}

}