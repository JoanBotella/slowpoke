<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\body;

final class
	DemoFrontBodyWidgetContext
{

	private string $headerElement;

	private string $navElement;

	private string $notificationsElement;

	private string $asideElement;

	private string $mainElement;

	private string $footerElement;

	public function __construct(
		string $headerElement,
		string $navElement,
		string $notificationsElement,
		string $asideElement,
		string $mainElement,
		string $footerElement
	)
	{
		$this->headerElement = $headerElement;
		$this->navElement = $navElement;
		$this->notificationsElement = $notificationsElement;
		$this->asideElement = $asideElement;
		$this->mainElement = $mainElement;
		$this->footerElement = $footerElement;
	}

	public function getHeaderElement():string
	{
		return $this->headerElement;
	}

	public function getNavElement():string
	{
		return $this->navElement;
	}

	public function getNotificationsElement():string
	{
		return $this->notificationsElement;
	}

	public function getAsideElement():string
	{
		return $this->asideElement;
	}

	public function getMainElement():string
	{
		return $this->mainElement;
	}

	public function getFooterElement():string
	{
		return $this->footerElement;
	}

}