<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\header;

use demo\book\front\service\widget\header\DemoFrontHeaderWidgetContext;

interface
	DemoFrontHeaderWidgetItf
{

	public function render(DemoFrontHeaderWidgetContext $context):string;

}