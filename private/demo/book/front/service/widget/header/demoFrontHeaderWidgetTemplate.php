<?php
declare(strict_types=1);

use demo\book\front\service\widget\header\DemoFrontHeaderWidgetContext;

/**
 * @var DemoFrontHeaderWidgetContext $context
 */

?><header data-widget="demo-front-header">
	<span><?= $context->getAppName() ?></span>
</header>