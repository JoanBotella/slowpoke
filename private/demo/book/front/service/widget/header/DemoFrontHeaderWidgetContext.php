<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\header;

final class
	DemoFrontHeaderWidgetContext
{

	private string $appName;

	public function __construct(
		string $appName
	)
	{
		$this->appName = $appName;
	}

	public function getAppName():string
	{
		return $this->appName;
	}

}