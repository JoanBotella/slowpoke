<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\header;

use demo\book\front\service\widget\header\DemoFrontHeaderWidgetContext;
use demo\book\front\service\widget\header\DemoFrontHeaderWidgetItf;

final class
	DemoFrontHeaderWidget
implements
	DemoFrontHeaderWidgetItf
{

	public function render(DemoFrontHeaderWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/demoFrontHeaderWidgetTemplate.php';
		return ob_get_clean();
	}

}