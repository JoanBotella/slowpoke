<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\footer;

use demo\book\front\service\widget\footer\library\translation\DemoFrontFooterWidgetTranslationItf;

final class
	DemoFrontFooterWidgetContext
{

	private DemoFrontFooterWidgetTranslationItf $translation;

	private string $authorAnchorHref;

	public function __construct(
		DemoFrontFooterWidgetTranslationItf $translation,
		string $authorAnchorHref
	)
	{
		$this->translation = $translation;
		$this->authorAnchorHref = $authorAnchorHref;
	}

	public function getTranslation():DemoFrontFooterWidgetTranslationItf
	{
		return $this->translation;
	}

	public function getAuthorAnchorHref():string
	{
		return $this->authorAnchorHref;
	}

}