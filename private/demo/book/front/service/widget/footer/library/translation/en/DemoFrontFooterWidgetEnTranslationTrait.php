<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\footer\library\translation\en;

trait
	DemoFrontFooterWidgetEnTranslationTrait
{

	public function demoFrontFooterWidget_authorAnchorContent():string { return 'Created by Joan Botella'; }

	public function demoFrontFooterWidget_authorAnchorTitle():string { return 'Go to the author\'s website'; }

}
