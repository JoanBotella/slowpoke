<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\footer\library\translation;

interface
	DemoFrontFooterWidgetTranslationItf
{

	public function demoFrontFooterWidget_authorAnchorContent():string;

	public function demoFrontFooterWidget_authorAnchorTitle():string;

}