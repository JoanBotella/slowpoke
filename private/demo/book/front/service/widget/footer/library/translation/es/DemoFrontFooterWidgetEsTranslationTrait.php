<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\footer\library\translation\es;

trait
	DemoFrontFooterWidgetEsTranslationTrait
{

	public function demoFrontFooterWidget_authorAnchorContent():string { return 'Creado por Joan Botella'; }

	public function demoFrontFooterWidget_authorAnchorTitle():string { return 'Ir a la web del autor'; }

}
