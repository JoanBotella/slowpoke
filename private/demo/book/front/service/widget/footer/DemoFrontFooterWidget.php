<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\footer;

use demo\book\front\service\widget\footer\DemoFrontFooterWidgetContext;
use demo\book\front\service\widget\footer\DemoFrontFooterWidgetItf;

final class
	DemoFrontFooterWidget
implements
	DemoFrontFooterWidgetItf
{

	public function render(DemoFrontFooterWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/demoFrontFooterWidgetTemplate.php';
		return ob_get_clean();
	}

}