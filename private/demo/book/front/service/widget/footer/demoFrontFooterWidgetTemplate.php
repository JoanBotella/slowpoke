<?php
declare(strict_types=1);

use demo\book\front\service\widget\footer\DemoFrontFooterWidgetContext;

/**
 * @var DemoFrontFooterWidgetContext $context
 */

$translation = $context->getTranslation();

?><footer data-widget="demo-front-footer">
	<ul>
		<li>
			<a href="<?= $context->getAuthorAnchorHref() ?>" title="<?= $translation->demoFrontFooterWidget_authorAnchorTitle() ?>" target="_blank"><?= $translation->demoFrontFooterWidget_authorAnchorContent() ?></a>
		</li>
	</ul>
</footer>