<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\footer;

use demo\book\front\service\widget\footer\DemoFrontFooterWidgetContext;

interface
	DemoFrontFooterWidgetItf
{

	public function render(DemoFrontFooterWidgetContext $context):string;

}