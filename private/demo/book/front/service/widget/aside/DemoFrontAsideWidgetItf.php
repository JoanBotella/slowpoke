<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\aside;

use demo\book\front\service\widget\aside\DemoFrontAsideWidgetContext;

interface
	DemoFrontAsideWidgetItf
{

	public function render(DemoFrontAsideWidgetContext $context):string;

}