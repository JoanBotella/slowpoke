<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\aside;

use demo\book\front\service\widget\aside\DemoFrontAsideWidgetContext;
use demo\book\front\service\widget\aside\DemoFrontAsideWidgetItf;

final class
	DemoFrontAsideWidget
implements
	DemoFrontAsideWidgetItf
{

	public function render(DemoFrontAsideWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/demoFrontAsideWidgetTemplate.php';
		return ob_get_clean();
	}

}