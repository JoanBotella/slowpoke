<?php
declare(strict_types=1);

use demo\book\front\service\widget\aside\DemoFrontAsideWidgetContext;

/**
 * @var DemoFrontAsideWidgetContext $context
 */

?><aside data-widget="demo-front-aside">
	<p>Lorem ipsum dolor sit amet.</p>
</aside>