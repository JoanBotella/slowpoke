<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\nav;

use demo\book\front\service\widget\nav\DemoFrontNavWidgetContext;
use demo\book\front\service\widget\nav\DemoFrontNavWidgetItf;

final class
	DemoFrontNavWidget
implements
	DemoFrontNavWidgetItf
{

	public function render(DemoFrontNavWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/demoFrontNavWidgetTemplate.php';
		return ob_get_clean();
	}

}