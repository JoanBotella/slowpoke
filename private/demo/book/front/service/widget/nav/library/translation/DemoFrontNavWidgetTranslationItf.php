<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\nav\library\translation;

interface
	DemoFrontNavWidgetTranslationItf
{

	public function demoFrontNavWidget_aboutAnchorContent():string;

	public function demoFrontNavWidget_aboutAnchorTitle():string;

	public function demoFrontNavWidget_homeAnchorContent():string;

	public function demoFrontNavWidget_homeAnchorTitle():string;

}