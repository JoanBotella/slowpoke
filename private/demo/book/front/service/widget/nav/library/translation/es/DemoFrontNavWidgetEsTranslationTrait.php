<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\nav\library\translation\es;

trait
	DemoFrontNavWidgetEsTranslationTrait
{

	public function demoFrontNavWidget_aboutAnchorContent():string { return 'Sobre Nosotros'; }

	public function demoFrontNavWidget_aboutAnchorTitle():string { return 'Ir a la página Sobre Nosotros'; }

	public function demoFrontNavWidget_homeAnchorContent():string { return 'Inicio'; }

	public function demoFrontNavWidget_homeAnchorTitle():string { return 'Ir a la página Inicio'; }

}