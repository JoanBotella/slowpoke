<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\nav\library\translation\en;

trait
	DemoFrontNavWidgetEnTranslationTrait
{

	public function demoFrontNavWidget_aboutAnchorContent():string { return 'About'; }

	public function demoFrontNavWidget_aboutAnchorTitle():string { return 'Go to the About page'; }

	public function demoFrontNavWidget_homeAnchorContent():string { return 'Home'; }

	public function demoFrontNavWidget_homeAnchorTitle():string { return 'Go to the Home page'; }

}