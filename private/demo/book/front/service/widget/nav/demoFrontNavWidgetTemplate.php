<?php
declare(strict_types=1);

use demo\book\front\service\widget\nav\DemoFrontNavWidgetContext;

/**
 * @var DemoFrontNavWidgetContext $context
 */

$translation = $context->getTranslation();

?><nav data-widget="demo-front-nav">
	<ul>
		<li><a href="<?= $context->getHomeAnchorHref() ?>" title="<?= $translation->demoFrontNavWidget_homeAnchorTitle() ?>"><?= $translation->demoFrontNavWidget_homeAnchorContent() ?></a></li>
		<li><a href="<?= $context->getAboutAnchorHref() ?>" title="<?= $translation->demoFrontNavWidget_aboutAnchorTitle() ?>"><?= $translation->demoFrontNavWidget_aboutAnchorContent() ?></a></li>
	</ul>
</nav>