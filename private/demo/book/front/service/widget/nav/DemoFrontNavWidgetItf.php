<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\nav;

use demo\book\front\service\widget\nav\DemoFrontNavWidgetContext;

interface
	DemoFrontNavWidgetItf
{

	public function render(DemoFrontNavWidgetContext $context):string;

}