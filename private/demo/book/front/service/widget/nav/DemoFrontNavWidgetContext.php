<?php
declare(strict_types=1);

namespace demo\book\front\service\widget\nav;

use demo\book\front\service\widget\nav\library\translation\DemoFrontNavWidgetTranslationItf;

final class
	DemoFrontNavWidgetContext
{

	private DemoFrontNavWidgetTranslationItf $translation;

	private string $homeAnchorHref;

	private string $aboutAnchorHref;

	public function __construct(
		DemoFrontNavWidgetTranslationItf $translation,
		string $homeAnchorHref,
		string $aboutAnchorHref
	)
	{
		$this->translation = $translation;
		$this->homeAnchorHref = $homeAnchorHref;
		$this->aboutAnchorHref = $aboutAnchorHref;
	}

	public function getTranslation():DemoFrontNavWidgetTranslationItf
	{
		return $this->translation;
	}

	public function getHomeAnchorHref():string
	{
		return $this->homeAnchorHref;
	}

	public function getAboutAnchorHref():string
	{
		return $this->aboutAnchorHref;
	}

}