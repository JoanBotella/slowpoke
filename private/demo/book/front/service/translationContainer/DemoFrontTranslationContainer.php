<?php
declare(strict_types=1);

namespace demo\book\front\service\translationContainer;

use Exception;
use slowpoke\core\library\language\CoreEnLanguage;
use slowpoke\core\library\language\CoreEsLanguage;
use demo\book\front\library\translation\en\DemoFrontEnTranslation;
use demo\book\front\library\translation\es\DemoFrontEsTranslation;
use demo\book\front\library\translation\DemoFrontTranslationItf;
use demo\book\front\service\translationContainer\DemoFrontTranslationContainerItf;

final class
	DemoFrontTranslationContainer
implements
	DemoFrontTranslationContainerItf
{

	private array $translations;

	public function getTranslation(string $languageCode):DemoFrontTranslationItf
	{
		if (!isset($this->translations))
		{
			$this->translations = [];
		}

		if (!isset($this->translations[$languageCode]))
		{
			$this->translations[$languageCode] = $this->buildTranslation($languageCode);
		}

		return $this->translations[$languageCode];
	}

	public function buildTranslation(string $languageCode):DemoFrontTranslationItf
	{
		switch ($languageCode)
		{
			case CoreEnLanguage::CODE:
				return new DemoFrontEnTranslation();
			case CoreEsLanguage::CODE:
				return new DemoFrontEsTranslation();
		}
		throw new Exception('Language '.$languageCode.' not supported');
	}

}