<?php
declare(strict_types=1);

namespace demo\book\front\service\translationContainer;

use demo\book\front\library\translation\DemoFrontTranslationItf;

interface
	DemoFrontTranslationContainerItf
{

	public function getTranslation(string $languageCode):DemoFrontTranslationItf;

}