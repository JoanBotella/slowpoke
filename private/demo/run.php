<?php
declare(strict_types=1);

require __DIR__.'/setup.php';

use demo\library\serviceContainer\DemoServiceContainer;

$demoServiceContainer = new DemoServiceContainer();

$coreApp = $demoServiceContainer->getCoreApp();
$coreApp->run();