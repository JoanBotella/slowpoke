<?php
declare(strict_types=1);

namespace demo\library\serviceContainer;

use demo\book\front\library\serviceContainer\DemoFrontServiceContainerItf;
use slowpoke\core\library\serviceContainer\CoreServiceContainerItf;
use slowpoke\session\library\serviceContainer\SessionServiceContainerItf;

interface
	DemoServiceContainerItf
extends
	CoreServiceContainerItf,
	SessionServiceContainerItf,
	DemoFrontServiceContainerItf
{

}
