'use strict';

const
	gulp = require('gulp'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
	minifyJs = require('gulp-terser'),
	uglifyCss = require('gulp-csso'),
	del = require('del'),

	projectDir = '../..',

		privateDir = projectDir + '/private',

			slowpokeDir = privateDir + '/slowpoke',

				slowpokeCoreDir = slowpokeDir + '/core',

			demoDir = privateDir + '/demo',

				demoFrontDir = demoDir + '/book/front',

		publicDir = projectDir + '/public',

			publicCssDir = publicDir + '/css',
			publicImageDir = publicDir + '/image',
			publicJsDir = publicDir + '/js'
;

const deleteDir = function (dir)
{
	del.sync(
		[
			dir + '/**',
		],
		{
			force: true
		}
	);
}

function css(done)
{
	deleteDir(publicCssDir);

	gulp
		.src([
			slowpokeCoreDir + '/asset/css/head.css',
			slowpokeCoreDir + '/service/widget/html/asset/css/style.css',
			slowpokeCoreDir + '/service/widget/notifications/asset/css/style.css',

			demoFrontDir + '/asset/css/style.css',
			demoFrontDir + '/service/widget/header/asset/css/style.css',
			demoFrontDir + '/service/widget/nav/asset/css/style.css',
			demoFrontDir + '/service/widget/aside/asset/css/style.css',
			demoFrontDir + '/service/widget/footer/asset/css/style.css',
			demoFrontDir + '/service/widget/body/asset/css/style.css',

			demoFrontDir + '/page/home/asset/css/style.css',
			demoFrontDir + '/page/home/service/widget/main/asset/css/style.css',

			demoFrontDir + '/page/about/asset/css/style.css',
			demoFrontDir + '/page/about/service/widget/main/asset/css/style.css',

			demoFrontDir + '/page/login/asset/css/style.css',
			demoFrontDir + '/page/login/service/widget/main/asset/css/style.css',

			demoFrontDir + '/page/status400/asset/css/style.css',
			demoFrontDir + '/page/status400/service/widget/main/asset/css/style.css',

			demoFrontDir + '/page/status404/asset/css/style.css',
			demoFrontDir + '/page/status404/service/widget/main/asset/css/style.css',

			demoFrontDir + '/page/status500/asset/css/style.css',
			demoFrontDir + '/page/status500/service/widget/main/asset/css/style.css',

			demoDir + '/asset/css/style.css',
		])
		.pipe(concat('style.css'))
		.pipe(gulp.dest(publicCssDir))
		.pipe(rename('style.min.css'))
		.pipe(uglifyCss())
		.pipe(gulp.dest(publicCssDir))
	;

	done();
}

function img(done)
{
	deleteDir(publicImageDir);

	gulp
		.src([
			slowpokeCoreDir + '/asset/image/**',
			slowpokeCoreDir + '/service/widget/html/asset/image/**',
			slowpokeCoreDir + '/service/widget/notifications/asset/image/**',

			demoFrontDir + '/asset/image/**',
			demoFrontDir + '/service/widget/header/asset/image/**',
			demoFrontDir + '/service/widget/nav/asset/image/**',
			demoFrontDir + '/service/widget/aside/asset/image/**',
			demoFrontDir + '/service/widget/footer/asset/image/**',
			demoFrontDir + '/service/widget/body/asset/image/**',

			demoFrontDir + '/page/home/asset/image/**',
			demoFrontDir + '/page/home/service/widget/main/asset/image/**',

			demoFrontDir + '/page/about/asset/image/**',
			demoFrontDir + '/page/about/service/widget/main/asset/image/**',

			demoFrontDir + '/page/login/asset/image/**',
			demoFrontDir + '/page/login/service/widget/main/asset/image/**',

			demoFrontDir + '/page/status400/asset/image/**',
			demoFrontDir + '/page/status400/service/widget/main/asset/image/**',

			demoFrontDir + '/page/status404/asset/image/**',
			demoFrontDir + '/page/status404/service/widget/main/asset/image/**',

			demoFrontDir + '/page/status500/asset/image/**',
			demoFrontDir + '/page/status500/service/widget/main/asset/image/**',

			demoDir + '/asset/image/**',
		])
		.pipe(gulp.dest(publicImageDir))
	;

	done();
}

function js(done)
{
	deleteDir(publicJsDir);

	gulp
		.src([
			slowpokeCoreDir + '/asset/js/head.js',

			slowpokeCoreDir + '/service/widget/html/asset/js/script.js',
			slowpokeCoreDir + '/service/widget/notifications/asset/js/script.js',

			demoFrontDir + '/asset/js/script.js',
			demoFrontDir + '/service/widget/header/asset/js/script.js',
			demoFrontDir + '/service/widget/nav/asset/js/script.js',
			demoFrontDir + '/service/widget/aside/asset/js/script.js',
			demoFrontDir + '/service/widget/footer/asset/js/script.js',
			demoFrontDir + '/service/widget/body/asset/js/script.js',

			demoFrontDir + '/page/home/asset/js/script.js',
			demoFrontDir + '/page/home/service/widget/main/asset/js/script.js',

			demoFrontDir + '/page/about/asset/js/script.js',
			demoFrontDir + '/page/about/service/widget/main/asset/js/script.js',

			demoFrontDir + '/page/login/asset/js/script.js',
			demoFrontDir + '/page/login/service/widget/main/asset/js/script.js',

			demoFrontDir + '/page/status400/asset/js/script.js',
			demoFrontDir + '/page/status400/service/widget/main/asset/js/script.js',

			demoFrontDir + '/page/status404/asset/js/script.js',
			demoFrontDir + '/page/status404/service/widget/main/asset/js/script.js',

			demoFrontDir + '/page/status500/asset/js/script.js',
			demoFrontDir + '/page/status500/service/widget/main/asset/js/script.js',

			demoDir + '/asset/js/script.js',

			slowpokeCoreDir + '/asset/js/route.js',
			slowpokeCoreDir + '/asset/js/tail.js',
		])
		.pipe(concat('script.js'))
		.pipe(gulp.dest(publicJsDir))
		.pipe(rename('script.min.js'))
		.pipe(minifyJs())
		.pipe(gulp.dest(publicJsDir))
	;

	done();
}

exports.default = gulp.parallel(
	css,
	img,
	js
);
