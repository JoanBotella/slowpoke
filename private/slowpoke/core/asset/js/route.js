
const route = function ()
{
	const root = document.querySelector('[data-page]');

	if (root === undefined)
	{
		return;
	}

	const pageCode = root.getAttribute('data-page');

	if (pageCode === undefined)
	{
		throw 'data-page attribute not found!';
	}

	if (typeof pageSetuppers[pageCode] === 'function')
	{
		pageSetuppers[pageCode]();
	}
}