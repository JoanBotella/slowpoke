<?php
declare(strict_types=1);

namespace slowpoke\core\library\requestValidator;

use slowpoke\core\library\request\CoreRequest;

interface
	CoreRequestValidatorItf
{

	public function isValid(CoreRequest $coreRequest):bool;

}