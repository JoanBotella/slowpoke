<?php
declare(strict_types=1);

namespace slowpoke\core\library\pdo;

final class
	CorePdoInput
{

	private string $dsn;

	private string $username;

	private string $password;

	private array $options;

	public function __construct(
		string $dsn
	)
	{
		$this->dsn = $dsn;
	}

	public function getDsn():string
	{
		return $this->dsn;
	}

	public function hasUsername():bool
	{
		return isset($this->username);
	}

	public function getUsernameAfterHas():string
	{
		return $this->username;
	}

	public function setUsername(string $v):void
	{
		$this->username = $v;
	}

	public function unsetUsername():void
	{
		unset($this->username);
	}

	public function hasPassword():bool
	{
		return isset($this->password);
	}

	public function getPasswordAfterHas():string
	{
		return $this->password;
	}

	public function setPassword(string $v):void
	{
		$this->password = $v;
	}

	public function unsetPassword():void
	{
		unset($this->password);
	}

	public function hasOptions():bool
	{
		return isset($this->options);
	}

	public function getOptionsAfterHas():array
	{
		return $this->options;
	}

	public function setOptions(array $v):void
	{
		$this->options = $v;
	}

	public function unsetOptions():void
	{
		unset($this->options);
	}

}