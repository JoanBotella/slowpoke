<?php
declare(strict_types=1);

namespace slowpoke\core\library\route;

use slowpoke\core\library\controller\CoreControllerItf;
use slowpoke\core\library\requestMatcher\CoreRequestMatcherItf;
use slowpoke\core\library\requestValidator\CoreRequestValidatorItf;

interface
	CoreRouteItf
{

	public function getController():CoreControllerItf;

	public function getRequestMatcher():CoreRequestMatcherItf;

	public function getRequestValidator():CoreRequestValidatorItf;

}