<?php
declare(strict_types=1);

namespace slowpoke\core\library\requestMatcher;

use slowpoke\core\library\request\CoreRequest;

interface
	CoreRequestMatcherItf
{

	public function isMatch(CoreRequest $coreRequest):bool;

}