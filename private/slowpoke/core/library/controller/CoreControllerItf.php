<?php
declare(strict_types=1);

namespace slowpoke\core\library\controller;

use slowpoke\core\library\request\CoreRequest;
use slowpoke\core\library\response\CoreResponse;

interface
	CoreControllerItf
{

	public function run(CoreRequest $coreRequest):CoreResponse;

}