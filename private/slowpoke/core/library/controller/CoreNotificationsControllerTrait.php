<?php
declare(strict_types=1);

namespace slowpoke\core\library\controller;

use slowpoke\core\library\CoreNotifications;

trait
	CoreNotificationsControllerTrait
{

	private CoreNotifications $coreNotifications;

	protected function getCoreNotifications():CoreNotifications
	{
		if (!isset($this->coreNotifications))
		{
			$this->coreNotifications = $this->buildCoreNotifications();
		}
		return $this->coreNotifications;
	}

		protected function buildCoreNotifications():CoreNotifications
		{
			return new CoreNotifications();
		}

	protected function setCoreNotifications(CoreNotifications $coreNotifications):void
	{
		$this->coreNotifications = $coreNotifications;
	}

}