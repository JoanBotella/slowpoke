<?php
declare(strict_types=1);

namespace slowpoke\core\library\controller;

use slowpoke\core\library\request\CoreRequest;
use slowpoke\core\library\response\CoreResponse;

abstract class
	CoreControllerAbs
{

	private CoreRequest $coreRequest;

	private CoreResponse $coreResponse;

	public function run(CoreRequest $coreRequest):CoreResponse
	{
		$this->coreRequest = $coreRequest;

		$this->setup();

		$this->act();

		$this->coreResponse = $this->buildResponse();

		$this->tearDown();

		$coreResponse = $this->coreResponse;

		$this->reset();

		return $coreResponse;
	}

		protected function setup():void
		{
		}

		protected function act():void
		{

		}

		abstract protected function buildResponse():CoreResponse;

		protected function tearDown():void
		{
		}

		protected function reset():void
		{
			unset($this->coreRequest);
			unset($this->coreResponse);
		}

	protected function getCoreRequest():CoreRequest
	{
		return $this->coreRequest;
	}

	protected function setCoreRequest(CoreRequest $v):void
	{
		$this->coreRequest = $v;
	}

	protected function getCoreResponse():CoreResponse
	{
		return $this->coreResponse;
	}

	protected function setCoreResponse(CoreResponse $v):void
	{
		$this->coreResponse = $v;
	}

}
