<?php
declare(strict_types=1);

namespace slowpoke\core\library;

final class
	CoreNotifications
{

	private array $successes;

	private array $infos;

	private array $warnings;

	private array $errors;

	public function addSuccess(string $message):void
	{
		if (!$this->hasSuccesses())
		{
			$this->successes = [];
		}
		$this->successes[] = $message;
	}

	public function hasSuccesses():bool
	{
		return isset($this->successes);
	}

	public function getSuccessesAfterHas():array
	{
		return $this->successes;
	}

	public function addInfo(string $message):void
	{
		if (!$this->hasInfos())
		{
			$this->infos = [];
		}
		$this->infos[] = $message;
	}

	public function hasInfos():bool
	{
		return isset($this->infos);
	}

	public function getInfosAfterHas():array
	{
		return $this->infos;
	}

	public function addWarning(string $message):void
	{
		if (!$this->hasWarnings())
		{
			$this->warnings = [];
		}
		$this->warnings[] = $message;
	}

	public function hasWarnings():bool
	{
		return isset($this->warnings);
	}

	public function getWarningsAfterHas():array
	{
		return $this->warnings;
	}

	public function addError(string $message):void
	{
		if (!$this->hasErrors())
		{
			$this->errors = [];
		}
		$this->errors[] = $message;
	}

	public function hasErrors():bool
	{
		return isset($this->errors);
	}

	public function getErrorsAfterHas():array
	{
		return $this->errors;
	}

}