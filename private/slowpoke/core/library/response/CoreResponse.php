<?php
declare(strict_types=1);

namespace slowpoke\core\library\response;

use slowpoke\core\library\cookie\CoreCookie;

final class
	CoreResponse
{

	private int $httpStatus;

	private string $body;

	private array $cookies;

	public function __construct(
		int $httpStatus,
		string $body
	)
	{
		$this->httpStatus = $httpStatus;
		$this->body = $body;
		$this->cookies = [];
	}

	public function getHttpStatus():Int
	{
		return $this->httpStatus;
	}

	public function getBody():string
	{
		return $this->body;
	}

	public function addCookie(CoreCookie $cookie):void
	{
		$this->cookies[] = $cookie;
	}

	public function getCookies():array
	{
		return $this->cookies;
	}

	public function unsetCookies():void
	{
		$this->cookies = [];
	}

}