<?php
declare(strict_types=1);

namespace slowpoke\core\library\responseBuilder;

use Exception;
use slowpoke\core\library\http\CoreHttpStatusConstant;
use slowpoke\core\library\response\CoreResponse;
use slowpoke\core\service\widget\html\CoreHtmlWidgetContext;
use slowpoke\core\service\widget\html\CoreHtmlWidgetItf;

abstract class
	CoreResponseBuilderAbs
{

	private string $protocol;

	private string $domain;

	private string $basePath;

	private bool $useMinifiedScripts;

	private bool $useMinifiedStyles;

	private CoreHtmlWidgetItf $coreHtmlWidget;

	public function __construct(
		string $protocol,
		string $domain,
		string $basePath,
		bool $useMinifiedScripts,
		bool $useMinifiedStyles,
		CoreHtmlWidgetItf $coreHtmlWidget
	)
	{
		$this->protocol = $protocol;
		$this->domain = $domain;
		$this->basePath = $basePath;
		$this->useMinifiedScripts = $useMinifiedScripts;
		$this->useMinifiedStyles = $useMinifiedStyles;
		$this->coreHtmlWidget = $coreHtmlWidget;
	}

	protected function buildCoreResponse():CoreResponse
	{
		$coreResponse = new CoreResponse(
			$this->getHttpStatusCode(),
			$this->buildHtmlElement()
		);

		$coreResponse = $this->addCookies($coreResponse);

		return $coreResponse;
	}

		protected function getHttpStatusCode():int
		{
			return CoreHttpStatusConstant::OK;
		}

		private function buildHtmlElement():string
		{
			$context = new CoreHtmlWidgetContext(
				$this->getTitleContent(),
				$this->getLanguageCode(),
				$this->getPageCode(),
				$this->buildBaseHref(),
				$this->buildBodyElement()
			);

			$context->setFaviconUrl(
				$this->getFaviconUrl()
			);

			$context->setScriptUrls(
				$this->buildScriptUrls()
			);

			$context->setStyleUrls(
				$this->buildStyleUrls()
			);

			if ($this->hasCanonicalHref())
			{
				$context->setCanonicalHref(
					$this->getCanonicalHrefAfterHas()
				);
			}

			return $this->coreHtmlWidget->render(
				$context
			);
		}

			abstract protected function getTitleContent():string;

			abstract protected function getLanguageCode():string;

			abstract protected function getPageCode():string;

			private function buildBaseHref():string
			{
				return
					$this->protocol
					.'://'
					.$this->domain
					.'/'
					.$this->basePath
					.'/'
				;
			}

			abstract protected function buildBodyElement():string;

			protected function getFaviconUrl():string
			{
				return 'image/favicon.png';
			}

			protected function buildScriptUrls():array
			{
				return
					$this->useMinifiedScripts
						? [
							'js/script.min.js',
						]
						: [
							'js/script.js',
						]
				;
			}

			protected function buildStyleUrls():array
			{
				return
					$this->useMinifiedStyles
						? [
							'css/style.min.css',
						]
						: [
							'css/style.css',
						]
				;
			}

			protected function hasCanonicalHref():bool
			{
				return false;
			}

			protected function getCanonicalHrefAfterHas():string
			{
				throw new Exception('The canonical href has not been defined');
			}

		private function addCookies(CoreResponse $coreResponse):CoreResponse
		{
			foreach ($this->buildCookies() as $coreCookie)
			{
				$coreResponse->addCookie(
					$coreCookie
				);
			}
			return $coreResponse;
		}

			protected function buildCookies():array
			{
				return [
				];
			}

}