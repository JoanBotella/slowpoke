<?php
declare(strict_types=1);

namespace slowpoke\core\library;

final class
	CoreTimeConstants
{

	const SECONDS_IN_30_MINUTES = 1800;

}