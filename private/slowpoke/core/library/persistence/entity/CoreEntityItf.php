<?php
declare(strict_types=1);

namespace slowpoke\core\library\persistence\entity;

use DateTimeInterface;

interface
	CoreEntityItf
{

	public function hasId():bool;

	public function getIdAfterHas():int;

	public function setId(int $v):void;

	public function unsetId():void;

	public function hasCreatedAt():bool;

	public function getCreatedAtAfterHas():DateTimeInterface;

	public function setCreatedAt(DateTimeInterface $v):void;

	public function unsetCreatedAt():void;

	public function hasUpdatedAt():bool;

	public function getUpdatedAtAfterHas():DateTimeInterface;

	public function setUpdatedAt(DateTimeInterface $v):void;

	public function unsetUpdatedAt():void;

}