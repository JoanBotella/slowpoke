<?php
declare(strict_types=1);

namespace slowpoke\core\library\persistence\entity;

use DateTimeInterface;

abstract class
	CoreEntityAbs
implements
	CoreEntityItf
{

	private int $id;

	public function hasId():bool
	{
		return isset($this->id);
	}

	public function getIdAfterHas():int
	{
		return $this->id;
	}

	public function setId(int $v):void
	{
		$this->id = $v;
	}

	public function unsetId():void
	{
		unset($this->id);
	}

	private DateTimeInterface $createdAt;

	public function hasCreatedAt():bool
	{
		return isset($this->createdAt);
	}

	public function getCreatedAtAfterHas():DateTimeInterface
	{
		return $this->createdAt;
	}

	public function setCreatedAt(DateTimeInterface $v):void
	{
		$this->createdAt = $v;
	}

	public function unsetCreatedAt():void
	{
		unset($this->createdAt);
	}

	private DateTimeInterface $updatedAt;

	public function hasUpdatedAt():bool
	{
		return isset($this->updatedAt);
	}

	public function getUpdatedAtAfterHas():DateTimeInterface
	{
		return $this->updatedAt;
	}

	public function setUpdatedAt(DateTimeInterface $v):void
	{
		$this->updatedAt = $v;
	}

	public function unsetUpdatedAt():void
	{
		unset($this->updatedAt);
	}

}