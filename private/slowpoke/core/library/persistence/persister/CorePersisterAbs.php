<?php
declare(strict_types=1);

namespace slowpoke\core\library\persistence\persister;

abstract class
	CorePersisterAbs
{

	private int $affectedRows;

	public function hasAffectedRows():bool
	{
		return isset($this->affectedRows);
	}

	public function getAffectedRowsAfterHas():int
	{
		return $this->affectedRows;
	}

	protected function setAffectedRows(int $v):void
	{
		$this->affectedRows = $v;
	}

	protected function unsetAffectedRows():void
	{
		unset($this->affectedRows);
	}

}