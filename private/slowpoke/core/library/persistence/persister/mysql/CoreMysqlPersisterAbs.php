<?php
declare(strict_types=1);

namespace slowpoke\core\library\persistence\persister\mysql;

use DateTime;
use DateTimeInterface;
use PDO;
use slowpoke\core\library\persistence\persister\CorePdoPersisterAbs;

abstract class
	CoreMysqlPersisterAbs
extends
	CorePdoPersisterAbs
{

	abstract protected function getTable():string;

	abstract protected function addEntityToResultByRow(array $result, array $row):array;

	protected function selectWhere(string $where, array $bindings):array
	{
		$sql = '
			SELECT
				*
			FROM
				`'.$this->getTable().'`
			WHERE
				'.$where.'
			;
		';

		$statement = $this->query(
			$sql,
			$bindings
		);

		$result = [];

		foreach ($statement as $row)
		{
			$result = $this->addEntityToResultByRow($result, $row);
		}

		return $result;
	}

	protected function insertColumns(array $columns, array $bindings):void
	{
		$backtickedColumns = [];
		$marks = [];

		foreach ($columns as $column)
		{
			$backtickedColumns[] = '`'.$column.'`';
			$marks[] = '?';
		}

		$sql = '
			INSERT INTO
				`'.$this->getTable().'`
			(
		';

		$sql .= implode(
			',',
			$backtickedColumns
		);

		$sql .= '
			)
			VALUES
			(
		';

		$sql .= implode(
			',',
			$marks
		);

		$sql .= '
			);
		';

		$this->query(
			$sql,
			$bindings
		);
	}

	protected function updateWhere(string $set, string $where, array $bindings):void
	{
		$sql = '
			UPDATE
				`'.$this->getTable().'`
			SET
				'.$set.'
			WHERE
				'.$where.'
			;
		';

		$this->query(
			$sql,
			$bindings
		);
	}

	protected function deleteWhere(string $where, array $bindings):void
	{
		$sql = '
			DELETE FROM
				`'.$this->getTable().'`
			WHERE
				'.$where.'
			;
		';

		$this->query(
			$sql,
			$bindings
		);
	}

	protected function buildDateTimeByString(string $dateTimeString):DateTimeInterface
	{
		return DateTime::createFromFormat('Y-m-d H:i:s', $dateTimeString);
	}

	protected function buildStringByDateTime(DateTimeInterface $dateTime):string
	{
		return $dateTime->format('Y-m-d H:i:s');
	}

}