<?php
declare(strict_types=1);

namespace slowpoke\core\library\persistence\persister;

use Exception;
use slowpoke\core\library\persistence\persister\CorePersisterAbs;
use PDO;
use PDOStatement;
use slowpoke\core\service\pdoContainer\CorePdoContainerItf;

abstract class
	CorePdoPersisterAbs
extends
	CorePersisterAbs
{

	private CorePdoContainerItf $corePdoContainer;

	public function __construct(
		CorePdoContainerItf $corePdoContainer
	)
	{
		$this->corePdoContainer = $corePdoContainer;
	}

	protected function getPdo():PDO
	{
		return $this->corePdoContainer->get();
	}

	protected function query(string $sql, array $bindings):PDOStatement
	{
		$this->unsetAffectedRows();

		$pdo = $this->getPdo();
		$statement = $this->buildStatement($pdo, $sql, $bindings);

		if ($statement->execute() === false)
		{
			$errorInfo = $statement->errorInfo();
			throw new Exception('It was an error on the database query: '.print_r($errorInfo,true));
		}

		$this->affectedRows = $statement->rowCount();

		return $statement;
	}

		private function buildStatement(PDO $pdo, string $sql, array $bindings):PDOStatement
		{
			$statement = $pdo->prepare($sql);
	
			$bindingsLength = count($bindings);
			for ($i = 0; $i < $bindingsLength; $i++)
			{
				$binding = $bindings[$i];
				$value = $binding[0];
				$type = $binding[1];
				$statement->bindValue($i + 1, $value, $type);
			}

			return $statement;
		}

}