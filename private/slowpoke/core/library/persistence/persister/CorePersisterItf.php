<?php
declare(strict_types=1);

namespace slowpoke\core\library\persistence\persister;

interface
	CorePersisterItf
{

	public function hasAffectedRows():bool;

	public function getAffectedRowsAfterHas():int;

}