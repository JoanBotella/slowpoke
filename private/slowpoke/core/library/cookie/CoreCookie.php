<?php
declare(strict_types=1);

namespace slowpoke\core\library\cookie;

use DateTime;

final class
	CoreCookie
{

	private string $name;

	private string $value;

	private DateTime $expires;

	private int $maxAge;

	private string $domain;

	private string $path;

	private bool $secure;

	private bool $httpOnly;

	private string $sameSite;

	public function __construct(
		string $name
	)
	{
		$this->name = $name;
	}

	public function getName():string
	{
		return $this->name;
	}

	public function setName(string $v):void
	{
		$this->name = $v;
	}

	public function hasValue():bool
	{
		return isset($this->value);
	}

	public function getValueAfterHas():string
	{
		return $this->value;
	}

	public function setValue(string $v):void
	{
		$this->value = $v;
	}

	public function unsetValue():void
	{
		unset($this->value);
	}

	public function hasExpires():bool
	{
		return isset($this->expires);
	}

	public function getExpiresAfterHas():DateTime
	{
		return $this->expires;
	}

	public function setExpires(DateTime $v):void
	{
		$this->expires = $v;
	}

	public function unsetExpires():void
	{
		unset($this->expires);
	}

	public function hasMaxAge():bool
	{
		return isset($this->maxAge);
	}

	public function getMaxAgeAfterHas():int
	{
		return $this->maxAge;
	}

	public function setMaxAge(int $v):void
	{
		$this->maxAge = $v;
	}

	public function unsetMaxAge():void
	{
		unset($this->maxAge);
	}

	public function hasDomain():bool
	{
		return isset($this->domain);
	}

	public function getDomainAfterHas():string
	{
		return $this->domain;
	}

	public function setDomain(string $v):void
	{
		$this->domain = $v;
	}

	public function unsetDomain():void
	{
		unset($this->domain);
	}

	public function hasPath():bool
	{
		return isset($this->path);
	}

	public function getPathAfterHas():string
	{
		return $this->path;
	}

	public function setPath(string $v):void
	{
		$this->path = $v;
	}

	public function unsetPath():void
	{
		unset($this->path);
	}

	public function hasSecure():bool
	{
		return isset($this->secure);
	}

	public function getSecureAfterHas():bool
	{
		return $this->secure;
	}

	public function setSecure(bool $v):void
	{
		$this->secure = $v;
	}

	public function unsetSecure():void
	{
		unset($this->secure);
	}

	public function hasHttpOnly():bool
	{
		return isset($this->httpOnly);
	}

	public function getHttpOnlyAfterHas():bool
	{
		return $this->httpOnly;
	}

	public function setHttpOnly(bool $v):void
	{
		$this->httpOnly = $v;
	}

	public function unsetHttpOnly():void
	{
		unset($this->httpOnly);
	}

	public function hasSameSite():bool
	{
		return isset($this->sameSite);
	}

	public function getSameSiteAfterHas():string
	{
		return $this->sameSite;
	}

	public function setSameSite(string $v):void
	{
		$this->sameSite = $v;
	}

	public function unsetSameSite():void
	{
		unset($this->sameSite);
	}

}