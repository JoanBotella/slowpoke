<?php
declare(strict_types=1);

namespace slowpoke\core\library\cookie;

final class
	CoreCookieSameSiteConstant
{

	const NONE = 'None';

	const LAX = 'Lax';

	const STRICT = 'Strict';

}