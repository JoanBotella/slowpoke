<?php
declare(strict_types=1);

namespace slowpoke\core\library\request;

final class
	CoreRequest
{

	private string $languageCode;

	private string $path;

	private array $pathSegments;

	private array $cookies;

	private array $getParameters;

	private array $postParameters;

	public function __construct(
		string $languageCode,
		string $path,
		array $pathSegments,
		array $cookies,
		array $getParameters,
		array $postParameters
	)
	{
		$this->languageCode = $languageCode;
		$this->path = $path;
		$this->pathSegments = $pathSegments;
		$this->cookies = $cookies;
		$this->getParameters = $getParameters;
		$this->postParameters = $postParameters;
	}

	public function getLanguageCode():string
	{
		return $this->languageCode;
	}

	public function getPath():string
	{
		return $this->path;
	}

	public function getPathSegments():array
	{
		return $this->pathSegments;
	}

	public function getCookies():array
	{
		return $this->cookies;
	}

	public function getGetParameters():array
	{
		return $this->getParameters;
	}

	public function getPostParameters():array
	{
		return $this->postParameters;
	}

}