<?php
declare(strict_types=1);

namespace slowpoke\core\library\serviceContainer;

use slowpoke\core\library\controller\CoreControllerItf;
use slowpoke\core\library\language\CoreLanguageItf;
use slowpoke\core\library\pdo\CorePdoInput;
use slowpoke\core\service\app\CoreApp;
use slowpoke\core\service\app\CoreAppItf;
use slowpoke\core\service\cookieDispatcher\CoreCookieDispatcher;
use slowpoke\core\service\cookieDispatcher\CoreCookieDispatcherItf;
use slowpoke\core\service\pdoContainer\CorePdoContainer;
use slowpoke\core\service\pdoContainer\CorePdoContainerItf;
use slowpoke\core\service\requestBuilder\CoreRequestBuilder;
use slowpoke\core\service\requestBuilder\CoreRequestBuilderItf;
use slowpoke\core\service\responseDispatcher\CoreResponseDispatcher;
use slowpoke\core\service\responseDispatcher\CoreResponseDispatcherItf;
use slowpoke\core\service\router\CoreRouter;
use slowpoke\core\service\router\CoreRouterItf;
use slowpoke\core\service\widget\html\CoreHtmlWidget;
use slowpoke\core\service\widget\html\CoreHtmlWidgetItf;
use slowpoke\core\service\widget\notifications\CoreNotificationsWidget;
use slowpoke\core\service\widget\notifications\CoreNotificationsWidgetItf;

trait
	CoreServiceContainerTrait
{

	abstract protected function configuration_core_protocol():string;

	abstract protected function configuration_core_domain():string;

	abstract protected function configuration_core_basePath():string;

	abstract protected function configuration_core_defaultLanguage():CoreLanguageItf;

	abstract protected function configuration_core_supportedLanguages():array;

	abstract protected function configuration_core_useMinifiedScripts():bool;

	abstract protected function configuration_core_useMinifiedStyles():bool;

	abstract protected function configuration_core_applicationName():string;

	abstract protected function configuration_core_routes():array;

	abstract protected function configuration_core_internalServerErrorController():CoreControllerItf;

	abstract protected function configuration_core_badRequestController():CoreControllerItf;

	abstract protected function configuration_core_notFoundController():CoreControllerItf;

	abstract protected function configuration_core_corePdoInput():CorePdoInput;

	// ---

	private CoreAppItf $coreApp;

	public function getCoreApp():CoreAppItf
	{
		if (!isset($this->coreApp))
		{
			$this->coreApp = $this->buildCoreApp();
		}
		return $this->coreApp;
	}

		protected function buildCoreApp():CoreAppItf
		{
			return new CoreApp(
				$this->getCoreRequestBuilder(),
				$this->getCoreRouter(),
				$this->getCoreResponseDispatcher(),
				$this->configuration_core_internalServerErrorController()
			);
		}

	private CoreCookieDispatcherItf $coreCookieDispatcher;

	public function getCoreCookieDispatcher():CoreCookieDispatcherItf
	{
		if (!isset($this->coreCookieDispatcher))
		{
			$this->coreCookieDispatcher = $this->buildCoreCookieDispatcher();
		}
		return $this->coreCookieDispatcher;
	}

		protected function buildCoreCookieDispatcher():CoreCookieDispatcherItf
		{
			return new CoreCookieDispatcher(
			);
		}

	private CoreHtmlWidgetItf $coreHtmlWidget;

	public function getCoreHtmlWidget():CoreHtmlWidgetItf
	{
		if (!isset($this->coreHtmlWidget))
		{
			$this->coreHtmlWidget = $this->buildCoreHtmlWidget();
		}
		return $this->coreHtmlWidget;
	}

		protected function buildCoreHtmlWidget():CoreHtmlWidgetItf
		{
			return new CoreHtmlWidget(
			);
		}

	private CoreNotificationsWidgetItf $coreNotificationsWidget;

	public function getCoreNotificationsWidget():CoreNotificationsWidgetItf
	{
		if (!isset($this->coreNotificationsWidget))
		{
			$this->coreNotificationsWidget = $this->buildCoreNotificationsWidget();
		}
		return $this->coreNotificationsWidget;
	}

		protected function buildCoreNotificationsWidget():CoreNotificationsWidgetItf
		{
			return new CoreNotificationsWidget(
			);
		}

	private CorePdoContainerItf $corePdoContainer;

	public function getCorePdoContainer():CorePdoContainerItf
	{
		if (!isset($this->corePdoContainer))
		{
			$this->corePdoContainer = $this->buildCorePdoContainer();
		}
		return $this->corePdoContainer;
	}

		protected function buildCorePdoContainer():CorePdoContainerItf
		{
			return new CorePdoContainer(
				$this->configuration_core_corePdoInput()
			);
		}

	private CoreRequestBuilderItf $coreRequestBuilder;

	public function getCoreRequestBuilder():CoreRequestBuilderItf
	{
		if (!isset($this->coreRequestBuilder))
		{
			$this->coreRequestBuilder = $this->buildCoreRequestBuilder();
		}
		return $this->coreRequestBuilder;
	}

		protected function buildCoreRequestBuilder():CoreRequestBuilderItf
		{
			return new CoreRequestBuilder(
				$this->configuration_core_basePath(),
				$this->configuration_core_defaultLanguage(),
				$this->configuration_core_supportedLanguages()
			);
		}

	private CoreResponseDispatcherItf $coreResponseDispatcher;

	public function getCoreResponseDispatcher():CoreResponseDispatcherItf
	{
		if (!isset($this->coreResponseDispatcher))
		{
			$this->coreResponseDispatcher = $this->buildCoreResponseDispatcher();
		}
		return $this->coreResponseDispatcher;
	}

		protected function buildCoreResponseDispatcher():CoreResponseDispatcherItf
		{
			return new CoreResponseDispatcher(
				$this->getCoreCookieDispatcher()
			);
		}

	private CoreRouterItf $coreRouter;

	public function getCoreRouter():CoreRouterItf
	{
		if (!isset($this->coreRouter))
		{
			$this->coreRouter = $this->buildCoreRouter();
		}
		return $this->coreRouter;
	}

		protected function buildCoreRouter():CoreRouterItf
		{
			return new CoreRouter(
				$this->configuration_core_routes(),
				$this->configuration_core_badRequestController(),
				$this->configuration_core_notFoundController()
			);
		}

}