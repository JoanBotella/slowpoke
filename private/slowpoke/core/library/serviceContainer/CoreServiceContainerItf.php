<?php
declare(strict_types=1);

namespace slowpoke\core\library\serviceContainer;

use slowpoke\core\service\app\CoreAppItf;
use slowpoke\core\service\cookieDispatcher\CoreCookieDispatcherItf;
use slowpoke\core\service\pdoContainer\CorePdoContainerItf;
use slowpoke\core\service\requestBuilder\CoreRequestBuilderItf;
use slowpoke\core\service\responseDispatcher\CoreResponseDispatcherItf;
use slowpoke\core\service\router\CoreRouterItf;
use slowpoke\core\service\widget\html\CoreHtmlWidgetItf;
use slowpoke\core\service\widget\notifications\CoreNotificationsWidgetItf;

interface
	CoreServiceContainerItf
{

	public function getCoreApp():CoreAppItf;

	public function getCoreCookieDispatcher():CoreCookieDispatcherItf;

	public function getCoreHtmlWidget():CoreHtmlWidgetItf;

	public function getCoreNotificationsWidget():CoreNotificationsWidgetItf;

	public function getCorePdoContainer():CorePdoContainerItf;

	public function getCoreRequestBuilder():CoreRequestBuilderItf;

	public function getCoreResponseDispatcher():CoreResponseDispatcherItf;

	public function getCoreRouter():CoreRouterItf;

}