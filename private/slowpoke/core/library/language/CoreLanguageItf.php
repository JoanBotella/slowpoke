<?php
declare(strict_types=1);

namespace slowpoke\core\library\language;

interface
	CoreLanguageItf
{

	public function getCode():string;

	public function getName():string;

	public function getEnglishName():string;

}