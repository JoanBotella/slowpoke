<?php
declare(strict_types=1);

namespace slowpoke\core\library\language;

use slowpoke\core\library\language\CoreLanguageItf;

final class
	CoreEsLanguage
implements
	CoreLanguageItf
{
	const CODE = 'es';

	public function getCode():string
	{
		return static::CODE;
	}

	public function getName():string
	{
		return 'castellano';
	}

	public function getEnglishName():string
	{
		return 'Castillian';
	}

}
