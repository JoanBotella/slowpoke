<?php
declare(strict_types=1);

namespace slowpoke\core\library\language;

use slowpoke\core\library\language\CoreLanguageItf;

final class
	CoreEnLanguage
implements
	CoreLanguageItf
{
	const CODE = 'en';

	public function getCode():string
	{
		return static::CODE;
	}

	public function getName():string
	{
		return 'English';
	}

	public function getEnglishName():string
	{
		return 'English';
	}

}
