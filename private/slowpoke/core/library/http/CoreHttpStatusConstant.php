<?php
declare(strict_types=1);

namespace slowpoke\core\library\http;

final class
	CoreHttpStatusConstant
{

	const OK = 200;

	const BAD_REQUEST = 400;

	const NOT_FOUND = 404;

	const INTERNAL_SERVER_ERROR = 500;

}