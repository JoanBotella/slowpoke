<?php
declare(strict_types=1);

namespace slowpoke\core\library\mapWrapper;

abstract class
	CoreStringMapWrapperAbs
{

	private array $map;

	public function __construct(
		array $map
	)
	{
		$this->map = $map;
	}

	protected function has(string $key):bool
	{
		return isset($this->map[$key]);
	}

	protected function getAfterHas(string $key):string
	{
		return $this->map[$key];
	}

	protected function set(string $key, string $value):void
	{
		$this->map[$key] = $value;
	}

	protected function unset(string $key):void
	{
		unset($this->map[$key]);
	}

}