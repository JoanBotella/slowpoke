<?php
declare(strict_types=1);

namespace slowpoke\core\service\cookieDispatcher;

use slowpoke\core\library\cookie\CoreCookie;

interface
	CoreCookieDispatcherItf
{

	public function dispatch(CoreCookie $coreCookie):void;

}