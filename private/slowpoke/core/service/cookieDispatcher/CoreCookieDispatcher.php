<?php
declare(strict_types=1);

namespace slowpoke\core\service\cookieDispatcher;

use slowpoke\core\library\cookie\CoreCookie;
use slowpoke\core\service\cookieDispatcher\CoreCookieDispatcherItf;

final class
	CoreCookieDispatcher
implements
	CoreCookieDispatcherItf
{

	public function dispatch(CoreCookie $coreCookie):void
	{
		$value =
			$coreCookie->hasValue()
				? $coreCookie->getValueAfterHas()
				: ''
		;

		$options = $this->buildOptions($coreCookie);

		setcookie(
			$coreCookie->getName(),
			$value,
			$options
		);
	}

		private function buildOptions(CoreCookie $coreCookie):array
		{
			$options = [];

			if ($coreCookie->hasExpires())
			{
				$options['expires'] = $coreCookie->getExpiresAfterHas()->getTimestamp();
			}
	
			if ($coreCookie->hasPath())
			{
				$options['path'] = $coreCookie->getPathAfterHas();
			}
	
			if ($coreCookie->hasDomain())
			{
				$options['domain'] = $coreCookie->getDomainAfterHas();
			}
	
			if ($coreCookie->hasSecure())
			{
				$options['secure'] = $coreCookie->getSecureAfterHas();
			}
	
			if ($coreCookie->hasHttpOnly())
			{
				$options['httponly'] = $coreCookie->getHttpOnlyAfterHas();
			}
	
			if ($coreCookie->hasSameSite())
			{
				$options['samesite'] = $coreCookie->getSameSiteAfterHas();
			}

			return $options;
		}

}