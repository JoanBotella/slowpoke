<?php
declare(strict_types=1);

namespace slowpoke\core\service\responseDispatcher;

use slowpoke\core\library\cookie\CoreCookie;
use slowpoke\core\library\response\CoreResponse;
use slowpoke\core\service\cookieDispatcher\CoreCookieDispatcherItf;
use slowpoke\core\service\responseDispatcher\CoreResponseDispatcherItf;

final class
	CoreResponseDispatcher
implements
	CoreResponseDispatcherItf
{

	private CoreCookieDispatcherItf $coreCookieDispatcher;

	private CoreResponse $coreResponse;

	public function __construct(
		CoreCookieDispatcherItf $coreCookieDispatcher
	)
	{
		$this->coreCookieDispatcher = $coreCookieDispatcher;
	}

	public function dispatch(CoreResponse $coreResponse):void
	{
		$this->coreResponse = $coreResponse;

		$this->dispatchHttpStatus();

		$this->dispatchCookies();

		$this->dispatchBody();

		$this->reset();
	}

		private function dispatchHttpStatus():void
		{
			http_response_code(
				$this->coreResponse->getHttpStatus()
			);
		}

		private function dispatchCookies():void
		{
			foreach ($this->coreResponse->getCookies() as $coreCookie)
			{
				$this->coreCookieDispatcher->dispatch($coreCookie);
			}
		}

		private function dispatchBody():void
		{
			echo $this->coreResponse->getBody();
		}

		private function reset():void
		{
			unset($this->coreResponse);
		}

}