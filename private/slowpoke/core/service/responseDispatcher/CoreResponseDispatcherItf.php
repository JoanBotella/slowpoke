<?php
declare(strict_types=1);

namespace slowpoke\core\service\responseDispatcher;

use slowpoke\core\library\response\CoreResponse;

interface
	CoreResponseDispatcherItf
{

	public function dispatch(CoreResponse $coreResponse):void;

}
