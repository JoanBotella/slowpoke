<?php
declare(strict_types=1);

namespace slowpoke\core\service\pdoContainer;

use PDO;

interface
	CorePdoContainerItf
{

	public function get():PDO;

}