<?php
declare(strict_types=1);

namespace slowpoke\core\service\pdoContainer;

use PDO;
use slowpoke\core\library\pdo\CorePdoInput;
use slowpoke\core\service\pdoContainer\CorePdoContainerItf;

final class
	CorePdoContainer
implements
	CorePdoContainerItf
{

	private CorePdoInput $corePdoInput;

	private PDO $pdo;

	public function __construct(
		CorePdoInput $corePdoInput
	)
	{
		$this->corePdoInput = $corePdoInput;
	}

	public function get():PDO
	{
		if (!isset($this->pdo))
		{
			$this->pdo = $this->buildPdo();
		}
		return $this->pdo;
	}

		private function buildPdo():PDO
		{
			$dsn = $this->corePdoInput->getDsn();
			$username =
				$this->corePdoInput->hasUsername()
					? $this->corePdoInput->getUsernameAfterHas()
					: null
			;
			$password =
				$this->corePdoInput->hasPassword()
					? $this->corePdoInput->getPasswordAfterHas()
					: null
			;
			$options =
				$this->corePdoInput->hasOptions()
					? $this->corePdoInput->getOptionsAfterHas()
					: null
			;
			return new PDO(
				$dsn,
				$username,
				$password,
				$options
			);
		}

}