<?php
declare(strict_types=1);

namespace slowpoke\core\service\app;

interface
	CoreAppItf
{

	public function run():void;

}
