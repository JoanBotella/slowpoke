<?php
declare(strict_types=1);

namespace slowpoke\core\service\app;

use Exception;
use slowpoke\core\library\controller\CoreControllerItf;
use slowpoke\core\library\request\CoreRequest;
use slowpoke\core\library\response\CoreResponse;
use slowpoke\core\service\app\CoreAppItf;
use slowpoke\core\service\requestBuilder\CoreRequestBuilderItf;
use slowpoke\core\service\responseDispatcher\CoreResponseDispatcherItf;
use slowpoke\core\service\router\CoreRouterItf;

final class
	CoreApp
implements
	CoreAppItf
{

	private CoreRequestBuilderItf $coreRequestBuilder;

	private CoreRouterItf $coreRouter;

	private CoreResponseDispatcherItf $coreResponseDispatcher;

	private CoreControllerItf $internalServerErrorController;

	private CoreRequest $coreRequest;

	public function __construct(
		CoreRequestBuilderItf $coreRequestBuilder,
		CoreRouterItf $coreRouter,
		CoreResponseDispatcherItf $coreResponseDispatcher,
		CoreControllerItf $internalServerErrorController
	)
	{
		$this->coreRequestBuilder = $coreRequestBuilder;
		$this->coreRouter = $coreRouter;
		$this->coreResponseDispatcher = $coreResponseDispatcher;
		$this->internalServerErrorController = $internalServerErrorController;
	}

	public function run():void
	{
		$this->setupRequest();
	
		$coreResponse = $this->routeRequestSafely();

		$this->coreResponseDispatcher->dispatch($coreResponse);

		$this->reset();
	}

		private function setupRequest():void
		{
			$this->coreRequest = $this->coreRequestBuilder->build();
		}

		private function routeRequestSafely():CoreResponse
		{
			try
			{
				return $this->coreRouter->route($this->coreRequest);
			}
			catch (Exception $exception)
			{
				return $this->buildInternalServerErrorResponse();
			}
		}

			private function buildInternalServerErrorResponse():CoreResponse
			{
				return $this->internalServerErrorController->run($this->coreRequest);
			}

		private function reset():void
		{
			unset($this->coreRequest);
		}

}