<?php
declare(strict_types=1);

namespace slowpoke\core\service\router;

use slowpoke\core\library\request\CoreRequest;
use slowpoke\core\library\response\CoreResponse;

interface
	CoreRouterItf
{

	public function route(CoreRequest $coreRequest):CoreResponse;

}