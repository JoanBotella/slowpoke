<?php
declare(strict_types=1);

namespace slowpoke\core\service\router;

use slowpoke\core\library\request\CoreRequest;
use slowpoke\core\library\response\CoreResponse;
use slowpoke\core\library\route\CoreRouteItf;
use slowpoke\core\library\controller\CoreControllerItf;
use slowpoke\core\service\router\CoreRouterItf;

final class
	CoreRouter
implements
	CoreRouterItf
{

	private array $routes;

	private CoreControllerItf $badRequestController;

	private CoreControllerItf $notFoundController;

	private CoreRequest $coreRequest;

	private CoreResponse $coreResponse;

	public function __construct(
		array $routes,
		CoreControllerItf $badRequestController,
		CoreControllerItf $notFoundController
	)
	{
		$this->routes = $routes;
		$this->badRequestController = $badRequestController;
		$this->notFoundController = $notFoundController;
	}

	public function route(CoreRequest $coreRequest):CoreResponse
	{
		$this->coreRequest = $coreRequest;

		foreach ($this->routes as $route)
		{
			$this->tryToSetupResponseByRoute($route);

			if (isset($this->coreResponse))
			{
				break;
			}
		}

		$coreResponse =
			isset($this->coreResponse)
				? $this->coreResponse
				: $this->buildNotFoundResponse()
		;

		$this->reset();

		return $coreResponse;
	}

		private function tryToSetupResponseByRoute(CoreRouteItf $route):void
		{
			$coreRequestMatcher = $route->getRequestMatcher();

			if (!$coreRequestMatcher->isMatch($this->coreRequest))
			{
				return;
			}

			$coreRequestValidator = $route->getRequestValidator();

			if (!$coreRequestValidator->isValid($this->coreRequest))
			{
				$this->coreResponse = $this->buildBadRequestResponse();
				return;
			}

			$controller = $route->getController();
			$this->coreResponse = $controller->run($this->coreRequest);
		}

			private function buildBadRequestResponse():CoreResponse
			{
				return $this->badRequestController->run($this->coreRequest);
			}

		private function buildNotFoundResponse():CoreResponse
		{
			return $this->notFoundController->run($this->coreRequest);
		}

		private function reset():void
		{
			unset($this->coreResponse);
		}

}