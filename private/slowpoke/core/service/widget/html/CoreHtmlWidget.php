<?php
declare(strict_types=1);

namespace slowpoke\core\service\widget\html;

use slowpoke\core\service\widget\html\CoreHtmlWidgetContext;
use slowpoke\core\service\widget\html\CoreHtmlWidgetItf;

final class
	CoreHtmlWidget
implements
	CoreHtmlWidgetItf
{

	public function render(CoreHtmlWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/coreHtmlWidgetTemplate.php';
		return ob_get_clean();
	}

}