<?php
declare(strict_types=1);

namespace slowpoke\core\service\widget\html;

final class
	CoreHtmlWidgetContext
{

	private string $titleContent;

	private string $languageCode;

	private string $pageCode;

	private string $baseHref;

	private string $faviconUrl;

	private array $styleUrls;

	private array $scriptUrls;

	private string $canonicalHref;

	private string $bodyElement;

	public function __construct(
		string $titleContent,
		string $languageCode,
		string $pageCode,
		string $baseHref,
		string $bodyElement
	)
	{
		$this->titleContent = $titleContent;
		$this->languageCode = $languageCode;
		$this->pageCode = $pageCode;
		$this->baseHref = $baseHref;
		$this->styleUrls = [];
		$this->scriptUrls = [];
		$this->bodyElement = $bodyElement;
	}

	public function getTitleContent():string
	{
		return $this->titleContent;
	}

	public function getLanguageCode():string
	{
		return $this->languageCode;
	}

	public function getPageCode():string
	{
		return $this->pageCode;
	}

	public function getBaseHref():string
	{
		return $this->baseHref;
	}

	public function hasFaviconUrl():bool
	{
		return isset($this->faviconUrl);
	}

	public function getFaviconUrlAfterHas():string
	{
		return $this->faviconUrl;
	}

	public function setFaviconUrl(string $v):void
	{
		$this->faviconUrl = $v;
	}

	public function unsetFaviconUrl():void
	{
		unset($this->faviconUrl);
	}

	public function getStyleUrls():array
	{
		return $this->styleUrls;
	}

	public function setStyleUrls(array $v):void
	{
		$this->styleUrls = $v;
	}

	public function getScriptUrls():array
	{
		return $this->scriptUrls;
	}

	public function setScriptUrls(array $v):void
	{
		$this->scriptUrls = $v;
	}

	public function hasCanonicalHref():bool
	{
		return isset($this->canonicalHref);
	}

	public function getCanonicalHrefAfterHas():string
	{
		return $this->canonicalHref;
	}

	public function setCanonicalHref(string $v):void
	{
		$this->canonicalHref = $v;
	}

	public function unsetCanonicalHref():void
	{
		unset($this->canonicalHref);
	}

	public function getBodyElement():string
	{
		return $this->bodyElement;
	}

}