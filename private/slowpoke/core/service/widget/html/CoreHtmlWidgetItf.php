<?php
declare(strict_types=1);

namespace slowpoke\core\service\widget\html;

use slowpoke\core\service\widget\html\CoreHtmlWidgetContext;

interface
	CoreHtmlWidgetItf
{

	public function render(CoreHtmlWidgetContext $context):string;

}