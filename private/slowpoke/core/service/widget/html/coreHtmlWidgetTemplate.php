<?php
declare(strict_types=1);

use slowpoke\core\service\widget\html\CoreHtmlWidgetContext;

/**
 * @var CoreHtmlWidgetContext $context
 */

?><!DOCTYPE html>
<html lang="<?= $context->getLanguageCode() ?>" data-page="<?= $context->getPageCode() ?>" data-widget="core-html">
<head>
	<meta charset="UTF-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

	<base href="<?= $context->getBaseHref() ?>" />

	<title><?= $context->getTitleContent() ?></title>

	<?php if ($context->hasFaviconUrl()): ?>

	<link rel="icon" href="<?= $context->getFaviconUrlAfterHas() ?>" type="image/png" />
	<?php endif ?>

	<?php foreach ($context->getStyleUrls() as $styleUrl): ?>

	<link href="<?= $styleUrl ?>" rel="stylesheet" media="screen" />
	<?php endforeach ?>

	<?php foreach ($context->getScriptUrls() as $scriptUrl): ?>

	<script src="<?= $scriptUrl ?>" defer="defer"></script>
	<?php endforeach ?>

	<?php if ($context->hasCanonicalHref()): ?>

	<link rel="canonical" href="<?= $context->getCanonicalHrefAfterHas() ?>" />
	<?php endif ?>

</head>
<?= $context->getBodyElement() ?>

</html>