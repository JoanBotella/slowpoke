<?php
declare(strict_types=1);

namespace slowpoke\core\service\widget\notifications;

use slowpoke\core\service\widget\notifications\CoreNotificationsWidgetContext;
use slowpoke\core\service\widget\notifications\CoreNotificationsWidgetItf;

final class
	CoreNotificationsWidget
implements
	CoreNotificationsWidgetItf
{

	public function render(CoreNotificationsWidgetContext $context):string
	{
		ob_start();
		require __DIR__.'/coreNotificationsWidgetTemplate.php';
		return ob_get_clean();
	}

}