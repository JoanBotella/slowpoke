<?php
declare(strict_types=1);

namespace slowpoke\core\service\widget\notifications;

use slowpoke\core\library\CoreNotifications;

final class
	CoreNotificationsWidgetContext
{

	private CoreNotifications $coreNotifications;

	public function __construct(
		CoreNotifications $coreNotifications
	)
	{
		$this->coreNotifications = $coreNotifications;
	}

	public function getCoreNotifications():CoreNotifications
	{
		return $this->coreNotifications;
	}

	public function setCoreNotifications(CoreNotifications $v):void
	{
		$this->coreNotifications = $v;
	}

}