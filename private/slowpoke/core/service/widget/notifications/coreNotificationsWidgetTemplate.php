<?php
declare(strict_types=1);

use slowpoke\core\service\widget\notifications\CoreNotificationsWidgetContext;

/**
 * @var CoreNotificationsWidgetContext $context
 */

$notifications = $context->getCoreNotifications();

?>

<?php if (
	$notifications->hasErrors()
	|| $notifications->hasWarnings()
	|| $notifications->hasInfos()
	|| $notifications->hasSuccesses()
): ?>
<ul data-widget="core-notifications">

<?php if ($notifications->hasErrors()): ?>
	<?php foreach ($notifications->getErrorsAfterHas() as $notification): ?>
	<li class="error"><?= $notification ?></li>
	<?php endforeach ?>
<?php endif ?>

<?php if ($notifications->hasWarnings()): ?>
	<?php foreach ($notifications->getWarningsAfterHas() as $notification): ?>
	<li class="warning"><?= $notification ?></li>
	<?php endforeach ?>
<?php endif ?>

<?php if ($notifications->hasInfos()): ?>
	<?php foreach ($notifications->getInfosAfterHas() as $notification): ?>
	<li class="info"><?= $notification ?></li>
	<?php endforeach ?>
<?php endif ?>

<?php if ($notifications->hasSuccesses()): ?>
	<?php foreach ($notifications->getSuccessesAfterHas() as $notification): ?>
	<li class="success"><?= $notification ?></li>
	<?php endforeach ?>
<?php endif ?>

</ul>
<?php endif ?>
