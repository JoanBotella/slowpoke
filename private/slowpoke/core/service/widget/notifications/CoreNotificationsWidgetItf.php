<?php
declare(strict_types=1);

namespace slowpoke\core\service\widget\notifications;

use slowpoke\core\service\widget\notifications\CoreNotificationsWidgetContext;

interface
	CoreNotificationsWidgetItf
{

	public function render(CoreNotificationsWidgetContext $context):string;

}