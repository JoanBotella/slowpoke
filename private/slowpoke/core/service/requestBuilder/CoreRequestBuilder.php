<?php
declare(strict_types=1);

namespace slowpoke\core\service\requestBuilder;

use slowpoke\core\library\language\CoreLanguageItf;
use slowpoke\core\library\request\CoreRequest;
use slowpoke\core\service\requestBuilder\CoreRequestBuilderItf;

final class
	CoreRequestBuilder
implements
	CoreRequestBuilderItf
{

	private string $basePath;

	private CoreLanguageItf $defaultLanguage;

	private array $supportedLanguages;

	private string $path;

	private array $pathSegments;

	private string $languageCode;

	public function __construct(
		string $basePath,
		CoreLanguageItf $defaultLanguage,
		array $supportedLanguages
	)
	{
		$this->basePath = $basePath;
		$this->defaultLanguage = $defaultLanguage;
		$this->supportedLanguages = $supportedLanguages;
	}

	public function build():CoreRequest
	{
		$this->setupPath();

		$this->setupPathSegments();

		$this->setupLanguageCode();

		$coreRequest = new CoreRequest(
			$this->languageCode,
			$this->path,
			$this->pathSegments,
			$_COOKIE,
			$_GET,
			$_POST
		);

		unset($this->languageCode);
		unset($this->path);
		unset($this->pathSegments);

		return $coreRequest;
	}

		private function setupPath():void
		{
			$path = $_SERVER['REQUEST_URI'];

			if (substr($path, 0, 1) === '/')
			{
				$path = substr($path, 1);
			}

			if (substr($path, -1) === '/')
			{
				$path = substr($path, 0, -1);
			}

			$basePath = $this->basePath;

			if (strpos($path, $basePath) === 0)
			{
				$path = substr($path, strlen($basePath));
			}

			if (substr($path, 0, 1) === '/')
			{
				$path = substr($path, 1);
			}

			$this->path = $path;
		}

		private function setupPathSegments():void
		{
			$this->pathSegments = explode('/', $this->path);
		}

		private function setupLanguageCode():void
		{
			if ($this->path === '')
			{
				$this->setupLanguageCodeFromTheDefaultLanguage();
				return;
			}

			$languages = $this->supportedLanguages;
			$segment = $this->pathSegments[0];

			foreach ($languages as $language)
			{
				$this->setupLanguageCodeIfLanguageMatchesSegment($language, $segment);

				if (isset($this->languageCode))
				{
					return;
				}
			}

			$this->setupLanguageCodeFromTheDefaultLanguage();
		}

			private function setupLanguageCodeFromTheDefaultLanguage():void
			{
				$this->languageCode = $this->defaultLanguage->getCode();
			}

			private function setupLanguageCodeIfLanguageMatchesSegment(CoreLanguageItf $language, string $segment):void
			{
				if ($language->getCode() === $segment)
				{
					$this->languageCode = $segment;
				}
			}

}