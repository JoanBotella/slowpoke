<?php
declare(strict_types=1);

namespace slowpoke\core\service\requestBuilder;

use slowpoke\core\library\request\CoreRequest;

interface
	CoreRequestBuilderItf
{

	public function build():CoreRequest;

}
