<?php
declare(strict_types=1);

namespace slowpoke\session\library\translation;

interface
	SessionTranslationItf
{

	public function session_sessionHasExpired():string;

}