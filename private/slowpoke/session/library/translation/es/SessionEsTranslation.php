<?php
declare(strict_types=1);

namespace slowpoke\session\library\translation\es;

use slowpoke\session\library\translation\SessionTranslationItf;

final class
	SessionEsTranslation
implements
	SessionTranslationItf
{

	public function session_sessionHasExpired():string { return 'La sessión ha expirado.'; }

}