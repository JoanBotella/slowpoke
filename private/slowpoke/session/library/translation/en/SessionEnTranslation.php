<?php
declare(strict_types=1);

namespace slowpoke\session\library\translation\en;

use slowpoke\session\library\translation\SessionTranslationItf;

final class
	SessionEnTranslation
implements
	SessionTranslationItf
{

	public function session_sessionHasExpired():string { return 'The session has expired.'; }

}