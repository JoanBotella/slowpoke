<?php
declare(strict_types=1);

namespace slowpoke\session\library\controller;

use slowpoke\core\library\controller\CoreNotificationsControllerTrait;
use slowpoke\core\library\request\CoreRequest;
use slowpoke\core\library\response\CoreResponse;
use slowpoke\session\library\translation\SessionTranslationItf;
use slowpoke\session\service\sessionManager\SessionSessionManagerItf;
use slowpoke\session\service\translationContainer\SessionTranslationContainerItf;

trait
	SessionControllerTrait
{
	use
		CoreNotificationsControllerTrait
	;

	protected function setup():void
	{
		parent::setup();

		$sessionSessionManager = $this->getSessionSessionManager();

		$sessionSessionManager->setup(
			$this->getCoreRequest()
		);

		if ($sessionSessionManager->isPreviousSessionExpired())
		{
			$this->addPreviousSessionExpiredNotification();
		}
	}

		abstract protected function getSessionSessionManager():SessionSessionManagerItf;

		abstract protected function getCoreRequest():CoreRequest;

			protected function addPreviousSessionExpiredNotification():void
			{
				$coreNotifications = $this->getCoreNotifications();

				$sessionTranslation = $this->getSessionTranslation();

				$coreNotifications->addWarning(
					$sessionTranslation->session_sessionHasExpired()
				);

				$this->setCoreNotifications($coreNotifications);
			}

				private function getSessionTranslation():SessionTranslationItf
				{
					$sessionTranslationContainer = $this->getSessionTranslationContainer();
					$languageCode = $this->getCoreRequest()->getLanguageCode();
					return $sessionTranslationContainer->getTranslation(
						$languageCode
					);
				}

					abstract protected function getSessionTranslationContainer():SessionTranslationContainerItf;

	protected function tearDown():void
	{
		$sessionSessionManager = $this->getSessionSessionManager();
		$coreResponse = $sessionSessionManager->tearDown(
			$this->getCoreResponse()
		);
		$this->setCoreResponse($coreResponse);
	}

		abstract protected function getCoreResponse():CoreResponse;

		abstract protected function setCoreResponse(CoreResponse $coreResponse):void;

}