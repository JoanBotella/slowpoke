<?php
declare(strict_types=1);

namespace slowpoke\session\library\persistence\entity\session;

use slowpoke\core\library\persistence\persister\CorePersisterItf;

interface
	SessionSessionPersisterItf
extends
	CorePersisterItf
{

	public function selectByKey(string $key):array;

	public function insert(SessionSessionEntity $sessionSessionEntity):void;

	public function update(SessionSessionEntity $sessionSessionEntity):void;

	public function deleteExpired():void;

}