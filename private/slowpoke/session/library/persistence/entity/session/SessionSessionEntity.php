<?php
declare(strict_types=1);

namespace slowpoke\session\library\persistence\entity\session;

use slowpoke\core\library\persistence\entity\CoreEntityAbs;
use DateTimeInterface;

final class
	SessionSessionEntity
extends
	CoreEntityAbs
{

	private string $key;

	public function hasKey():bool
	{
		return isset($this->key);
	}

	public function getKeyAfterHas():string
	{
		return $this->key;
	}

	public function setKey(string $v):void
	{
		$this->key = $v;
	}

	public function unsetKey():void
	{
		unset($this->key);
	}

	private DateTimeInterface $expiresAt;

	public function hasExpiresAt():bool
	{
		return isset($this->expiresAt);
	}

	public function getExpiresAtAfterHas():DateTimeInterface
	{
		return $this->expiresAt;
	}

	public function setExpiresAt(DateTimeInterface $v):void
	{
		$this->expiresAt = $v;
	}

	public function unsetExpiresAt():void
	{
		unset($this->expiresAt);
	}

}