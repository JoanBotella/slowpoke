<?php
declare(strict_types=1);

namespace slowpoke\session\library\serviceContainer;

use slowpoke\session\service\persister\session\mysql\SessionSessionMysqlPersister;
use slowpoke\session\service\persister\session\mysql\SessionSessionMysqlPersisterItf;
use slowpoke\session\service\sessionCookieBuilder\SessionSessionCookieBuilder;
use slowpoke\session\service\sessionCookieBuilder\SessionSessionCookieBuilderItf;
use slowpoke\session\service\sessionKeyGenerator\SessionSessionKeyGenerator;
use slowpoke\session\service\sessionKeyGenerator\SessionSessionKeyGeneratorItf;
use slowpoke\session\service\sessionManager\SessionSessionManager;
use slowpoke\session\service\sessionManager\SessionSessionManagerItf;
use slowpoke\session\service\sessionRequestWrapper\SessionSessionRequestWrapper;
use slowpoke\session\service\sessionRequestWrapper\SessionSessionRequestWrapperItf;
use slowpoke\session\service\translationContainer\SessionTranslationContainer;
use slowpoke\session\service\translationContainer\SessionTranslationContainerItf;

trait
	SessionServiceContainerTrait
{

	abstract protected function configuration_session_sessionDurationInSeconds():int;

	abstract protected function configuration_session_sessionKeyCookieName():string;

	// ---

	private SessionSessionCookieBuilderItf $sessionSessionCookieBuilder;

	public function getSessionSessionCookieBuilder():SessionSessionCookieBuilderItf
	{
		if (!isset($this->sessionSessionCookieBuilder))
		{
			$this->sessionSessionCookieBuilder = $this->buildSessionSessionCookieBuilder();
		}
		return $this->sessionSessionCookieBuilder;
	}

		protected function buildSessionSessionCookieBuilder():SessionSessionCookieBuilderItf
		{
			return new SessionSessionCookieBuilder(
				$this->configuration_core_domain(),
				$this->configuration_core_basePath(),
				$this->configuration_session_sessionKeyCookieName()
			);
		}

	private SessionSessionKeyGeneratorItf $sessionSessionKeyGenerator;

	public function getSessionSessionKeyGenerator():SessionSessionKeyGeneratorItf
	{
		if (!isset($this->sessionSessionKeyGenerator))
		{
			$this->sessionSessionKeyGenerator = $this->buildSessionSessionKeyGenerator();
		}
		return $this->sessionSessionKeyGenerator;
	}

		protected function buildSessionSessionKeyGenerator():SessionSessionKeyGeneratorItf
		{
			return new SessionSessionKeyGenerator(
			);
		}

	private SessionSessionManagerItf $sessionSessionManager;

	public function getSessionSessionManager():SessionSessionManagerItf
	{
		if (!isset($this->sessionSessionManager))
		{
			$this->sessionSessionManager = $this->buildSessionSessionManager();
		}
		return $this->sessionSessionManager;
	}

		protected function buildSessionSessionManager():SessionSessionManagerItf
		{
			return new SessionSessionManager(
				$this->getSessionSessionMysqlPersister(),
				$this->getSessionSessionKeyGenerator(),
				$this->getSessionSessionRequestWrapper(),
				$this->getSessionSessionCookieBuilder(),
				$this->configuration_session_sessionDurationInSeconds()
			);
		}

	private SessionSessionMysqlPersisterItf $sessionSessionMysqlPersister;

	public function getSessionSessionMysqlPersister():SessionSessionMysqlPersisterItf
	{
		if (!isset($this->sessionSessionMysqlPersister))
		{
			$this->sessionSessionMysqlPersister = $this->buildSessionSessionMysqlPersister();
		}
		return $this->sessionSessionMysqlPersister;
	}

		protected function buildSessionSessionMysqlPersister():SessionSessionMysqlPersisterItf
		{
			return new SessionSessionMysqlPersister(
				$this->getCorePdoContainer()
			);
		}

	private SessionSessionRequestWrapperItf $sessionSessionRequestWrapper;

	public function getSessionSessionRequestWrapper():SessionSessionRequestWrapperItf
	{
		if (!isset($this->sessionSessionRequestWrapper))
		{
			$this->sessionSessionRequestWrapper = $this->buildSessionSessionRequestWrapper();
		}
		return $this->sessionSessionRequestWrapper;
	}

		protected function buildSessionSessionRequestWrapper():SessionSessionRequestWrapperItf
		{
			return new SessionSessionRequestWrapper(
				$this->configuration_session_sessionKeyCookieName()
			);
		}

	private SessionTranslationContainerItf $sessionTranslationContainer;

	public function getSessionTranslationContainer():SessionTranslationContainerItf
	{
		if (!isset($this->sessionTranslationContainer))
		{
			$this->sessionTranslationContainer = $this->buildSessionTranslationContainer();
		}
		return $this->sessionTranslationContainer;
	}

		protected function buildSessionTranslationContainer():SessionTranslationContainerItf
		{
			return new SessionTranslationContainer(
			);
		}

}