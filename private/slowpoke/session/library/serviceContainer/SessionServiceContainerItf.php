<?php
declare(strict_types=1);

namespace slowpoke\session\library\serviceContainer;

use slowpoke\session\service\persister\session\mysql\SessionSessionMysqlPersisterItf;
use slowpoke\session\service\sessionCookieBuilder\SessionSessionCookieBuilderItf;
use slowpoke\session\service\sessionKeyGenerator\SessionSessionKeyGeneratorItf;
use slowpoke\session\service\sessionManager\SessionSessionManagerItf;
use slowpoke\session\service\sessionRequestWrapper\SessionSessionRequestWrapperItf;
use slowpoke\session\service\translationContainer\SessionTranslationContainerItf;

interface
	SessionServiceContainerItf
{

	public function getSessionSessionCookieBuilder():SessionSessionCookieBuilderItf;

	public function getSessionSessionKeyGenerator():SessionSessionKeyGeneratorItf;

	public function getSessionSessionManager():SessionSessionManagerItf;

	public function getSessionSessionMysqlPersister():SessionSessionMysqlPersisterItf;

	public function getSessionSessionRequestWrapper():SessionSessionRequestWrapperItf;

	public function getSessionTranslationContainer():SessionTranslationContainerItf;

}