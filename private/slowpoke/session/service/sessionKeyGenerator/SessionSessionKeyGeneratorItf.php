<?php
declare(strict_types=1);

namespace slowpoke\session\service\sessionKeyGenerator;

interface
	SessionSessionKeyGeneratorItf
{

	public function generate():string;

}