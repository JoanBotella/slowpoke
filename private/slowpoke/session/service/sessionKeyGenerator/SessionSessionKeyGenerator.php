<?php
declare(strict_types=1);

namespace slowpoke\session\service\sessionKeyGenerator;

use slowpoke\session\service\sessionKeyGenerator\SessionSessionKeyGeneratorItf;

final class
	SessionSessionKeyGenerator
implements
	SessionSessionKeyGeneratorItf
{

	const KEY_LENGTH = 10;

	public function generate():string
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);

		$randomString = '';
		for ($i = 0; $i < self::KEY_LENGTH; $i++)
		{
			$position = rand(0, $charactersLength - 1);
			$randomString .= $characters[$position];
		}

		return $randomString;
	}

}