<?php
declare(strict_types=1);

namespace slowpoke\session\service\sessionRequestWrapper;

use slowpoke\core\library\request\CoreRequest;

interface
	SessionSessionRequestWrapperItf
{

	public function hasSessionKey(CoreRequest $coreRequest):bool;

	public function getSessionKeyAfterHas(CoreRequest $coreRequest):string;

}