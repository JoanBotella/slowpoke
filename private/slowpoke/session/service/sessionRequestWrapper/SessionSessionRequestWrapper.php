<?php
declare(strict_types=1);

namespace slowpoke\session\service\sessionRequestWrapper;

use slowpoke\session\service\sessionRequestWrapper\SessionSessionRequestWrapperItf;
use slowpoke\core\library\request\CoreRequest;

final class
	SessionSessionRequestWrapper
implements
	SessionSessionRequestWrapperItf
{

	private string $sessionKeyCookieName;

	public function __construct(
		string $sessionKeyCookieName
	)
	{
		$this->sessionKeyCookieName = $sessionKeyCookieName;
	}

	public function hasSessionKey(CoreRequest $coreRequest):bool
	{
		$cookies = $coreRequest->getCookies();
		return isset($cookies[$this->sessionKeyCookieName]);
	}

	public function getSessionKeyAfterHas(CoreRequest $coreRequest):string
	{
		$cookies = $coreRequest->getCookies();
		return $cookies[$this->sessionKeyCookieName];
	}

}