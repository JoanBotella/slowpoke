<?php
declare(strict_types=1);

namespace slowpoke\session\service\sessionManager;

use DateTime;
use DateTimeInterface;
use slowpoke\session\library\persistence\entity\session\SessionSessionPersisterItf;
use slowpoke\session\service\sessionManager\SessionSessionManagerItf;
use slowpoke\session\library\persistence\entity\session\SessionSessionEntity;
use slowpoke\core\library\request\CoreRequest;
use slowpoke\core\library\response\CoreResponse;
use slowpoke\session\service\sessionCookieBuilder\SessionSessionCookieBuilderItf;
use slowpoke\session\service\sessionKeyGenerator\SessionSessionKeyGeneratorItf;
use slowpoke\session\service\sessionRequestWrapper\SessionSessionRequestWrapperItf;

final class
	SessionSessionManager
implements
	SessionSessionManagerItf
{

	private SessionSessionPersisterItf $sessionSessionPersister;

	private SessionSessionKeyGeneratorItf $sessionSessionKeyGenerator;

	private SessionSessionRequestWrapperItf $sessionSessionRequestWrapper;

	private SessionSessionCookieBuilderItf $sessionSessionCookieBuilder;

	private int $sessionDurationInSeconds;

	private CoreRequest $coreRequest;

	private CoreResponse $coreResponse;

	private string $currentSessionKey;

	private string $nextSessionKey;

	private SessionSessionEntity $sessionSessionEntity;

	private bool $previousSessionExpired;

	public function __construct(
		SessionSessionPersisterItf $sessionSessionPersister,
		SessionSessionKeyGeneratorItf $sessionSessionKeyGenerator,
		SessionSessionRequestWrapperItf $sessionSessionRequestWrapper,
		SessionSessionCookieBuilderItf $sessionSessionCookieBuilder,
		int $sessionDurationInSeconds
	)
	{
		$this->sessionSessionPersister = $sessionSessionPersister;
		$this->sessionSessionKeyGenerator = $sessionSessionKeyGenerator;
		$this->sessionSessionRequestWrapper = $sessionSessionRequestWrapper;
		$this->sessionSessionCookieBuilder = $sessionSessionCookieBuilder;
		$this->sessionDurationInSeconds = $sessionDurationInSeconds;
	}

	public function setup(CoreRequest $coreRequest):void
	{
		$this->coreRequest = $coreRequest;

		$this->deleteExpiredSessions();

		$this->setupCurrentSessionKey();

		if (!isset($this->currentSessionKey))
		{
			return;
		}

		$this->setupSessionSessionEntity();

		if (isset($this->sessionSessionEntity))
		{
			return;
		}

		$this->previousSessionExpired = true;
	}

		private function deleteExpiredSessions():void
		{
			$this->sessionSessionPersister->deleteExpired();
		}

		private function setupCurrentSessionKey():void
		{
			if ($this->sessionSessionRequestWrapper->hasSessionKey($this->coreRequest))
			{
				$this->currentSessionKey = $this->sessionSessionRequestWrapper->getSessionKeyAfterHas($this->coreRequest);
			}
		}

		private function setupSessionSessionEntity():void
		{
			$result = $this->sessionSessionPersister->selectByKey($this->currentSessionKey);

			if ($result)
			{
				$this->sessionSessionEntity = $result[0];
			}
		}

	public function tearDown(CoreResponse $coreResponse):CoreResponse
	{
		$this->coreResponse = $coreResponse;

		$this->nextSessionKey = $this->buildNextSessionKey();

		$this->updateSessionSessionEntityOrInsertANewOne();

		$this->addSessionCookieToResponse();

		$coreResponse = $this->coreResponse;

		$this->reset();

		return $coreResponse;
	}

		private function buildNextSessionKey():string
		{
			return $this->sessionSessionKeyGenerator->generate();
		}

		private function updateSessionSessionEntityOrInsertANewOne():void
		{
			if (isset($this->sessionSessionEntity))
			{
				$this->prepareSessionSessionEntityForUpdate();
				$this->sessionSessionPersister->update($this->sessionSessionEntity);
				return;
			}

			$this->sessionSessionPersister->insert(
				$this->buildNewSessionSessionEntity()
			);
		}

			private function prepareSessionSessionEntityForUpdate():void
			{
				$this->sessionSessionEntity->setKey($this->nextSessionKey);

				$this->sessionSessionEntity->setExpiresAt(
					$this->buildExpiresAt()
				);
			}

				private function buildExpiresAt():DateTimeInterface
				{
					$expiresAt = new DateTime('now');
					$expiresAt = $expiresAt->modify('+'.$this->sessionDurationInSeconds.' seconds');
					return $expiresAt;
				}

			private function buildNewSessionSessionEntity():SessionSessionEntity
			{
				$sessionSessionEntity = new SessionSessionEntity();

				$sessionSessionEntity->setKey($this->nextSessionKey);

				$sessionSessionEntity->setExpiresAt(
					$this->buildExpiresAt()
				);

				return $sessionSessionEntity;
			}

		private function addSessionCookieToResponse():void
		{
			$sessionCookie = $this->sessionSessionCookieBuilder->build($this->nextSessionKey);
			$this->coreResponse->addCookie($sessionCookie);
		}

		private function reset():void
		{
			unset($this->coreRequest);
			unset($this->coreResponse);
			unset($this->currentSessionKey);
			unset($this->nextSessionKey);
			unset($this->sessionSessionEntity);
			unset($this->previousSessionExpired);
		}

	public function hasSessionSessionEntity():bool
	{
		return isset($this->sessionSessionEntity);
	}

	public function getSessionSessionEntityAfterHas():SessionSessionEntity
	{
		return $this->sessionSessionEntity;
	}

	public function isPreviousSessionExpired():bool
	{
		return isset($this->previousSessionExpired);
	}

}