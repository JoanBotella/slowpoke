<?php
declare(strict_types=1);

namespace slowpoke\session\service\sessionManager;

use slowpoke\session\library\persistence\entity\session\SessionSessionEntity;
use slowpoke\core\library\request\CoreRequest;
use slowpoke\core\library\response\CoreResponse;

interface
	SessionSessionManagerItf
{

	public function setup(CoreRequest $coreRequest):void;

	public function tearDown(CoreResponse $coreResponse):CoreResponse;

	public function hasSessionSessionEntity():bool;

	public function getSessionSessionEntityAfterHas():SessionSessionEntity;

	public function isPreviousSessionExpired():bool;

}