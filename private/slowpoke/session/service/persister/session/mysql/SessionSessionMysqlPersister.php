<?php
declare(strict_types=1);

namespace slowpoke\session\service\persister\session\mysql;

use DateTime;
use PDO;
use slowpoke\session\library\persistence\entity\session\SessionSessionEntity;
use slowpoke\core\library\persistence\persister\mysql\CoreMysqlPersisterAbs;
use slowpoke\session\service\persister\session\mysql\SessionSessionMysqlPersisterItf;

final class
	SessionSessionMysqlPersister
extends
	CoreMysqlPersisterAbs
implements
	SessionSessionMysqlPersisterItf
{

	const TABLE = 'session_session';

	const COLUMN_ID = 'id';
	const COLUMN_CREATED_AT = 'created_at';
	const COLUMN_UPDATED_AT = 'updated_at';
	const COLUMN_KEY = 'key';
	const COLUMN_EXPIRES_AT = 'expires_at';

	protected function getTable():string
	{
		return self::TABLE;
	}

	protected function addEntityToResultByRow(array $result, array $row):array
	{
		$result[] = $this->buildEntityByRow($row);
		return $result;
	}

		private function buildEntityByRow(array $row):SessionSessionEntity
		{
			$sessionSessionEntity = new SessionSessionEntity();

			$key = self::COLUMN_ID;
			if (isset($row[$key]))
			{
				$sessionSessionEntity->setId(
					(int) $row[$key]
				);
			}

			$key = self::COLUMN_CREATED_AT;
			if (isset($row[$key]))
			{
				$sessionSessionEntity->setCreatedAt(
					$this->buildDateTimeByString($row[$key])
				);
			}

			$key = self::COLUMN_UPDATED_AT;
			if (isset($row[$key]))
			{
				$sessionSessionEntity->setUpdatedAt(
					$this->buildDateTimeByString($row[$key])
				);
			}

			$key = self::COLUMN_KEY;
			if (isset($row[$key]))
			{
				$sessionSessionEntity->setKey($row[$key]);
			}

			$key = self::COLUMN_EXPIRES_AT;
			if (isset($row[$key]))
			{
				$sessionSessionEntity->setExpiresAt(
					$this->buildDateTimeByString($row[$key])
				);
			}

			return $sessionSessionEntity;
		}

	public function selectByKey(string $key):array
	{
		$where = '
			`'.self::COLUMN_KEY.'` = ?
		';

		$bindings = [
			[
				$key,
				PDO::PARAM_STR
			],
		];

		return $this->selectWhere(
			$where,
			$bindings
		);
	}

	public function insert(SessionSessionEntity $sessionSessionEntity):void
	{
		$columns = [
			self::COLUMN_KEY,
			self::COLUMN_EXPIRES_AT,
		];

		$bindings = [];

		$bindings[] =
			$sessionSessionEntity->hasKey()
				? [
					$sessionSessionEntity->getKeyAfterHas(),
					PDO::PARAM_STR,
				]
				: [
					null,
					PDO::PARAM_NULL,
				]
		;

		$bindings[] =
			$sessionSessionEntity->hasExpiresAt()
				? [
					$this->buildStringByDateTime(
						$sessionSessionEntity->getExpiresAtAfterHas()
					),
					PDO::PARAM_STR,
				]
				: [
					null,
					PDO::PARAM_NULL,
				]
		;

		$this->insertColumns(
			$columns,
			$bindings
		);
	}

	public function update(SessionSessionEntity $sessionSessionEntity):void
	{
		$set = '
			`'.self::COLUMN_KEY.'` = ?,
			`'.self::COLUMN_EXPIRES_AT.'` = ?
		';

		$where = '
			`'.self::COLUMN_ID.'` = ?
		';

		$bindings[] =
			$sessionSessionEntity->hasKey()
				? [
					$sessionSessionEntity->getKeyAfterHas(),
					PDO::PARAM_STR,
				]
				: [
					null,
					PDO::PARAM_NULL,
				]
		;

		$bindings[] =
			$sessionSessionEntity->hasExpiresAt()
				? [
					$this->buildStringByDateTime(
						$sessionSessionEntity->getExpiresAtAfterHas()
					),
					PDO::PARAM_STR,
				]
				: [
					null,
					PDO::PARAM_NULL,
				]
		;

		$bindings[] =
			$sessionSessionEntity->hasId()
				? [
					$sessionSessionEntity->getIdAfterHas(),
					PDO::PARAM_INT,
				]
				: [
					null,
					PDO::PARAM_NULL,
				]
		;

		$this->updateWhere(
			$set,
			$where,
			$bindings
		);
	}

	public function deleteExpired():void
	{
		$where = '
			`'.self::COLUMN_EXPIRES_AT.'` <= ?
		';

		$bindings = [
			[
				$this->buildStringByDateTime(
					new DateTime('now')
				),
				PDO::PARAM_STR
			],
		];

		$this->deleteWhere(
			$where,
			$bindings
		);
	}

}