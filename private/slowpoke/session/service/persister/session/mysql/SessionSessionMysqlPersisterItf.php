<?php
declare(strict_types=1);

namespace slowpoke\session\service\persister\session\mysql;

use slowpoke\session\library\persistence\entity\session\SessionSessionPersisterItf;

interface
	SessionSessionMysqlPersisterItf
extends
	SessionSessionPersisterItf
{

}
