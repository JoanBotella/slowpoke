<?php
declare(strict_types=1);

namespace slowpoke\session\service\sessionCookieBuilder;

use slowpoke\core\library\cookie\CoreCookie;

interface
	SessionSessionCookieBuilderItf
{

	public function build(string $key):CoreCookie;

}