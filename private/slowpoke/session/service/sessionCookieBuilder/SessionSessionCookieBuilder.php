<?php
declare(strict_types=1);

namespace slowpoke\session\service\sessionCookieBuilder;

use slowpoke\core\library\cookie\CoreCookie;
use slowpoke\core\library\cookie\CoreCookieSameSiteConstant;
use slowpoke\session\service\sessionCookieBuilder\SessionSessionCookieBuilderItf;

final class
	SessionSessionCookieBuilder
implements
	SessionSessionCookieBuilderItf
{

	private string $domain;

	private string $basePath;

	private string $sessionKeyCookieName;

	public function __construct(
		string $domain,
		string $basePath,
		string $sessionKeyCookieName
	)
	{
		$this->domain = $domain;
		$this->basePath = $basePath;
		$this->sessionKeyCookieName = $sessionKeyCookieName;
	}

	public function build(string $key):CoreCookie
	{
		$coreCookie = new CoreCookie($this->sessionKeyCookieName);

		$coreCookie->setValue($key);

		$coreCookie->setDomain($this->domain);

		$coreCookie->setPath('/'.$this->basePath.'/');

		$coreCookie->setSecure(true);

		$coreCookie->setHttpOnly(true);

		$coreCookie->setSameSite(
			CoreCookieSameSiteConstant::NONE
		);

		return $coreCookie;
	}

}