<?php
declare(strict_types=1);

namespace slowpoke\session\service\translationContainer;

use slowpoke\session\library\translation\SessionTranslationItf;

interface
	SessionTranslationContainerItf
{

	public function getTranslation(string $languageCode):SessionTranslationItf;

}