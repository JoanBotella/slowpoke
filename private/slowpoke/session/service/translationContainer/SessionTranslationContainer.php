<?php
declare(strict_types=1);

namespace slowpoke\session\service\translationContainer;

use Exception;
use slowpoke\core\library\language\CoreEnLanguage;
use slowpoke\core\library\language\CoreEsLanguage;
use slowpoke\session\library\translation\en\SessionEnTranslation;
use slowpoke\session\library\translation\es\SessionEsTranslation;
use slowpoke\session\library\translation\SessionTranslationItf;
use slowpoke\session\service\translationContainer\SessionTranslationContainerItf;

final class
	SessionTranslationContainer
implements
	SessionTranslationContainerItf
{

	private array $translations;

	public function getTranslation(string $languageCode):SessionTranslationItf
	{
		if (!isset($this->translations))
		{
			$this->translations = [];
		}

		if (!isset($this->translations[$languageCode]))
		{
			$this->translations[$languageCode] = $this->buildTranslation($languageCode);
		}

		return $this->translations[$languageCode];
	}

	public function buildTranslation(string $languageCode):SessionTranslationItf
	{
		switch ($languageCode)
		{
			case CoreEnLanguage::CODE:
				return new SessionEnTranslation();
			case CoreEsLanguage::CODE:
				return new SessionEsTranslation();
		}
		throw new Exception('Language '.$languageCode.' not supported');
	}

}